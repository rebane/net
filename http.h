#ifndef _HTTP_H_
#define _HTTP_H_

#include <stdint.h>

#define HTTP_MODE_REQUEST       1
#define HTTP_MODE_RESPONSE      2

#define HTTP_EVENT_METHOD       1
#define HTTP_EVENT_URL          2
#define HTTP_EVENT_QUERY_KEY    3
#define HTTP_EVENT_QUERY_VALUE  4
#define HTTP_EVENT_QUERY_DONE   5
#define HTTP_EVENT_VERSION      6
#define HTTP_EVENT_STATUS       7
#define HTTP_EVENT_REASON       8
#define HTTP_EVENT_REQUEST_DONE 9
#define HTTP_EVENT_STATUS_DONE  10
#define HTTP_EVENT_PARAM        11
#define HTTP_EVENT_VALUE        12
#define HTTP_EVENT_HEADER_DONE  13
#define HTTP_EVENT_BODY         14
#define HTTP_EVENT_BODY_DONE    15

struct http_struct{
	uint8_t mode;
	uint8_t state;
	uint8_t esc;
	uint8_t c;
	uint8_t cl;
	int32_t parse_loc;
	int32_t token_start;
	int32_t content_len;
	int32_t token_len;
	int16_t space_len;
	int16_t buffer_loc;
	int16_t buffer_len;
	uint8_t *buffer;
};

typedef struct http_struct http_t;

void http_init(http_t *hh, void *buf, int16_t count, uint8_t mode);
uint8_t http_parse(http_t *hh, const void *buf, int16_t count, void *user, uint8_t(*callback)(http_t *, int , int32_t, int16_t, const void *, int16_t, void *));
void http_set_buffer(http_t *hh, void *buf, int16_t count);

#endif

