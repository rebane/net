#include "udp.h"
#include "net.h"
#include <string.h>

int udp_parse_raw(udp_header_t *header, uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count){
	uint16_t sum;
	if(count < UDP_HEADER_LEN)return(NET_ERROR_FORMAT);
	header->len = udp_len(buf);
	if(header->len > count)return(NET_ERROR_FORMAT);
	if(header->len < UDP_HEADER_LEN)return(NET_ERROR_FORMAT);
	sum = udp_chk(buf);
	if(sum != 0){ // UDP checksum computation is optional for IPv4. If a checksum is not used it should be set to the value zero.
		if(sum != udp_chk_calc(dest_ip, src_ip, buf))return(NET_ERROR_FORMAT);
	}
	header->dest_port = udp_dest_port(buf);
	header->src_port = udp_src_port(buf);
	header->data = &((uint8_t *)buf)[UDP_HEADER_LEN];
	header->len -= UDP_HEADER_LEN;
	return(0);
}

int16_t udp_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((UDP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[UDP_HEADER_LEN], buf, count);
	udp_src_port_set(dest, src_port);
	udp_dest_port_set(dest, dest_port);
	udp_len_set(dest, UDP_HEADER_LEN + count);
#ifdef UDP_USE_CHK
	udp_chk_set(dest, udp_chk_calc(dest_ip, src_ip, dest));
#else
	udp_chk_set(dest, 0);
#endif
	return(UDP_HEADER_LEN + count);
}

uint16_t udp_chk_calc(uint32_t dest_ip, uint32_t src_ip, const void *buf){
	uint32_t sum;
	uint16_t i, l;
	sum = 0;
	sum += (src_ip >> 16) & 0xFFFF;
	sum += (src_ip >> 0) & 0xFFFF;
	sum += (dest_ip >> 16) & 0xFFFF;
	sum += (dest_ip >> 0) & 0xFFFF;
	sum += (uint16_t)UDP_IPV4_PROTOCOL_UDP;
	l = ((uint16_t)((uint8_t *)buf)[4] << 8) | ((uint8_t *)buf)[5];
	sum += l;
	for(i = 0; i < l; i += 2){
		if(i == 6)continue;
		if((i + 1) >= l){
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8);
		}else{
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	return((~sum) & 0xFFFF);
}

