#ifndef _NET_API_H_
#define _NET_API_H_

#include <stdint.h>

// RESOLV
void net_api_reset();
void net_api_resolv_query(const char *name, uint16_t len);
int net_api_resolv_done(const char *name, uint16_t len, uint32_t *ip);

void net_api_recv_resolv(const void *recv_buffer, uint16_t len);

// SOCKET
int net_api_socket_listen(uint16_t port);
void net_api_socket_nolisten(uint16_t port);
int net_api_socket_accept(uint16_t port);
int net_api_socket_connect(uint32_t dest_ip, uint16_t dest_port);
int net_api_socket_connected(int id);
int net_api_socket_read(int id, void *buf, int count);
int net_api_socket_write(int id, const void *buf, int count);
int net_api_socket_write_pending(int id);
void net_api_socket_shutdown(int id);
void net_api_socket_close(int id);

#endif

