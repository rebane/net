# net

net is a tcp/ip library for use in microcontrollers

Copyright © 2017, [Veiko Rütter](https://bitbucket.org/rebane/).
Released under the [MIT License](LICENSE).

