#include "ppp.h"
#include "net.h"

int ppp_parse_raw(ppp_header_t *header, const void *buf, int16_t count){
	if(count < PPP_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(ppp_len(buf) < PPP_HEADER_LEN)return(NET_ERROR_FORMAT);
	header->code = ppp_code(buf);
	header->identifier = ppp_identifier(buf);
	header->len = ppp_len(buf) - PPP_HEADER_LEN;
	header->data = &((uint8_t *)buf)[PPP_HEADER_LEN];
	return(0);
}

int16_t ppp_write(void *dest, int16_t max, uint8_t code, uint8_t identifier, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((PPP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[PPP_HEADER_LEN], buf, count);
	ppp_code_set(dest, code);
	ppp_identifier_set(dest, identifier);
	ppp_len_set(dest, (PPP_HEADER_LEN + count));
	return(PPP_HEADER_LEN + count);
}

int16_t ppp_parameter_write(void *dest, int16_t max, int16_t offset, uint8_t type, const void *buf, uint8_t count){
	if(offset < 0)return(offset);
	if((offset + 2 + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[offset + 2], buf, count);
	((uint8_t *)dest)[offset + 0] = type;
	((uint8_t *)dest)[offset + 1] = 2 + count;
	return(offset + 2 + count);
}

int16_t ppp_parameter_write_addr(void *dest, int16_t max, int16_t offset, uint8_t type, uint32_t addr){
	if(offset < 0)return(offset);
	if((offset + 2 + 4) > max)return(NET_ERROR_MEM);
	((uint8_t *)dest)[offset + 2] = ((addr >> 24) & 0xFF);
	((uint8_t *)dest)[offset + 3] = ((addr >> 16) & 0xFF);
	((uint8_t *)dest)[offset + 4] = ((addr >> 8) & 0xFF);
	((uint8_t *)dest)[offset + 5] = ((addr >> 0) & 0xFF);
	((uint8_t *)dest)[offset + 0] = type;
	((uint8_t *)dest)[offset + 1] = 2 + 4;
	return(offset + 2 + 4);
}

void ppp_parameter_first(ppp_parameter_t *parameter, const void *buf, int16_t count){
	parameter->buf = (uint8_t *)buf;
	parameter->size = count;
	parameter->next = 0;
	ppp_parameter_next(parameter);
}

uint8_t ppp_parameter_valid(ppp_parameter_t *parameter){
	return(parameter->valid);
}

void ppp_parameter_next(ppp_parameter_t *parameter){
	if(parameter->size < parameter->next){
		parameter->valid = 0;
		return;
	}
	parameter->buf += parameter->next;
	parameter->size -= parameter->next;
	parameter->len = parameter->buf[1];
	if(parameter->len < 2){
		parameter->valid = 0;
		return;
	}
	parameter->len -= 2;
	if(parameter->len > parameter->size){
		parameter->valid = 0;
		return;
	}
	parameter->type = parameter->buf[0];
	parameter->data = &parameter->buf[2];
	parameter->next = parameter->len + 2;
	parameter->valid = 1;
}

