#ifndef _IPV4_H_
#define _IPV4_H_

#include <stdint.h>

#define IPV4_HEADER_LEN                   20

#define IPV4_VERSION_IPV4                 4
#define IPV4_PROTOCOL_ICMP                0x01
#define IPV4_PROTOCOL_IGMP                0x02
#define IPV4_PROTOCOL_TCP                 0x06
#define IPV4_PROTOCOL_UDP                 0x11

#define ipv4_version(buf)                 ((((uint8_t *)(buf))[0] >> 4) & 0x0F)
#define ipv4_ihl(buf)                     ((((uint8_t *)(buf))[0] >> 0) & 0x0F)
#define ipv4_dscp(buf)                    ((((uint8_t *)(buf))[1] >> 2) & 0x3F)
#define ipv4_ecn(buf)                     ((((uint8_t *)(buf))[1] >> 0) & 0x03)
#define ipv4_len(buf)                     (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define ipv4_identification(buf)          (((uint16_t)((uint8_t *)(buf))[4] << 8) | ((uint8_t *)(buf))[5])
#define ipv4_flags(buf)                   ((((uint8_t *)(buf))[6] >> 5) & 0x07)
#define ipv4_fragment_offset(buf)         (((uint16_t)(((uint8_t *)(buf))[6] & 0x1F) << 8) | ((uint8_t *)(buf))[7])
#define ipv4_ttl(buf)                     (((uint8_t *)(buf))[8])
#define ipv4_protocol(buf)                (((uint8_t *)(buf))[9])
#define ipv4_chk(buf)                     (((uint16_t)((uint8_t *)(buf))[10] << 8) | ((uint8_t *)(buf))[11])
#define ipv4_src_ip(buf)                  (((uint32_t)((uint8_t *)(buf))[12] << 24) | ((uint32_t)((uint8_t *)(buf))[13] << 16) | ((uint32_t)((uint8_t *)(buf))[14] << 8) | ((uint32_t)((uint8_t *)(buf))[15] << 0))
#define ipv4_dest_ip(buf)                 (((uint32_t)((uint8_t *)(buf))[16] << 24) | ((uint32_t)((uint8_t *)(buf))[17] << 16) | ((uint32_t)((uint8_t *)(buf))[18] << 8) | ((uint32_t)((uint8_t *)(buf))[19] << 0))

#define ipv4_version_set(buf, version)    (((uint8_t *)(buf))[0] = (((uint8_t *)(buf))[0] & 0x0F) | (((uint8_t)(version) << 4) & 0xF0))
#define ipv4_ihl_set(buf, ihl)            (((uint8_t *)(buf))[0] = (((uint8_t *)(buf))[0] & 0xF0) | (((uint8_t)(ihl) << 0) & 0x0F))
#define ipv4_dscp_set(buf, dscp)          (((uint8_t *)(buf))[1] = (((uint8_t *)(buf))[1] & 0x03) | (((uint8_t)(dscp) << 2) & 0xFC))
#define ipv4_ecn_set(buf, ecn)            (((uint8_t *)(buf))[1] = (((uint8_t *)(buf))[1] & 0xFC) | (((uint8_t)(ecn) << 0) & 0x03))
#define ipv4_len_set(buf, len)            do{ ((uint8_t *)(buf))[2] = (((uint16_t)(len) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(len) >> 0) & 0xFF); }while(0)
#define ipv4_identification_set(buf, id)  do{ ((uint8_t *)(buf))[4] = (((uint16_t)(id) >> 8) & 0xFF); ((uint8_t *)(buf))[5] = (((uint16_t)(id) >> 0) & 0xFF); }while(0)
#define ipv4_flags_set(buf, flags)        (((uint8_t *)(buf))[6] = (((uint8_t *)(buf))[6] & 0x1F) | (((uint8_t)(flags) << 5) & 0xE0))
#define ipv4_fragment_offset_set(buf, fo) do{ (((uint8_t *)(buf))[6] = (((uint8_t *)(buf))[6] & 0xE0) | (((uint16_t)(fo) >> 8) & 0x1F)); ((uint8_t *)(buf))[7] = (((uint16_t)(fo) >> 0) & 0xFF); }while(0)
#define ipv4_ttl_set(buf, ttl)            (((uint8_t *)(buf))[8] = ((uint8_t)(ttl)))
#define ipv4_protocol_set(buf, protocol)  (((uint8_t *)(buf))[9] = ((uint8_t)(protocol)))
#define ipv4_chk_set(buf, chk)            do{ ((uint8_t *)(buf))[10] = (((uint16_t)(chk) >> 8) & 0xFF); ((uint8_t *)(buf))[11] = (((uint16_t)(chk) >> 0) & 0xFF); }while(0)
#define ipv4_src_ip_set(buf, src_ip)      do{ ((uint8_t *)(buf))[12] = (((uint32_t)(src_ip) >> 24) & 0xFF); ((uint8_t *)(buf))[13] = (((uint32_t)(src_ip) >> 16) & 0xFF); ((uint8_t *)(buf))[14] = (((uint32_t)(src_ip) >> 8) & 0xFF); ((uint8_t *)(buf))[15] = (((uint32_t)(src_ip) >> 0) & 0xFF); }while(0)
#define ipv4_dest_ip_set(buf, dest_ip)    do{ ((uint8_t *)(buf))[16] = (((uint32_t)(dest_ip) >> 24) & 0xFF); ((uint8_t *)(buf))[17] = (((uint32_t)(dest_ip) >> 16) & 0xFF); ((uint8_t *)(buf))[18] = (((uint32_t)(dest_ip) >> 8) & 0xFF); ((uint8_t *)(buf))[19] = (((uint32_t)(dest_ip) >> 0) & 0xFF); }while(0)

struct ipv4_header_struct{
	uint32_t dest_ip;
	uint32_t src_ip;
	uint8_t protocol;
	void *data;
	int16_t len;
};

typedef struct ipv4_header_struct ipv4_header_t;

int ipv4_parse_raw(ipv4_header_t *header, const void *buf, int16_t count);
int16_t ipv4_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint8_t protocol, const void *buf, int16_t count);
uint16_t ipv4_chk_calc(const void *buf);

#endif

