#include "echo.h"
#include "net.h"

void echo_poll(){
	net_icmp_t icmp;
	if(!net_recv_icmp(&icmp))return;
	if(icmp.type == ICMP_TYPE_ECHO_REQUEST){
		if(icmp.code == 0){ // send echo reply
			net_send_icmp(icmp.src_ip, icmp.dest_ip, ICMP_TYPE_ECHO_REPLY, 0, icmp.header, icmp.data, icmp.len);
		}
	}
}

