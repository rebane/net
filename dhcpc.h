#ifndef _DHCPC_H_
#define _DHCPC_H_

#include <stdint.h>

struct dhcpc_struct{
	uint8_t state;
	int ifnum;
	uint32_t xid;
	uint32_t server;
	uint32_t ip;
	uint32_t nm;
	uint32_t gw;
	uint32_t ns;
	uint8_t try;
	uint32_t timer;
};

typedef struct dhcpc_struct dhcpc_t;

void dhcpc_init(dhcpc_t *dhcpc, int ifnum);
void dhcpc_poll(dhcpc_t *dhcpc);

#endif

