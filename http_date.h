#ifndef _HTTP_DATE_H_
#define _HTTP_DATE_H_

#include <time.h>

int getdate_http_r(const void *buf, int size, struct tm *res);
int getdate_rfc1123_r(const void *buf, int size, struct tm *res);
int getdate_rfc850_r(const void *buf, int size, struct tm *res);
int getdate_asctime_r(const void *buf, int size, struct tm *res);

#endif

