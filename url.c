#include "url.h"
#include <ctype.h>

// https://tools.ietf.org/html/rfc1808.html

static void url_parse_net_loc(url_t *url, uint16_t s, char *str, uint16_t len);

void url_parse(url_t *url, char *str, uint16_t len){
	uint16_t s, i;
	s = 0;
	for(i = 0; (i < len) && (str[s + i] != '#'); i++);
	if(str[s + i] == '#'){
		url->fragment = s + (i + 1);
		url->fragment_len = len - (i + 1);
		len = i;
	}else{
		url->fragment = s + i;
		url->fragment_len = 0;
	}
	url->scheme = s + 0;
	url->scheme_len = 0;
	for(i = 0; i < len; i++){
		if(str[s + i] == ':'){
			if(i){
				url->scheme_len = i;
				s += (i + 1);
				len -= (i + 1);
			}
			break;
		}else if((str[s + i] != '+') && (str[s + i] != '-') && (str[s + i] != '.') && !isalnum(str[(s + i)])){
			break;
		}
	}
	if((len > 1) && (str[s + 0] == '/') && (str[s + 1] == '/')){
		url->net_loc = (s + 2);
		for(i = 2; (i < len) && (str[s + i] != '/'); i++);
		if(str[s + i] == '/'){
			url->net_loc_len = i - 2;
		}else{
			url->net_loc_len = i - 1;
		}
		url_parse_net_loc(url, s + 2, &str[url->net_loc], url->net_loc_len);
		s += i;
		len -= i;
	}else{
		url->net_loc = url->user = url->pass = url->host = url->port = (s + 0);
		url->net_loc_len = url->pass_len = url->user_len = url->host_len = url->port_len = 0;
	}
	url->request = s + 0;
	url->request_len = len;
	for(i = 0; (i < len) && (str[s + i] != '?'); i++);
	if(str[s + i] == '?'){
		url->query = s + (i + 1);
		url->query_len = len - (i + 1);
		len = i;
	}else{
		url->query = s + i;
		url->query_len = 0;
	}
	for(i = 0; (i < len) && (str[s + i] != ';'); i++);
	if(str[s + i] == ';'){
		url->params = s + (i + 1);
		url->params_len = len - (i + 1);
		len = i;
	}else{
		url->params = s + i;
		url->params_len = 0;
	}
	url->path = s + 0;
	url->path_len = len;
}

static void url_parse_net_loc(url_t *url, uint16_t s, char *str, uint16_t len){
	uint16_t i;
	i = 0;
	url->host = s + i;
	for(url->host_len = 0; (i < len) && (str[i] != ':') && (str[i] != '@'); i++, url->host_len++);
	if(i == len){
		url->user = url->pass = url->host;
		url->port = s + i;
		url->user_len = url->pass_len = url->port_len = 0;
		return;
	}
	if(str[i] == ':'){
		i++;
		url->port = s + i;
		for(url->port_len = 0; (i < len) && (str[i] != '@'); i++, url->port_len++);
		if(i == len){
			url->user = url->pass = s + i;
			url->user_len = url->pass_len = 0;
			return;
		}
	}else{
		url->port = s + i;
		url->port_len = 0;
	}
	if(str[i] == '@'){
		url->user = url->host;
		url->user_len = url->host_len;
		url->pass = url->port;
		url->pass_len = url->port_len;
		i++;
		url->host = s + i;
		for(url->host_len = 0; (i < len) && (str[i] != ':'); i++, url->host_len++);
		if((i < len) && (str[i] == ':')){
			url->port = s + i + 1;
			url->port_len = len - i - 1;
		}else{
			url->port = s + i;
			url->port_len = 0;
		}
	}else{
		url->user = url->pass = s + i;
		url->user_len = url->pass_len = 0;
	}
}

