#include "interface.h"
#include "net.h"
#include "arp.h"

static void interface_ethernet_recv_ethernet_raw(int ifnum, interface_ethernet_t *interface_ethernet, const void *buf, int16_t count);
static void interface_ethernet_recv_arp_ethernet_ipv4_raw(int ifnum, interface_ethernet_t *interface_ethernet, const void *dest_mac, const void *src_mac, const void *buf, int16_t count);
static int16_t interface_ethernet_send_arp_ethernet_ipv4(int ifnum, const void *dest_mac, uint16_t oper, void *target_mac, void *sender_mac, uint32_t target_ip, uint32_t sender_ip);

int interface_ethernet_add(interface_ethernet_t *interface_ethernet, void*(*recv)(int16_t *len), int16_t(*send)(const void *buf, int16_t count)){
	int ifnum, i;
	ifnum = interface_add(INTERFACE_TYPE_ETHERNET, &interface_ethernet->interface, interface_ethernet);
	if(ifnum < 0)return(ifnum);
	interface_ethernet->recv = recv;
	interface_ethernet->send = send;
	for(i = 0; i < INTERFACE_ETHERNET_ARP_MAX; i++){
		interface_ethernet->arp_table[i].ip = 0;
	}
	interface_ethernet->arp_temp_ip = 0;
	return(ifnum);
}

interface_ethernet_t *interface_ethernet_get(int ifnum){
	return((interface_ethernet_t *)interface_get_custom(ifnum));
}

int16_t interface_ethernet_poll(int ifnum, interface_ethernet_t *interface_ethernet){
	int i;
	uint32_t timer;
	timer = net_timer_get();
	interface_ethernet->data = interface_ethernet->recv(&interface_ethernet->len);
	if((interface_ethernet->data != (void *)0) && interface_ethernet->len){
		interface_ethernet_recv_ethernet_raw(ifnum, interface_ethernet, interface_ethernet->data, interface_ethernet->len);
	}else{
		interface_ethernet->data = (void *)0;
		interface_ethernet->len = 0;
	}
	for(i = 0; i < INTERFACE_ETHERNET_ARP_MAX; i++){
		if(interface_ethernet->arp_table[i].used_timeout >= timer){
			if(interface_ethernet->arp_table[i].refresh_timeout <= timer){
				interface_ethernet_send_arp_ethernet_ipv4(ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ARP_OPER_REQUEST, "\x00\x00\x00\x00\x00\x00", interface_ethernet->mac, interface_ethernet->arp_table[i].ip, interface_ethernet->interface.ip);
				if(memcmp(interface_ethernet->arp_table[i].mac, "\x00\x00\x00\x00\x00\x00", 6)){
					interface_ethernet->arp_table[i].refresh_timeout = timer + 30;
				}else{
					interface_ethernet->arp_table[i].refresh_timeout = timer + 2;
				}
			}
		}
	}
	return(interface_ethernet->len);
}

void interface_ethernet_mac_set(int ifnum, void *mac){
	interface_ethernet_t *interface_ethernet;
	interface_ethernet = interface_ethernet_get(ifnum);
	memmove(&interface_ethernet->mac, mac, 6);
}

void *interface_ethernet_mac_get(int ifnum){
	interface_ethernet_t *interface_ethernet;
	interface_ethernet = interface_ethernet_get(ifnum);
	return(interface_ethernet->mac);
}

void interface_ethernet_arp_add(int ifnum, uint32_t ip, void *mac){
	interface_ethernet_t *interface_ethernet;
	int i, j;
	uint32_t lowest_used_timeout;
	interface_ethernet = interface_ethernet_get(ifnum);
	lowest_used_timeout = 0xFFFFFFFF;
	j = 0;
	for(i = 0; i < INTERFACE_ETHERNET_ARP_MAX; i++){
		if(interface_ethernet->arp_table[i].ip == ip){
			j = i;
			goto ready;
		}
		if(interface_ethernet->arp_table[i].used_timeout < lowest_used_timeout){
			lowest_used_timeout = interface_ethernet->arp_table[i].used_timeout;
			j = i;
		}
	}
	interface_ethernet->arp_table[j].ip = ip;
ready:
	memmove(interface_ethernet->arp_table[j].mac, mac, 6);
	interface_ethernet->arp_table[j].refresh_timeout = net_timer_get() + 30;
	interface_ethernet->arp_table[j].count = 0;
}

// ETHERNET
static void interface_ethernet_recv_ethernet_raw(int ifnum, interface_ethernet_t *interface_ethernet, const void *buf, int16_t count){
	ethernet_header_t ethernet_header;
	uint32_t src_ip;
	if(ethernet_parse_raw(&ethernet_header, buf, count) < 0)return;
	if(ethernet_header.type == ETHERNET_TYPE_ARP){
		interface_ethernet_recv_arp_ethernet_ipv4_raw(ifnum, interface_ethernet, ethernet_header.dest_mac, ethernet_header.src_mac, ethernet_header.data, ethernet_header.len);
	}else if(ethernet_header.type == ETHERNET_TYPE_IPV4){
		if(ethernet_header.len >= IPV4_HEADER_LEN){ // add temporary ARP record
			src_ip = ipv4_src_ip(ethernet_header.data);
			if((src_ip != 0) && ((src_ip & interface_ethernet->interface.nm) == interface_ethernet->interface.net) && (src_ip != interface_ethernet->interface.bc) && (src_ip != interface_ethernet->interface.net) && (src_ip != 0xFFFFFFFF)){ // add multicast exclusion
				interface_ethernet->arp_temp_ip = src_ip;
				memmove(interface_ethernet->arp_temp_mac, ethernet_header.src_mac, 6);
			}
		}
		net_recv_ipv4_raw(ifnum, ethernet_header.data, ethernet_header.len);
	}
}

void *interface_ethernet_recv_ethernet(int ifnum, int16_t *len){
	interface_ethernet_t *interface_ethernet;
	if(interface_type(ifnum) != INTERFACE_TYPE_ETHERNET)return((void *)0);
	interface_ethernet = interface_ethernet_get(ifnum);
	*len = interface_ethernet->len;
	return(interface_ethernet->data);
}

int16_t interface_ethernet_send_ethernet(int ifnum, const void *dest_mac, uint16_t type, const void *buf, int16_t count){
	interface_ethernet_t *interface_ethernet;
	if(interface_type(ifnum) != INTERFACE_TYPE_ETHERNET)return(NET_ERROR_HW);
	interface_ethernet = interface_ethernet_get(ifnum);
	count = ethernet_write(net_send_buffer_ethernet, net_send_buffer_ethernet_len, dest_mac, interface_ethernet->mac, type, buf, count);
	if(count < 0)return(count);
	return(interface_ethernet->send(net_send_buffer_ethernet, count));
}

// ARP
static void interface_ethernet_recv_arp_ethernet_ipv4_raw(int ifnum, interface_ethernet_t *interface_ethernet, const void *dest_mac, const void *src_mac, const void *buf, int16_t count){
	arp_ethernet_ipv4_header_t arp_ethernet_ipv4_header;
	int i;
	if(arp_parse_ethernet_ipv4_raw(&arp_ethernet_ipv4_header, buf, count) < 0)return;
	if(arp_ethernet_ipv4_header.oper == ARP_OPER_REQUEST){
		if(interface_ethernet->interface.ip == arp_ethernet_ipv4_header.target_ip){
			interface_ethernet_send_arp_ethernet_ipv4(ifnum, src_mac, ARP_OPER_REPLY, arp_ethernet_ipv4_header.sender_mac, interface_ethernet->mac, arp_ethernet_ipv4_header.sender_ip, interface_ethernet->interface.ip);
		}
	}else if(arp_ethernet_ipv4_header.oper == ARP_OPER_REPLY){
		for(i = 0; i < INTERFACE_ETHERNET_ARP_MAX; i++){
			if(arp_ethernet_ipv4_header.sender_ip == interface_ethernet->arp_table[i].ip){
				memmove(interface_ethernet->arp_table[i].mac, arp_ethernet_ipv4_header.sender_mac, 6);
				interface_ethernet->arp_table[i].refresh_timeout = net_timer_get() + 30;
				interface_ethernet->arp_table[i].count = 0;
				break;
			}
		}
	}
}


static int16_t interface_ethernet_send_arp_ethernet_ipv4(int ifnum, const void *dest_mac, uint16_t oper, void *target_mac, void *sender_mac, uint32_t target_ip, uint32_t sender_ip){
	int16_t count;
	count = arp_write_ethernet_ipv4(net_send_buffer_arp, net_send_buffer_arp_len, oper, target_mac, sender_mac, target_ip, sender_ip);
	if(count < 0)return(count);
	return(interface_ethernet_send_ethernet(ifnum, dest_mac, ETHERNET_TYPE_ARP, net_send_buffer_arp, count));
}

// IPV4
int16_t interface_ethernet_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count){
	interface_ethernet_t *interface_ethernet;
	int i, j;
	uint32_t lowest_used_timeout;
	uint32_t timer;
	int16_t len;
	if(interface_type(ifnum) != INTERFACE_TYPE_ETHERNET)return(NET_ERROR_HW);
	interface_ethernet = interface_ethernet_get(ifnum);
	if((gw == 0xFFFFFFFF) || (gw == interface_ethernet->interface.bc)){ // broadcast
		return(interface_ethernet_send_ethernet(ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ETHERNET_TYPE_IPV4, buf, count));
	}else if((gw & 0xF0000000) == 0xE0000000){ // multicast
		((uint8_t *)net_send_buffer_ethernet)[0] = 0x01;
		((uint8_t *)net_send_buffer_ethernet)[1] = 0x00;
		((uint8_t *)net_send_buffer_ethernet)[2] = 0x5E;
		((uint8_t *)net_send_buffer_ethernet)[3] = (gw >> 16) & 0x7F;
		((uint8_t *)net_send_buffer_ethernet)[4] = (gw >> 8) & 0xFF;
		((uint8_t *)net_send_buffer_ethernet)[5] = (gw >> 0) & 0xFF;
		return(interface_ethernet_send_ethernet(ifnum, net_send_buffer_ethernet, ETHERNET_TYPE_IPV4, buf, count));
	}else{
		timer = net_timer_get();
		for(j = 0; j < INTERFACE_ETHERNET_ARP_MAX; j++){
			if(gw == interface_ethernet->arp_table[j].ip){
				interface_ethernet->arp_table[j].used_timeout = timer + 10;
				if(memcmp(interface_ethernet->arp_table[j].mac, "\x00\x00\x00\x00\x00\x00", 6)){
					return(interface_ethernet_send_ethernet(ifnum, interface_ethernet->arp_table[j].mac, ETHERNET_TYPE_IPV4, buf, count));
				}
				break;
			}
		}
		i = 0;
		if(j == INTERFACE_ETHERNET_ARP_MAX){
			j = 0;
			lowest_used_timeout = 0xFFFFFFFF;
			for(i = 0; i < INTERFACE_ETHERNET_ARP_MAX; i++){
				if(interface_ethernet->arp_table[i].used_timeout < lowest_used_timeout){
					lowest_used_timeout = interface_ethernet->arp_table[i].used_timeout;
					j = i;
				}
			}
			interface_ethernet->arp_table[j].ip = gw;
			memset(interface_ethernet->arp_table[j].mac, 0, 6);
			interface_ethernet->arp_table[j].count = 0;
			interface_ethernet->arp_table[j].used_timeout = timer + 10;
			interface_ethernet->arp_table[j].refresh_timeout = timer + 2;
		}
		len = 0;
		if(gw == interface_ethernet->arp_temp_ip){
			// printf("WARNING, USING TEMP MAC\n");
			len = interface_ethernet_send_ethernet(ifnum, interface_ethernet->arp_temp_mac, ETHERNET_TYPE_IPV4, buf, count);
		}
		if(i){
			// printf("SENDING ARP REQUEST\n");
			interface_ethernet_send_arp_ethernet_ipv4(ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ARP_OPER_REQUEST, "\x00\x00\x00\x00\x00\x00", interface_ethernet->mac, interface_ethernet->arp_table[j].ip, interface_ethernet->interface.ip);
		}
		if(len >= 0)return(len); // tegelt kogu seda huinjaad poleks vaja, kui enc driver suudaks saata mitu paketti jutti
		if(interface_ethernet->arp_table[j].count > 5)return(NET_ERROR_NOROUTE); // no route to host
	}
	return(NET_ERROR_HW);
}

