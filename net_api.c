#include "net_api.h"
#include "net.h"
#include "dns.h"

static void *net_api_recv_resolv_buffer;
static uint16_t net_api_recv_resolv_len;

void net_api_reset(){
	net_api_recv_resolv_buffer = (void *)0;
	net_api_recv_resolv_len = 0;
}

// RESOLV
void net_api_resolv_query(const char *name, uint16_t len){
	net_send_resolv_query_a(name, len);
}

int net_api_resolv_done(const char *name, uint16_t len, uint32_t *ip){
	dns_section_t d;
	if(net_api_recv_resolv_buffer == (void *)0)return(0);
	for(dns_section_first(net_api_recv_resolv_buffer, net_api_recv_resolv_len, &d); dns_section_valid(&d); dns_section_next(&d)){
		if((dns_flags(net_api_recv_resolv_buffer) & 0x8000) && (d.section_type == DNS_SECTION_ANSWER) && (d.type == DNS_TYPE_A) && (d.class == 0x0001) && (d.rlength == 4)){
			if(dns_name_isequal(net_api_recv_resolv_buffer, net_api_recv_resolv_len, d.name, name, len)){
				*ip = ((uint32_t)((uint8_t *)net_api_recv_resolv_buffer)[d.rdata + 0] << 24) | ((uint32_t)((uint8_t *)net_api_recv_resolv_buffer)[d.rdata + 1] << 16) | ((uint32_t)((uint8_t *)net_api_recv_resolv_buffer)[d.rdata + 2] << 8) | ((uint32_t)((uint8_t *)net_api_recv_resolv_buffer)[d.rdata + 3] << 0);
				return(1);
			}
		}
	}
	return(0);
}

void net_api_recv_resolv(const void *recv_buffer, uint16_t len){
	net_api_recv_resolv_buffer = (void *)recv_buffer;
	net_api_recv_resolv_len = len;
}

// SOCKET
int net_api_socket_listen(uint16_t port){
	return(net_socket_listen(port));
}

void net_api_socket_nolisten(uint16_t port){
	net_socket_nolisten(port);
}

int net_api_socket_accept(uint16_t port){
	return(net_socket_accept(port));
}

int net_api_socket_connect(uint32_t dest_ip, uint16_t dest_port){
	return(net_socket_connect(dest_ip, dest_port));
}

int net_api_socket_connected(int id){
	return(net_socket_connected(id));
}

int net_api_socket_read(int id, void *buf, int count){
	return(net_socket_read(id, buf, count));
}

int net_api_socket_write(int id, const void *buf, int count){
	return(net_socket_write(id, buf, count));
}

int net_api_socket_write_pending(int id){
	return(net_socket_write_pending(id));
}

void net_api_socket_shutdown(int id){
	return(net_socket_shutdown(id));
}

void net_api_socket_close(int id){
	net_socket_close(id);
}

