#include "dhcp.h"
#include "net.h"

int dhcp_parse_raw(dhcp_header_t *header, const void *buf, int16_t count){
	if(count < DHCP_HEADER_LEN)return(0);
	if(dhcp_magic_cookie(buf) != DHCP_MAGIC_COOKIE)return(0);
	header->op = dhcp_op(buf);
	header->htype = dhcp_htype(buf);
	header->hlen = dhcp_hlen(buf);
	header->xid = dhcp_xid(buf);
//	header->ciaddr = dhcp_ciaddr(buf);
	header->yiaddr = dhcp_yiaddr(buf);
	header->siaddr = dhcp_siaddr(buf);
//	header->giaddr = dhcp_giaddr(buf);
	header->chaddr = dhcp_chaddr(buf);
	header->data = &((uint8_t *)buf)[DHCP_HEADER_LEN];
	header->len = count - DHCP_HEADER_LEN;
	return(1);
}

int16_t dhcp_write(void *dest, int16_t max, uint8_t op, uint32_t xid, uint32_t yiaddr, uint32_t siaddr, const void *chaddr, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((DHCP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[DHCP_HEADER_LEN], buf, count);
	dhcp_chaddr_set(dest, chaddr, 6);
	dhcp_op_set(dest, op);
	dhcp_htype_set(dest, DHCP_HTYPE_ETHERNET);
	dhcp_hlen_set(dest, 6);
	dhcp_hops_set(dest, 0);
	dhcp_xid_set(dest, xid);
	dhcp_secs_set(dest, 0);
	dhcp_flags_set(dest, 0x8000);
	dhcp_ciaddr_set(dest, 0x00000000);
	dhcp_yiaddr_set(dest, yiaddr);
	dhcp_siaddr_set(dest, siaddr);
	dhcp_giaddr_set(dest, 0x00000000);
	memset(&((uint8_t *)dest)[44], 0, 192); // BOOTP legacy to zero
	dhcp_magic_cookie_set(dest, DHCP_MAGIC_COOKIE);
	return(DHCP_HEADER_LEN + count);
}

int16_t dhcp_option_write(void *dest, int16_t max, int16_t offset, uint8_t type, const void *buf, uint8_t count){
	if(offset < 0)return(offset);
	if((offset + 2 + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[offset + 2], buf, count);
	((uint8_t *)dest)[offset + 0] = type;
	((uint8_t *)dest)[offset + 1] = count;
	return(offset + 2 + count);
}

int16_t dhcp_option_write_addr(void *dest, int16_t max, int16_t offset, uint8_t type, uint32_t addr){
	if(offset < 0)return(offset);
	if((offset + 2 + 4) > max)return(NET_ERROR_MEM);
	((uint8_t *)dest)[offset + 2] = ((addr >> 24) & 0xFF);
	((uint8_t *)dest)[offset + 3] = ((addr >> 16) & 0xFF);
	((uint8_t *)dest)[offset + 4] = ((addr >> 8) & 0xFF);
	((uint8_t *)dest)[offset + 5] = ((addr >> 0) & 0xFF);
	((uint8_t *)dest)[offset + 0] = type;
	((uint8_t *)dest)[offset + 1] = 4;
	return(offset + 2 + 4);
}

int16_t dhcp_option_write_end(void *dest, int16_t max, int16_t offset){
	if(offset < 0)return(offset);
	if((offset + 1) > max)return(NET_ERROR_MEM);
	((uint8_t *)dest)[offset + 0] = 0xFF;
	return(offset + 1);
}

void dhcp_option_first(dhcp_option_t *option, const void *buf, int16_t count){
	option->buf = (uint8_t *)buf;
	option->size = count;
	option->next = 0;
	dhcp_option_next(option);
}

uint8_t dhcp_option_valid(dhcp_option_t *option){
	return(option->option != 0xFF);
}

void dhcp_option_next(dhcp_option_t *option){
	if(option->size < (option->next + 2)){
		option->option = 0xFF;
		return;
	}
	option->buf += option->next;
	option->size -= option->next;
	option->len = option->buf[1];
	if(option->len > (option->size - 2)){
		option->option = 0xFF;
		return;
	}
	option->option = option->buf[0];
	option->param = &option->buf[2];
	option->next = option->len + 2;
}

