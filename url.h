#ifndef _URL_H_
#define _URL_H_

#include <stdint.h>

struct url_struct{
	uint16_t scheme;
	uint16_t scheme_len;
	uint16_t net_loc;
	uint16_t net_loc_len;
	uint16_t user;
	uint16_t user_len;
	uint16_t pass;
	uint16_t pass_len;
	uint16_t host;
	uint16_t host_len;
	uint16_t port;
	uint16_t port_len;
	uint16_t request;
	uint16_t request_len;
	uint16_t path;
	uint16_t path_len;
	uint16_t params;
	uint16_t params_len;
	uint16_t query;
	uint16_t query_len;
	uint16_t fragment;
	uint16_t fragment_len;
};

typedef struct url_struct url_t;

void url_parse(url_t *url, char *str, uint16_t len);

#endif

