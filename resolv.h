#ifndef _RESOLV_H_
#define _RESOLV_H_

#include <stdint.h>
#include "dns.h"

#define RESOLV_DEST_PORT 53

int resolv_recv_match_answer_a(dns_name_t *name, uint32_t *ip);
int resolv_recv_match_answer_a_cstr(const char *name, uint32_t *ip);
int resolv_recv_match_answer_cname(dns_name_t *name, dns_name_t *alias);
int resolv_recv_match_answer_cname_cstr(const char *name, dns_name_t *alias);
int resolv_recv_match_answer_cstr(const char *name, uint32_t *ip);
int16_t resolv_send_query_a(uint32_t ns, const char *name, int16_t count);
int16_t resolv_send_query_a_cstr(uint32_t ns, const char *name);

#endif

