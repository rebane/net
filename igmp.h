#ifndef _IGMP_H_
#define _IGMP_H_

#include <stdint.h>

#define IGMP_HEADER_LEN                  8

#define IGMP_TYPE_QUERY                  0x11
#define IGMP_TYPE_REPORT_V1              0x12
#define IGMP_TYPE_REPORT_V2              0x16
#define IGMP_TYPE_REPORT_V3              0x22
#define IGMP_TYPE_LEAVE                  0x17

#define IGMP_IPV4_PROTOCOL_IGMP          0x02

#define igmp_type(buf)                   (((uint8_t *)(buf))[0])
#define igmp_max_resp(buf)               (((uint8_t *)(buf))[1])
#define igmp_chk(buf)                    (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define igmp_group_address(buf)          (((uint32_t)((uint8_t *)(buf))[4] << 24) | ((uint32_t)((uint8_t *)(buf))[5] << 16) | ((uint32_t)((uint8_t *)(buf))[6] << 8) | ((uint32_t)((uint8_t *)(buf))[7] << 0))

#define igmp_type_set(buf, type)         (((uint8_t *)(buf))[0] = ((uint8_t)(type)))
#define igmp_max_resp_set(buf, max_resp) (((uint8_t *)(buf))[1] = ((uint8_t)(max_resp)))
#define igmp_chk_set(buf, chk)           do{ ((uint8_t *)(buf))[2] = (((uint16_t)(chk) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(chk) >> 0) & 0xFF); }while(0)
#define igmp_group_address_set(buf, ga)  do{ ((uint8_t *)(buf))[4] = (((uint32_t)(ga) >> 24) & 0xFF); ((uint8_t *)(buf))[5] = (((uint32_t)(ga) >> 16) & 0xFF); ((uint8_t *)(buf))[6] = (((uint32_t)(ga) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint32_t)(ga) >> 0) & 0xFF); }while(0)

struct igmp_header_struct{
	uint8_t type;
	uint8_t max_resp;
	uint32_t group_address;
	void *data;
	int16_t len;
};

typedef struct igmp_header_struct igmp_header_t;

int igmp_parse_raw(igmp_header_t *header, const void *buf, int16_t count);
int16_t igmp_write(void *dest, int16_t max, uint8_t type, uint8_t max_resp, uint32_t group_address, const void *buf, int16_t count);
uint16_t igmp_chk_calc(const void *buf, int16_t count);

#endif

