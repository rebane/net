#ifndef _PAP_H_
#define _PAP_H_

#include <stdint.h>

#define PAP_HEADER_LEN                 4

#define PAP_CODE_AUTH_REQUEST          1
#define PAP_CODE_AUTH_ACK              2
#define PAP_CODE_AUTH_NAK              3

#define pap_code(buf)                  (((uint8_t *)(buf))[0])
#define pap_identifier(buf)            (((uint8_t *)(buf))[1])
#define pap_len(buf)                   (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])

#define pap_code_set(buf, code)        (((uint8_t *)(buf))[0] = (code))
#define pap_identifier_set(buf, ident) (((uint8_t *)(buf))[1] = (ident))
#define pap_len_set(buf, len)          do{ ((uint8_t *)(buf))[2] = (((uint16_t)(len) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(len) >> 0) & 0xFF); }while(0)

struct pap_header_struct{
	uint8_t code;
	uint8_t identifier;
	union{
		void *message;
		void *peerid;
	};
	union{
		uint8_t message_len;
		uint8_t peerid_len;
	};
	void *passwd;
	uint8_t passwd_len;
};

typedef struct pap_header_struct pap_header_t;

int pap_parse_raw(pap_header_t *header, const void *buf, int16_t count);
int16_t pap_write(void *dest, int16_t max, uint8_t code, uint8_t identifier, const void *buf, uint8_t count, const void *buf2, uint8_t count2);

#endif

