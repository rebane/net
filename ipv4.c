#include "ipv4.h"
#include "net.h"
#include <string.h>

int ipv4_parse_raw(ipv4_header_t *header, const void *buf, int16_t count){
	uint16_t header_len;
	if(count < IPV4_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(ipv4_version(buf) != IPV4_VERSION_IPV4)return(NET_ERROR_FORMAT);
	header_len = ipv4_ihl(buf) * 4;
	if(header_len < IPV4_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(header_len > count)return(NET_ERROR_FORMAT);
	header->len = ipv4_len(buf);
	if(header->len > count)return(NET_ERROR_FORMAT);
	if(header->len < header_len)return(NET_ERROR_FORMAT);
	if(ipv4_chk(buf) != ipv4_chk_calc(buf))return(NET_ERROR_FORMAT);
	header->dest_ip = ipv4_dest_ip(buf);
	header->src_ip = ipv4_src_ip(buf);
	header->protocol = ipv4_protocol(buf);
	header->data = &((uint8_t *)buf)[header_len];
	header->len -= header_len;
	return(0);
}

int16_t ipv4_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint8_t protocol, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((IPV4_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[IPV4_HEADER_LEN], buf, count);
	ipv4_version_set(dest, IPV4_VERSION_IPV4);
	ipv4_ihl_set(dest, 5);
	ipv4_dscp_set(dest, 0);
	ipv4_ecn_set(dest, 0);
	ipv4_len_set(dest, IPV4_HEADER_LEN + count);
	ipv4_identification_set(dest, 0);
	ipv4_flags_set(dest, 0);
	ipv4_fragment_offset_set(dest, 0);
	if(protocol == IPV4_PROTOCOL_IGMP){
		ipv4_ttl_set(dest, 1);
	}else{
		ipv4_ttl_set(dest, 64);
	}
	ipv4_protocol_set(dest, protocol);
	ipv4_src_ip_set(dest, src_ip);
	ipv4_dest_ip_set(dest, dest_ip);
	ipv4_chk_set(dest, ipv4_chk_calc(dest));
	return(IPV4_HEADER_LEN + count);
}

uint16_t ipv4_chk_calc(const void *buf){
	uint32_t sum;
	uint16_t i;
	sum = 0;
	for(i = 0; i < (ipv4_ihl(buf) * 4); i += 2){
		if(i == 10)continue;
		sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	sum = (~sum) & 0xFFFF;
	return(sum);
}

