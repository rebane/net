#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <stdint.h>

#define SOCKET_RX_FIFO_SIZE      192
#define SOCKET_TX_FIFO_SIZE      392
#define SOCKET_TX_SIZE           64

#define SOCKET_STATUS_LISTEN     0
#define SOCKET_STATUS_CONNECTING 1
#define SOCKET_STATUS_CONNECTED  2
#define SOCKET_STATUS_CLOSED     3

struct socket_struct{
	uint32_t remote_ip;
	uint32_t local_ip;
	uint16_t remote_port;
	uint16_t local_port;
	struct{
		uint8_t state;
		uint32_t seq;
		uint8_t fifo[SOCKET_RX_FIFO_SIZE];
		int16_t fifo_len;
		int16_t fifo_in;
		int16_t fifo_out;
	}rx;
	struct{
		uint8_t state;
		uint32_t seq;
		uint32_t seq_req;
		uint16_t win;
		uint8_t fifo[SOCKET_TX_FIFO_SIZE];
		int16_t fifo_len;
		int16_t fifo_in;
		int16_t fifo_out;
		uint8_t buffer[SOCKET_TX_SIZE];
		int16_t len;
	}tx;
	uint8_t try;
	uint32_t timer;
	uint8_t send;
	uint8_t poll_send;
}__attribute__((packed));

typedef struct socket_struct socket_t;

void socket_init();
void socker_reset(socket_t *socket);
int socket_status(socket_t *socket);
int16_t socket_accept(socket_t *socket, uint16_t local_port);
int16_t socket_connect(socket_t *socket, uint32_t remote_ip, uint16_t remote_port);
int16_t socket_read(socket_t *socket, void *buf, int16_t count);
int16_t socket_write(socket_t *socket, const void *buf, int16_t count);
int16_t socket_shutdown(socket_t *socket);
void socket_poll(socket_t *socket);

#endif

