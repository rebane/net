#ifndef _ARP_H_
#define _ARP_H_

#include <stdint.h>
#include <string.h>

#define ARP_HEADER_LEN            8

#define ARP_HTYPE_ETHERNET        0x0001
#define ARP_PTYPE_IPV4            0x0800
#define ARP_HLEN_ETHERNET         6
#define ARP_PLEN_IPV4             4
#define ARP_OPER_REQUEST          0x0001
#define ARP_OPER_REPLY            0x0002

#define arp_htype(buf)            (((uint16_t)((uint8_t *)(buf))[0] << 8) | ((uint8_t *)(buf))[1])
#define arp_ptype(buf)            (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define arp_hlen(buf)             (((uint8_t *)(buf))[4])
#define arp_plen(buf)             (((uint8_t *)(buf))[5])
#define arp_oper(buf)             (((uint16_t)((uint8_t *)(buf))[6] << 8) | ((uint8_t *)(buf))[7])
#define arp_sha(buf)              (&((uint8_t *)(buf))[ARP_HEADER_LEN])
#define arp_spa(buf)              (&((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf))])
#define arp_spa_ipv4(buf)         (((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + 0] << 24) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + 1] << 16) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + 2] << 8) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + 3] << 0))
#define arp_tha(buf)              (&((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf))])
#define arp_tpa(buf)              (&((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf)) + arp_hlen((buf))])
#define arp_tpa_ipv4(buf)         (((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf)) + arp_hlen((buf)) + 0] << 24) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf)) + arp_hlen((buf)) + 1] << 16) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf)) + arp_hlen((buf)) + 2] << 8) | ((uint32_t)((uint8_t *)(buf))[ARP_HEADER_LEN + arp_hlen((buf)) + arp_plen((buf)) + arp_hlen((buf)) + 3] << 0))

#define arp_mac(arp_ha)           (arp_ha)
#define arp_ipv4(arp_pa)          (((uint32_t)((uint8_t *)(arp_pa))[0] << 24) | ((uint32_t)((uint8_t *)(arp_pa))[1] << 16) | ((uint32_t)((uint8_t *)(arp_pa))[2] << 8) | ((uint32_t)((uint8_t *)(arp_pa))[3] << 0))

#define arp_htype_set(buf, htype) do{ ((uint8_t *)(buf))[0] = (((uint16_t)(htype) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(htype) >> 0) & 0xFF); }while(0)
#define arp_ptype_set(buf, ptype) do{ ((uint8_t *)(buf))[2] = (((uint16_t)(ptype) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(ptype) >> 0) & 0xFF); }while(0)
#define arp_hlen_set(buf, hlen)   (((uint8_t *)(buf))[4] = ((uint8_t)(hlen)))
#define arp_plen_set(buf, plen)   (((uint8_t *)(buf))[5] = ((uint8_t)(plen)))
#define arp_oper_set(buf, oper)   do{ ((uint8_t *)(buf))[6] = (((uint16_t)(oper) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint16_t)(oper) >> 0) & 0xFF); }while(0)

struct arp_header_struct{
	uint16_t htype;
	uint16_t ptype;
	uint8_t hlen;
	uint8_t plen;
	uint16_t oper;
	void *sha;
	void *spa;
	void *tha;
	void *tpa;
};

struct arp_ethernet_ipv4_header_struct{
	uint16_t oper;
	void *sender_mac;
	uint32_t sender_ip;
	void *target_mac;
	uint32_t target_ip;
};

typedef struct arp_header_struct arp_header_t;
typedef struct arp_ethernet_ipv4_header_struct arp_ethernet_ipv4_header_t;

int arp_parse_raw(arp_header_t *header, const void *buf, int16_t count);
int arp_parse_ethernet_ipv4_raw(arp_ethernet_ipv4_header_t *header, const void *buf, int16_t count);
int16_t arp_write_ethernet_ipv4(void *dest, int16_t max, uint16_t oper, void *target_mac, void *sender_mac, uint32_t target_ip, uint32_t sender_ip);
int16_t arp_write(void *dest, int16_t max, uint16_t htype, uint16_t ptype, uint8_t hlen, uint8_t plen, uint16_t oper, void *tha, void *sha, void *tpa, void *spa);

#endif

