#include "interface.h"
#include "net.h"
#include "arp.h"

#define INTERFACE_MAX 3

static struct interface_list_struct{
	uint8_t type;
	struct interface_struct *interface;
	union{
		interface_ethernet_t *ethernet;
		interface_ppp_t *ppp;
		void *custom;
	};
}interface_list[INTERFACE_MAX];

static int interface_ifnum;

void interface_init(){
	int ifnum;
	for(ifnum = 0; ifnum < INTERFACE_MAX; ifnum++){
		interface_list[ifnum].type = INTERFACE_TYPE_NONE;
	}
	interface_ifnum = (INTERFACE_MAX - 1);
}

interface_t *interface_get(int ifnum){
	return(interface_list[ifnum].interface);
}

void *interface_get_custom(int ifnum){
	return(interface_list[ifnum].custom);
}

int interface_type(int ifnum){
	return(interface_list[ifnum].type);
}

int interface_add(int type, interface_t *interface, void *custom){
	int ifnum;
	for(ifnum = 0; ifnum < INTERFACE_MAX; ifnum++){
		if(interface_list[ifnum].type == INTERFACE_TYPE_NONE){
			interface_list[ifnum].type = type;
			interface_list[ifnum].interface = interface;
			interface_list[ifnum].interface->enabled = 0;
			interface_list[ifnum].interface->ip = 0;
			interface_list[ifnum].custom = custom;
			return(ifnum);
		}
	}
	return(NET_ERROR_MEM);
}

void interface_poll(){
	int i;
	for(i = 0; i < INTERFACE_MAX; i++){
		interface_ifnum++;
		if(interface_ifnum >= INTERFACE_MAX)interface_ifnum = 0;
		if(interface_list[interface_ifnum].type == INTERFACE_TYPE_NONE)continue;
		if(!interface_list[interface_ifnum].interface->enabled)continue;
		if(interface_list[interface_ifnum].type == INTERFACE_TYPE_ETHERNET){
			if(interface_ethernet_poll(interface_ifnum, interface_list[interface_ifnum].custom) > 0)break;
		}else if(interface_list[interface_ifnum].type == INTERFACE_TYPE_PPP){
			if(interface_ppp_poll(interface_ifnum, interface_list[interface_ifnum].custom) > 0)break;
		}
	}
}

void interface_link_set(int ifnum, uint8_t link){
	interface_list[ifnum].interface->link = link;
}

void interface_enable(int ifnum){
	interface_list[ifnum].interface->enabled = 1;
}

void interface_disable(int ifnum){
	interface_list[ifnum].interface->enabled = 0;
}

void interface_ipv4_set(int ifnum, uint32_t ip, uint32_t nm, uint32_t gw, uint32_t ns){
	interface_list[ifnum].interface->ip = ip;
	interface_list[ifnum].interface->nm = nm;
	interface_list[ifnum].interface->bc = (ip | ~nm);
	interface_list[ifnum].interface->net = ip & nm;
	interface_list[ifnum].interface->gw = gw;
	interface_list[ifnum].interface->ns = ns;
	net_print_ip("  IP:         ", interface_list[ifnum].interface->ip);
	net_print_ip("  NETMASK:    ", interface_list[ifnum].interface->nm);
	net_print_ip("  BROADCAST:  ", interface_list[ifnum].interface->bc);
	net_print_ip("  NET:        ", interface_list[ifnum].interface->net);
	net_print_ip("  GATEWAY:    ", interface_list[ifnum].interface->gw);
	net_print_ip("  NAMESERVER: ", interface_list[ifnum].interface->ns);
}

int16_t interface_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count){
	if(count < 0)return(count);
	if(interface_list[ifnum].type == INTERFACE_TYPE_ETHERNET)return(interface_ethernet_send_ipv4_raw(ifnum, gw, buf, count));
	if(interface_list[ifnum].type == INTERFACE_TYPE_PPP)return(interface_ppp_send_ipv4_raw(ifnum, gw, buf, count));
	return(NET_ERROR_HW);
}

// kui ifnum >= 0 siis tuleb kasutada seda interfeissi ja kui lisaks *src_ip == 0 siis tuleb leida vastav source ip interfeisi küljest
// kui ifnum < 0 siis tuleb leida õige interface ja kui lisaks *src_ip == 0 siis ka õige source ip
int interface_route_get(int ifnum, uint32_t dest_ip, uint32_t *src_ip, uint32_t *gw){
	int i;
	if(ifnum >= 0){
		if(ifnum >= INTERFACE_MAX)return(NET_ERROR_HW);
		if(*src_ip == 0)*src_ip = interface_list[ifnum].interface->ip;
		if((dest_ip == 0xFFFFFFFF) || (dest_ip == interface_list[ifnum].interface->bc) || ((dest_ip & 0xF0000000) == 0xE0000000)){ // sobib kui B/M-cast
			*gw = dest_ip;
		}else{
			*gw = interface_list[ifnum].interface->gw;
		}
		return(ifnum);
	}
	if(*src_ip == 0){ // leiame kust võrgu kaudu saaks ruutida
		for(i = 0; i < INTERFACE_MAX; i++){ // kõigepealt leiame kas sobib kuskile subnetti
			if(!interface_list[i].interface->enabled)continue;
			if(!interface_list[i].interface->link)continue;
			if(interface_list[i].interface->ip == 0)continue;
			if((dest_ip == 0xFFFFFFFF) || (dest_ip == interface_list[i].interface->bc) || ((dest_ip & 0xF0000000) == 0xE0000000)){ // sobib kui B/M-cast
				*src_ip = interface_list[i].interface->ip;
				*gw = dest_ip;
				return(i);
			}else if((dest_ip & interface_list[i].interface->nm) == interface_list[i].interface->net){ // sobib kui samas võrgus
				*src_ip = interface_list[i].interface->ip;
				*gw = dest_ip;
				return(i);
			}
		}
		for(i = 0; i < INTERFACE_MAX; i++){ // leiame mingi default route
			if(!interface_list[i].interface->enabled)continue;
			if(!interface_list[i].interface->link)continue;
			if(interface_list[i].interface->ip == 0)continue;
			if(interface_list[i].interface->gw == 0)continue;
			*src_ip = interface_list[i].interface->ip;
			*gw = interface_list[i].interface->gw;
			return(i);
		}
		return(NET_ERROR_NOROUTE); // no route
	}
	// leiame, mis interface on sourceks, kasutame seda interfeissi
	for(i = 0; i < INTERFACE_MAX; i++){
		if(!interface_list[i].interface->enabled)continue;
		if(interface_list[i].interface->ip == 0)continue;
		if(*src_ip != interface_list[i].interface->ip)continue;
		if((dest_ip == 0xFFFFFFFF) || (dest_ip == interface_list[i].interface->bc) || ((dest_ip & 0xF0000000) == 0xE0000000)){ // sobib kui B/M-cast
			*gw = dest_ip;
			return(i);
		}else if((dest_ip & interface_list[i].interface->nm) == interface_list[i].interface->net){ // sobib kui samas võrgus
			*gw = dest_ip;
			return(i);
		}else if(interface_list[i].interface->gw != 0){ // kui on default gw
			*gw = interface_list[i].interface->gw;
			return(i);
		}
	}
	// kui ip forward on enabletud
	return(NET_ERROR_NOROUTE);
}

int interface_route_is_local(int ifnum, uint32_t dest_ip){
	if(dest_ip == 0xFFFFFFFF)return(1); // broadcast
	if((dest_ip & 0xF0000000) == 0xE0000000)return(1); // multicast
	if(!interface_list[ifnum].interface->enabled)return(0);
	if(interface_list[ifnum].interface->ip == 0)return(0);
	if(dest_ip == interface_list[ifnum].interface->ip)return(1); // unicast
	if(dest_ip == interface_list[ifnum].interface->bc)return(1); // broadcast
	return(0);
}

uint32_t interface_ip_get(int ifnum){
	return(interface_list[ifnum].interface->ip);
}

int interface_ns_get(uint32_t *ns){
	uint32_t gw, src_ip;
	if(*ns){
		src_ip = 0;
		return(interface_route_get(-1, *ns, &src_ip, &gw));
	}
	*ns = 0xC0A80101;
	return(0);
}

