#ifndef _ICMP_H_
#define _ICMP_H_

#include <stdint.h>

#define ICMP_HEADER_LEN              8

#define ICMP_TYPE_ECHO_REPLY         0x00
#define ICMP_TYPE_ECHO_REQUEST       0x08

#define icmp_type(buf)               (((uint8_t *)(buf))[0])
#define icmp_code(buf)               (((uint8_t *)(buf))[1])
#define icmp_chk(buf)                (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define icmp_header(buf)             (&((uint8_t *)(buf))[4])

#define icmp_type_set(buf, type)     (((uint8_t *)(buf))[0] = ((uint8_t)(type)))
#define icmp_code_set(buf, code)     (((uint8_t *)(buf))[1] = ((uint8_t)(code)))
#define icmp_chk_set(buf, chk)       do{ ((uint8_t *)(buf))[2] = (((uint16_t)(chk) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(chk) >> 0) & 0xFF); }while(0)
#define icmp_header_set(buf, header) memmove(&((uint8_t *)(buf))[4], (header), 4)

struct icmp_header_struct{
	uint8_t type;
	uint8_t code;
	void *header;
	void *data;
	int16_t len;
};

typedef struct icmp_header_struct icmp_header_t;

int icmp_parse_raw(icmp_header_t *header, const void *buf, int16_t count);
int16_t icmp_write(void *dest, int16_t max, uint8_t type, uint8_t code, const void *header, const void *buf, int16_t count);
uint16_t icmp_chk_calc(const void *buf, int16_t count);

#endif

