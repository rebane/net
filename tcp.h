#ifndef _TCP_H_
#define _TCP_H_

#include <stdint.h>

#define TCP_HEADER_LEN                  20

#define TCP_IPV4_PROTOCOL_TCP           0x06

#define TCP_FLAG_FIN                    (((uint16_t)0x01) << 0)
#define TCP_FLAG_SYN                    (((uint16_t)0x01) << 1)
#define TCP_FLAG_RST                    (((uint16_t)0x01) << 2)
#define TCP_FLAG_PSH                    (((uint16_t)0x01) << 3)
#define TCP_FLAG_ACK                    (((uint16_t)0x01) << 4)
#define TCP_FLAG_URG                    (((uint16_t)0x01) << 5)
#define TCP_FLAG_ECE                    (((uint16_t)0x01) << 6)
#define TCP_FLAG_CWR                    (((uint16_t)0x01) << 7)
#define TCP_FLAG_NS                     (((uint16_t)0x01) << 8)

#define tcp_src_port(buf)               (((uint16_t)((uint8_t *)(buf))[0] << 8) | ((uint8_t *)(buf))[1])
#define tcp_dest_port(buf)              (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define tcp_seq_number(buf)             (((uint32_t)((uint8_t *)(buf))[4] << 24) | ((uint32_t)((uint8_t *)(buf))[5] << 16) | ((uint32_t)((uint8_t *)(buf))[6] << 8) | ((uint32_t)((uint8_t *)(buf))[7] << 0))
#define tcp_ack_number(buf)             (((uint32_t)((uint8_t *)(buf))[8] << 24) | ((uint32_t)((uint8_t *)(buf))[9] << 16) | ((uint32_t)((uint8_t *)(buf))[10] << 8) | ((uint32_t)((uint8_t *)(buf))[11] << 0))
#define tcp_data_offset(buf)            ((((uint8_t *)(buf))[12] >> 4) & 0x0F)
#define tcp_flags(buf)                  (((uint16_t)(((uint8_t *)(buf))[12] & 0x01) << 8) | ((uint8_t *)(buf))[13])
#define tcp_window_size(buf)            (((uint16_t)((uint8_t *)(buf))[14] << 8) | ((uint8_t *)(buf))[15])
#define tcp_chk(buf)                    (((uint16_t)((uint8_t *)(buf))[16] << 8) | ((uint8_t *)(buf))[17])
#define tcp_urgent_pointer(buf)         (((uint16_t)((uint8_t *)(buf))[18] << 8) | ((uint8_t *)(buf))[19])

#define tcp_src_port_set(buf, sp)       do{ ((uint8_t *)(buf))[0] = (((uint16_t)(sp) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(sp) >> 0) & 0xFF); }while(0)
#define tcp_dest_port_set(buf, dp)      do{ ((uint8_t *)(buf))[2] = (((uint16_t)(dp) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(dp) >> 0) & 0xFF); }while(0)
#define tcp_seq_number_set(buf, sn)     do{ ((uint8_t *)(buf))[4] = (((uint32_t)(sn) >> 24) & 0xFF); ((uint8_t *)(buf))[5] = (((uint32_t)(sn) >> 16) & 0xFF); ((uint8_t *)(buf))[6] = (((uint32_t)(sn) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint32_t)(sn) >> 0) & 0xFF); }while(0)
#define tcp_ack_number_set(buf, an)     do{ ((uint8_t *)(buf))[8] = (((uint32_t)(an) >> 24) & 0xFF); ((uint8_t *)(buf))[9] = (((uint32_t)(an) >> 16) & 0xFF); ((uint8_t *)(buf))[10] = (((uint32_t)(an) >> 8) & 0xFF); ((uint8_t *)(buf))[11] = (((uint32_t)(an) >> 0) & 0xFF); }while(0)
#define tcp_data_offset_set(buf, dof)   (((uint8_t *)(buf))[12] = (((uint8_t *)(buf))[12] & 0x0F) | (((uint8_t)(dof) << 4) & 0xF0))
#define tcp_flags_set(buf, flags)       do{ (((uint8_t *)(buf))[12] = (((uint8_t *)(buf))[12] & 0xF0) | (((uint16_t)(flags) >> 8) & 0x01)); ((uint8_t *)(buf))[13] = (((uint16_t)(flags) >> 0) & 0xFF); }while(0)
#define tcp_window_size_set(buf, ws)    do{ ((uint8_t *)(buf))[14] = (((uint16_t)(ws) >> 8) & 0xFF); ((uint8_t *)(buf))[15] = (((uint16_t)(ws) >> 0) & 0xFF); }while(0)
#define tcp_chk_set(buf, chk)           do{ ((uint8_t *)(buf))[16] = (((uint16_t)(chk) >> 8) & 0xFF); ((uint8_t *)(buf))[17] = (((uint16_t)(chk) >> 0) & 0xFF); }while(0)
#define tcp_urgent_pointer_set(buf, up) do{ ((uint8_t *)(buf))[18] = (((uint16_t)(up) >> 8) & 0xFF); ((uint8_t *)(buf))[19] = (((uint16_t)(up) >> 0) & 0xFF); }while(0)

struct tcp_header_struct{
	uint16_t dest_port;
	uint16_t src_port;
	uint32_t seq;
	uint32_t ack;
	uint16_t flags;
	uint16_t win;
	void *data;
	int16_t len;
};

typedef struct tcp_header_struct tcp_header_t;

int tcp_parse_raw(tcp_header_t *header, uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count);
int16_t tcp_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, uint32_t seq, uint32_t ack, uint16_t flags, uint16_t win, const void *buf, int16_t count);
uint16_t tcp_chk_calc(uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count);

#endif

