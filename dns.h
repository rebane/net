#ifndef _DNS_H_
#define _DNS_H_

#include <stdint.h>

#define DNS_HEADER_LEN                12

#define DNS_SECTION_ERROR             0
#define DNS_SECTION_QUESTION          1
#define DNS_SECTION_ANSWER            2
#define DNS_SECTION_AUTHORITY         3
#define DNS_SECTION_ADDITIONAL        4

#define DNS_TYPE_A                    0x0001
#define DNS_TYPE_CNAME                0x0005
#define DNS_TYPE_PTR                  0x000C

#define DNS_NAME_TYPE_STRING          0
#define DNS_NAME_TYPE_LABEL           1

#define dns_id(buf)                   (((uint16_t)((uint8_t *)(buf))[0] << 8) | (((uint8_t *)(buf))[1] << 0))
#define dns_flags(buf)                (((uint16_t)((uint8_t *)(buf))[2] << 8) | (((uint8_t *)(buf))[3] << 0))
#define dns_qdcount(buf)              (((uint16_t)((uint8_t *)(buf))[4] << 8) | (((uint8_t *)(buf))[5] << 0))
#define dns_ancount(buf)              (((uint16_t)((uint8_t *)(buf))[6] << 8) | (((uint8_t *)(buf))[7] << 0))
#define dns_nscount(buf)              (((uint16_t)((uint8_t *)(buf))[8] << 8) | (((uint8_t *)(buf))[9] << 0))
#define dns_arcount(buf)              (((uint16_t)((uint8_t *)(buf))[10] << 8) | (((uint8_t *)(buf))[11] << 0))

#define dns_id_set(buf, id)           do{ ((uint8_t *)(buf))[0] = (((uint16_t)(id) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(id) >> 0) & 0xFF); }while(0)
#define dns_flags_set(buf, flags)     do{ ((uint8_t *)(buf))[2] = (((uint16_t)(flags) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(flags) >> 0) & 0xFF); }while(0)
#define dns_qdcount_set(buf, qdcount) do{ ((uint8_t *)(buf))[4] = (((uint16_t)(qdcount) >> 8) & 0xFF); ((uint8_t *)(buf))[5] = (((uint16_t)(qdcount) >> 0) & 0xFF); }while(0)
#define dns_ancount_set(buf, ancount) do{ ((uint8_t *)(buf))[6] = (((uint16_t)(ancount) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint16_t)(ancount) >> 0) & 0xFF); }while(0)
#define dns_nscount_set(buf, nscount) do{ ((uint8_t *)(buf))[8] = (((uint16_t)(nscount) >> 8) & 0xFF); ((uint8_t *)(buf))[9] = (((uint16_t)(nscount) >> 0) & 0xFF); }while(0)
#define dns_arcount_set(buf, arcount) do{ ((uint8_t *)(buf))[10] = (((uint16_t)(arcount) >> 8) & 0xFF); ((uint8_t *)(buf))[11] = (((uint16_t)(arcount) >> 0) & 0xFF); }while(0)

#define dns_uint16_set(buf, value)    do{ ((uint8_t *)(buf))[0] = (((uint16_t)(value) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(value) >> 0) & 0xFF); }while(0)
#define dns_uint32_set(buf, value)    do{ ((uint8_t *)(buf))[0] = (((uint32_t)(value) >> 24) & 0xFF); ((uint8_t *)(buf))[1] = (((uint32_t)(value) >> 16) & 0xFF); ((uint8_t *)(buf))[2] = (((uint32_t)(value) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint32_t)(value) >> 0) & 0xFF); }while(0)

struct dns_section_struct{
	uint8_t section_type;
	uint16_t name;
	uint16_t type;
	uint16_t class;
	uint32_t ttl;
	uint16_t rlength;
	uint16_t rdata;
	uint8_t *buf;
	int16_t size;
	int16_t next;
	uint8_t section_num;
};

struct dns_label_struct{
	uint8_t valid;
	uint8_t length;
	uint16_t label;
	uint8_t *buf;
	int16_t size;
	int16_t next;
};

struct dns_name_struct{
	uint8_t type;
	union{
		char *name;
		uint8_t *buf;
	};
	union{
		int16_t len;
		int16_t count;
	};
	int16_t loc;
};

typedef struct dns_section_struct dns_section_t;
typedef struct dns_label_struct dns_label_t;
typedef struct dns_name_struct dns_name_t;

void dns_section_first(dns_section_t *section, const void *buf, int16_t count);
uint8_t dns_section_valid(dns_section_t *section);
void dns_section_next(dns_section_t *section);

void dns_label_first(dns_label_t *label, const void *buf, int16_t count, int16_t loc);
uint8_t dns_label_valid(dns_label_t *label);
void dns_label_next(dns_label_t *label);

dns_name_t *dns_name_cstr(dns_name_t *name, const char *cstr);
dns_name_t *dns_name_label(dns_name_t *name, const void *buf, int16_t count, int16_t loc);

uint8_t dns_name_isequal(dns_name_t *name1, dns_name_t *name2);

int16_t dns_name_encode(void *dest, int16_t max, const char *str, int16_t count);
int16_t dns_name_encode_cstr(void *dest, int16_t max, const char *str);

#endif

