#ifndef _DHCPD_H_
#define _DHCPD_H_

#include <stdint.h>

#define DHCPD_TABLE_SIZE 8

struct dhcpd_struct{
	int ifnum;
	uint32_t ip;
	uint32_t nm;
	uint32_t gw;
	uint32_t bc;
	uint32_t net;
	uint32_t ns;
	struct{
		uint8_t mac[6];
		uint32_t ip;
		uint32_t timestamp;
	}table[DHCPD_TABLE_SIZE];
};

typedef struct dhcpd_struct dhcpd_t;

void dhcpd_init(dhcpd_t *dhcpd, int ifnum);
void dhcpd_poll(dhcpd_t *dhcpd);

#endif

