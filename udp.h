#ifndef _UDP_H_
#define _UDP_H_

#include <stdint.h>

// #define UDP_USE_CHK

#define UDP_HEADER_LEN             8

#define UDP_IPV4_PROTOCOL_UDP      0x11

#define udp_src_port(buf)          (((uint16_t)((uint8_t *)(buf))[0] << 8) | ((uint8_t *)(buf))[1])
#define udp_dest_port(buf)         (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])
#define udp_len(buf)               (((uint16_t)((uint8_t *)(buf))[4] << 8) | ((uint8_t *)(buf))[5])
#define udp_chk(buf)               (((uint16_t)((uint8_t *)(buf))[6] << 8) | ((uint8_t *)(buf))[7])

#define udp_src_port_set(buf, sp)  do{ ((uint8_t *)(buf))[0] = (((uint16_t)(sp) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(sp) >> 0) & 0xFF); }while(0)
#define udp_dest_port_set(buf, dp) do{ ((uint8_t *)(buf))[2] = (((uint16_t)(dp) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(dp) >> 0) & 0xFF); }while(0)
#define udp_len_set(buf, len)      do{ ((uint8_t *)(buf))[4] = (((uint16_t)(len) >> 8) & 0xFF); ((uint8_t *)(buf))[5] = (((uint16_t)(len) >> 0) & 0xFF); }while(0)
#define udp_chk_set(buf, chk)      do{ ((uint8_t *)(buf))[6] = (((uint16_t)(chk) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint16_t)(chk) >> 0) & 0xFF); }while(0)

struct udp_header_struct{
	uint16_t dest_port;
	uint16_t src_port;
	void *data;
	int16_t len;
};

typedef struct udp_header_struct udp_header_t;

int udp_parse_raw(udp_header_t *header, uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count);
int16_t udp_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, int16_t count);
uint16_t udp_chk_calc(uint32_t dest_ip, uint32_t src_ip, const void *buf);

#endif

