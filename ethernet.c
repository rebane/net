#include "ethernet.h"
#include "net.h"
#include <string.h>

int ethernet_parse_raw(ethernet_header_t *header, const void *buf, int16_t count){
	if(count < ETHERNET_HEADER_LEN)return(NET_ERROR_FORMAT);
	header->dest_mac = ethernet_dest_mac(buf);
	header->src_mac = ethernet_src_mac(buf);
	header->type = ethernet_type(buf);
	header->data = &((uint8_t *)buf)[ETHERNET_HEADER_LEN];
	header->len = count - ETHERNET_HEADER_LEN;
	return(0);
}

int16_t ethernet_write(void *dest, int16_t max, const void *dest_mac, const void *src_mac, uint16_t type, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((ETHERNET_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[ETHERNET_HEADER_LEN], buf, count);
	ethernet_dest_mac_set(dest, dest_mac);
	ethernet_src_mac_set(dest, src_mac);
	ethernet_type_set(dest, type);
	return(ETHERNET_HEADER_LEN + count);
}

