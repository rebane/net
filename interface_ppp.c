#include "interface.h"
#include "net.h"

#define INTERFACE_PPP_STATE_OFF   0
#define INTERFACE_PPP_STATE_LCP   1
#define INTERFACE_PPP_STATE_PAP   2
#define INTERFACE_PPP_STATE_IPCP  3
#define INTERFACE_PPP_STATE_READY 4

#define INTERFACE_PPP_AUTH_NONE   0
#define INTERFACE_PPP_AUTH_PAP    1
#define INTERFACE_PPP_AUTH_CHAP   2

static void interface_ppp_write_fifo_put(interface_ppp_t *interface_ppp, uint8_t c);
static void interface_ppp_recv_ahdlc_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count);
static void interface_ppp_recv_lcp_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count);
static void interface_ppp_recv_pap_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count);
static void interface_ppp_recv_ipcp_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count);

int interface_ppp_add(interface_ppp_t *interface_ppp, int16_t(*read)(void *buf, int16_t count), int16_t(*write)(const void *buf, int16_t count)){
	int ifnum;
	ifnum = interface_add(INTERFACE_TYPE_PPP, &interface_ppp->interface, interface_ppp);
	interface_ppp->read = read;
	interface_ppp->write = write;
	interface_ppp->state = INTERFACE_PPP_STATE_OFF;
	return(ifnum);
}

interface_ppp_t *interface_ppp_get(int ifnum){
	return((interface_ppp_t *)interface_get_custom(ifnum));
}

int16_t interface_ppp_poll(int ifnum, interface_ppp_t *interface_ppp){
	int16_t i, l, len;
	uint8_t c;
	uint32_t timer;
	interface_ppp->ipv4_len = 0;
	for(i = 0; i < 16384;i++){
		l = interface_ppp->read(&c, 0);
		if(l < 0){
			if(interface_ppp->state != INTERFACE_PPP_STATE_OFF){
				printf("PPP: OFF\n");
				interface_link_set(ifnum, 0);
				interface_ppp->state = INTERFACE_PPP_STATE_OFF;
			}
			return(NET_ERROR_HW);
		}
		if(interface_ppp->state == INTERFACE_PPP_STATE_OFF){
			interface_ppp->read_esc = 0;
			interface_ppp->read_loc = 0;
			interface_ppp->write_fifo_in = interface_ppp->write_fifo_out = interface_ppp->write_fifo_len = 0;
			interface_ppp->ok_local = interface_ppp->ok_remote = 0;
			interface_ppp->ask_ns1 = interface_ppp->ask_ns2 = 1;
			interface_ppp->ip_local = interface_ppp->ip_remote = interface_ppp->ns1 = interface_ppp->ns2 = 0;
			interface_ppp->auth = INTERFACE_PPP_AUTH_NONE;
			interface_ppp->identifier = net_rand() % 0xFF;
			interface_ppp->timer = 0;
			interface_ppp->state = INTERFACE_PPP_STATE_LCP;
		}
		if(l == 0)break;
		if(interface_ppp->read_esc){
			if(c == 0x7E){
				interface_ppp->read_loc = 0;
				interface_ppp->read_esc = 0;
				printf("PPP: FRAME ERROR\n");
				continue;
			}
			interface_ppp->read_esc = 0;
			c ^= 0x20;
		}else if(c == 0x7E){
			interface_ppp_recv_ahdlc_raw(ifnum, interface_ppp, interface_ppp->read_buffer, interface_ppp->read_loc);
			interface_ppp->read_loc = 0;
			interface_ppp->read_esc = 0;
			if(interface_ppp->ipv4_len)break;
			continue;
		}else if(c == 0x7D){
			interface_ppp->read_esc = 1;
			continue;
		}
		if(interface_ppp->read_loc < INTERFACE_PPP_READ_BUFFER_LEN){
			interface_ppp->read_buffer[interface_ppp->read_loc++] = c;
		}
	}
	while(interface_ppp->write_fifo_len){
		l = interface_ppp->write(&interface_ppp->write_fifo[interface_ppp->write_fifo_out], 1);
		if(l < 0){
			if(interface_ppp->state != INTERFACE_PPP_STATE_OFF){
				printf("PPP: OFF\n");
				interface_link_set(ifnum, 0);
				interface_ppp->state = INTERFACE_PPP_STATE_OFF;
			}
			return(NET_ERROR_HW);
		}
		if(l == 0)break;
		interface_ppp->write_fifo_out++;
		if(interface_ppp->write_fifo_out >= INTERFACE_PPP_WRITE_FIFO_LEN)interface_ppp->write_fifo_out = 0;
		interface_ppp->write_fifo_len--;
	}
	if(interface_ppp->state == INTERFACE_PPP_STATE_LCP){
		timer = net_timer_get();
		if(!interface_ppp->ok_local && (timer > interface_ppp->timer)){
			interface_ppp_send_lcp(ifnum, PPP_LCP_CODE_CONFIGURE_REQUEST, interface_ppp->identifier, "", 0);
			interface_ppp->timer = timer + 5;
		}
		if(interface_ppp->ok_local && interface_ppp->ok_remote){
			interface_ppp->identifier = net_rand() % 0xFF;
			interface_ppp->timer = 0;
			if(interface_ppp->auth == INTERFACE_PPP_AUTH_PAP){
				printf("PPP: LCP OK, STARTING PAP\n");
				interface_ppp->state = INTERFACE_PPP_STATE_PAP;
			}else{
				printf("PPP: LCP OK, STARTING IPCP\n");
				interface_ppp->ok_local = interface_ppp->ok_remote = 0;
				interface_ppp->state = INTERFACE_PPP_STATE_IPCP;
			}
		}
	}
	if(interface_ppp->state == INTERFACE_PPP_STATE_PAP){
		timer = net_timer_get();
		if(timer > interface_ppp->timer){
			interface_ppp_send_pap(ifnum, PAP_CODE_AUTH_REQUEST, interface_ppp->identifier, "", 0, "", 0);
			interface_ppp->timer = timer + 5;
		}
	}
	if(interface_ppp->state == INTERFACE_PPP_STATE_IPCP){
		timer = net_timer_get();
		if(!interface_ppp->ok_local && (timer > interface_ppp->timer)){
			len = 0;
			len = ppp_parameter_write_addr(net_send_buffer_ppp_parameter, net_send_buffer_ppp_parameter_len, len, PPP_IPCP_TYPE_IP_ADDRESS, interface_ppp->ip_local);
			if(interface_ppp->ask_ns1){
				len = ppp_parameter_write_addr(net_send_buffer_ppp_parameter, net_send_buffer_ppp_parameter_len, len, PPP_IPCP_TYPE_DNS1, interface_ppp->ns1);
			}
			if(interface_ppp->ask_ns2){
				len = ppp_parameter_write_addr(net_send_buffer_ppp_parameter, net_send_buffer_ppp_parameter_len, len, PPP_IPCP_TYPE_DNS1, interface_ppp->ns2);
			}
			interface_ppp_send_ipcp(ifnum, PPP_IPCP_CODE_CONFIGURE_REQUEST, interface_ppp->identifier, net_send_buffer_ppp_parameter, len);
			interface_ppp->timer = timer + 5;
		}
		if(interface_ppp->ok_local && interface_ppp->ok_remote){
			printf("PPP: IPCP OK, PPP READY\n");
			interface_link_set(ifnum, 1);
			if(interface_ppp->ask_ns1 && interface_ppp->ns1){
				interface_ipv4_set(ifnum, interface_ppp->ip_local, 0xFFFFFFFF, interface_ppp->ip_remote, interface_ppp->ns1);
			}else{
				interface_ipv4_set(ifnum, interface_ppp->ip_local, 0xFFFFFFFF, interface_ppp->ip_remote, interface_ppp->ns2);
			}
			interface_ppp->state = INTERFACE_PPP_STATE_READY;
		}
	}
	return(interface_ppp->ipv4_len);
}

static void interface_ppp_write_fifo_put(interface_ppp_t *interface_ppp, uint8_t c){
	interface_ppp->write_fifo[interface_ppp->write_fifo_in] = c;
	interface_ppp->write_fifo_in++;
	if(interface_ppp->write_fifo_in >= INTERFACE_PPP_WRITE_FIFO_LEN)interface_ppp->write_fifo_in = 0;
	interface_ppp->write_fifo_len++;
}

// AHDLC
static void interface_ppp_recv_ahdlc_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count){
	ahdlc_header_t ahdlc_header;
	if(ahdlc_parse_raw(&ahdlc_header, buf, count) < 0)return;
	if(ahdlc_header.protocol == AHDLC_PROTOCOL_LCP){
		interface_ppp_recv_lcp_raw(ifnum, interface_ppp, ahdlc_header.data, ahdlc_header.len);
	}else if(ahdlc_header.protocol == AHDLC_PROTOCOL_PAP){
		interface_ppp_recv_pap_raw(ifnum, interface_ppp, ahdlc_header.data, ahdlc_header.len);
	}else if(ahdlc_header.protocol == AHDLC_PROTOCOL_IPCP){
		interface_ppp_recv_ipcp_raw(ifnum, interface_ppp, ahdlc_header.data, ahdlc_header.len);
	}else if(ahdlc_header.protocol == AHDLC_PROTOCOL_IPV4){
		if(interface_ppp->state == INTERFACE_PPP_STATE_READY){
			interface_ppp->ipv4_len = ahdlc_header.len;
			net_recv_ipv4_raw(ifnum, ahdlc_header.data, ahdlc_header.len);
		}
	}
}

int16_t interface_ppp_send_ahdlc(int ifnum, uint16_t protocol, const void *buf, int16_t count){
	int16_t i, len;
	uint8_t c;
	interface_ppp_t *interface_ppp;
	count = ahdlc_write(net_send_buffer_ahdlc, net_send_buffer_ahdlc_len, protocol, buf, count);
	if(count < 0)return(count);
	interface_ppp = interface_ppp_get(ifnum);
	len = 2;
	for(i = 0; i < count; i++){
		c = ((uint8_t *)net_send_buffer_ahdlc)[i];
		if((c < 32) || (c == 0x7D) || (c == 0x7F))len++;
		len++;
	}
	if(len > (INTERFACE_PPP_WRITE_FIFO_LEN - interface_ppp->write_fifo_len)){
		printf("PPP: FIFO FULL\n");
		return(NET_ERROR_HW);
	}
	interface_ppp_write_fifo_put(interface_ppp, 0x7E);
	for(i = 0; i < count; i++){
		c = ((uint8_t *)net_send_buffer_ahdlc)[i];
		if((c < 32) || (c == 0x7D) || (c == 0x7F)){
			interface_ppp_write_fifo_put(interface_ppp, 0x7D);
			c ^= 0x20;
		}
		interface_ppp_write_fifo_put(interface_ppp, c);
	}
	interface_ppp_write_fifo_put(interface_ppp, 0x7E);
	return(count);
}

static void interface_ppp_recv_lcp_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count){
	ppp_header_t ppp_header;
	ppp_parameter_t ppp_parameter;
	int16_t len;
	if(ppp_parse_raw(&ppp_header, buf, count) < 0)return;
	/*printf("NET: LCP: CODE: %u, IDENTIFIER: %u, LEN: %u, DATA: %01H\n", (unsigned int)ppp_header.code, (unsigned int)ppp_header.identifier, (unsigned int)ppp_header.len, ppp_header.data, (int)ppp_header.len);
	for(ppp_parameter_first(ppp_header.data, ppp_header.len, &ppp_parameter); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
		printf("NET: LCP: PARAMTER: TYPE: %u, LEN: %u, DATA: %01H\n", (unsigned int)ppp_parameter.type, (unsigned int)ppp_parameter.len, ppp_parameter.data, (int)ppp_parameter.len);
	}*/
	if(interface_ppp->state != INTERFACE_PPP_STATE_LCP)return;
	if(ppp_header.code == PPP_LCP_CODE_CONFIGURE_ACK){
		if(ppp_header.identifier == interface_ppp->identifier){
			interface_ppp->ok_local = 1;
		}
	}else if(ppp_header.code == PPP_LCP_CODE_CONFIGURE_REQUEST){
		len = 0;
		for(ppp_parameter_first(&ppp_parameter, ppp_header.data, ppp_header.len); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
			if(ppp_parameter.type == PPP_LCP_TYPE_ASYNC_MAP){
				continue;
			}else if(ppp_parameter.type == PPP_LCP_TYPE_AUTH_PROTOCOL){
				if((ppp_parameter.len == 2) && (ppp_parameter.data[0] == 0xC0) && (ppp_parameter.data[1] == 0x23)){
					interface_ppp->auth = INTERFACE_PPP_AUTH_PAP;
					continue;
				}
			}else if(ppp_parameter.type == PPP_LCP_TYPE_PROTOCOL_FIELD_COMP){
				continue;
			}else if(ppp_parameter.type == PPP_LCP_TYPE_ADDR_CTRL_FIELD_COMP){
				continue;
			}
			len = ppp_parameter_write(net_send_buffer_ppp_parameter, net_send_buffer_ppp_parameter_len, len, ppp_parameter.type, ppp_parameter.data, ppp_parameter.len);
		}
		if(len > 0){
			interface_ppp_send_lcp(ifnum, PPP_LCP_CODE_CONFIGURE_REJECT, ppp_header.identifier, net_send_buffer_ppp_parameter, len);
		}else{
			interface_ppp_send_lcp(ifnum, PPP_LCP_CODE_CONFIGURE_ACK, ppp_header.identifier, ppp_header.data, ppp_header.len);
			interface_ppp->ok_remote = 1;
		}
	}
}

static void interface_ppp_recv_pap_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count){
	pap_header_t pap_header;
	if(pap_parse_raw(&pap_header, buf, count) < 0)return;
	// printf("NET: PAP: CODE: %u, IDENTIFIER: %u, LEN: %u, DATA: %01H, LEN2: %u, DATA2: %01H\n", (unsigned int)pap_header.code, (unsigned int)pap_header.identifier, (unsigned int)pap_header.message_len, pap_header.message, (int)pap_header.message_len, (unsigned int)pap_header.passwd_len, pap_header.passwd, (int)pap_header.passwd_len);
	if(interface_ppp->state != INTERFACE_PPP_STATE_PAP)return;
	if(pap_header.code == PAP_CODE_AUTH_ACK){
		printf("PPP: PAP OK, STARTING IPCP\n");
		interface_ppp->ok_local = interface_ppp->ok_remote = 0;
		interface_ppp->identifier = net_rand() % 0xFF;
		interface_ppp->timer = 0;
		interface_ppp->state = INTERFACE_PPP_STATE_IPCP;
	}else if(pap_header.code == PAP_CODE_AUTH_NAK){
		printf("PPP: PAP NOT OK\n");
	}
}

static void interface_ppp_recv_ipcp_raw(int ifnum, interface_ppp_t *interface_ppp, const void *buf, int16_t count){
	ppp_header_t ppp_header;
	ppp_parameter_t ppp_parameter;
	int16_t len;
	if(ppp_parse_raw(&ppp_header, buf, count) < 0)return;
	/*printf("NET: IPCP: CODE: %u, IDENTIFIER: %u, LEN: %u, DATA: %01H\n", (unsigned int)ppp_header.code, (unsigned int)ppp_header.identifier, (unsigned int)ppp_header.len, ppp_header.data, (int)ppp_header.len);
	for(ppp_parameter_first(ppp_header.data, ppp_header.len, &ppp_parameter); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
		printf("NET: IPCP: PARAMTER: TYPE: %u, LEN: %u, DATA: %01H\n", (unsigned int)ppp_parameter.type, (unsigned int)ppp_parameter.len, ppp_parameter.data, (int)ppp_parameter.len);
	}*/
	if(interface_ppp->state != INTERFACE_PPP_STATE_IPCP)return;
	if(ppp_header.code == PPP_IPCP_CODE_CONFIGURE_ACK){
		if(ppp_header.identifier == interface_ppp->identifier){
			for(ppp_parameter_first(&ppp_parameter, ppp_header.data, ppp_header.len); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
				if(ppp_parameter.type == PPP_IPCP_TYPE_IP_ADDRESS){
					if(ppp_parameter.len == 4){
						interface_ppp->ip_local = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}else if(ppp_parameter.type == PPP_IPCP_TYPE_DNS1){
					if(ppp_parameter.len == 4){
						interface_ppp->ns1 = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}else if(ppp_parameter.type == PPP_IPCP_TYPE_DNS2){
					if(ppp_parameter.len == 4){
						interface_ppp->ns2 = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}
			}
			interface_ppp->ok_local = 1;
		}
	}else if(ppp_header.code == PPP_IPCP_CODE_CONFIGURE_NAK){
		if(ppp_header.identifier == interface_ppp->identifier){
			for(ppp_parameter_first(&ppp_parameter, ppp_header.data, ppp_header.len); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
				if(ppp_parameter.type == PPP_IPCP_TYPE_IP_ADDRESS){
					if(ppp_parameter.len == 4){
						interface_ppp->ip_local = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}else if(ppp_parameter.type == PPP_IPCP_TYPE_DNS1){
					if(ppp_parameter.len == 4){
						interface_ppp->ns1 = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}else if(ppp_parameter.type == PPP_IPCP_TYPE_DNS2){
					if(ppp_parameter.len == 4){
						interface_ppp->ns2 = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					}
				}
			}
			interface_ppp->identifier = net_rand() % 0xFF;
			interface_ppp->timer = 0;
		}
	}else if(ppp_header.code == PPP_IPCP_CODE_CONFIGURE_REJECT){
		if(ppp_header.identifier == interface_ppp->identifier){
			for(ppp_parameter_first(&ppp_parameter, ppp_header.data, ppp_header.len); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
				if(ppp_parameter.type == PPP_IPCP_TYPE_DNS1){
					interface_ppp->ask_ns1 = 0;
					interface_ppp->identifier = net_rand() % 0xFF;
					interface_ppp->timer = 0;
				}else if(ppp_parameter.type == PPP_IPCP_TYPE_DNS2){
					interface_ppp->ask_ns2 = 0;
					interface_ppp->identifier = net_rand() % 0xFF;
					interface_ppp->timer = 0;
				}
			}
		}
	}else if(ppp_header.code == PPP_IPCP_CODE_CONFIGURE_REQUEST){
		len = 0;
		for(ppp_parameter_first(&ppp_parameter, ppp_header.data, ppp_header.len); ppp_parameter_valid(&ppp_parameter); ppp_parameter_next(&ppp_parameter)){
			if(ppp_parameter.type == PPP_IPCP_TYPE_IP_ADDRESS){
				if(ppp_parameter.len == 4){
					interface_ppp->ip_remote = net_ipv4_addr(ppp_parameter.data[0], ppp_parameter.data[1], ppp_parameter.data[2], ppp_parameter.data[3]);
					interface_ppp->ok_remote = 1;
					continue;
				}
			}
			len = ppp_parameter_write(net_send_buffer_ppp_parameter, net_send_buffer_ppp_parameter_len, len, ppp_parameter.type, ppp_parameter.data, ppp_parameter.len);
		}
		if(len > 0){
			interface_ppp_send_ipcp(ifnum, PPP_IPCP_CODE_CONFIGURE_REJECT, ppp_header.identifier, net_send_buffer_ppp_parameter, len);
		}else{
			interface_ppp_send_ipcp(ifnum, PPP_IPCP_CODE_CONFIGURE_ACK, ppp_header.identifier, ppp_header.data, ppp_header.len);
		}
	}
}

int16_t interface_ppp_send_lcp(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count){
	count = ppp_write(net_send_buffer_ppp, net_send_buffer_ppp_len, code, identifier, buf, count);
	if(count < 0)return(count);
	return(interface_ppp_send_ahdlc(ifnum, AHDLC_PROTOCOL_LCP, net_send_buffer_ppp, count));
}

int16_t interface_ppp_send_pap(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count, const void *buf2, int16_t count2){
	count = pap_write(net_send_buffer_pap, net_send_buffer_pap_len, code, identifier, buf, count, buf2, count2);
	if(count < 0)return(count);
	return(interface_ppp_send_ahdlc(ifnum, AHDLC_PROTOCOL_PAP, net_send_buffer_pap, count));
}

int16_t interface_ppp_send_ipcp(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count){
	count = ppp_write(net_send_buffer_ppp, net_send_buffer_ppp_len, code, identifier, buf, count);
	if(count < 0)return(count);
	return(interface_ppp_send_ahdlc(ifnum, AHDLC_PROTOCOL_IPCP, net_send_buffer_ppp, count));
}

int16_t interface_ppp_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count){
	int16_t len;
	interface_ppp_t *interface_ppp;
	interface_ppp = interface_ppp_get(ifnum);
	if(interface_ppp->state != INTERFACE_PPP_STATE_READY)return(NET_ERROR_NOROUTE);
	len = interface_ppp_send_ahdlc(ifnum, AHDLC_PROTOCOL_IPV4, buf, count);
	return(len);
}

