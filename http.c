#include "http.h"
#include <stdio.h>
#include <ctype.h>

// TODO: callbackile flag mis näitaks et antud token overflows
// TODO: callbackile counter, et mitmendal sisendbaidil antud token asub ?? tehtud?
// TODO: callbackile reaalne tokeni length ka juurde (kui oli overflow)
// TODO: content-length handling

#define HTTP_STATE_REQUEST_METHOD_WAIT   1
#define HTTP_STATE_REQUEST_METHOD        2
#define HTTP_STATE_REQUEST_URL_WAIT      3
#define HTTP_STATE_REQUEST_URL           4
#define HTTP_STATE_REQUEST_QUERY_KEY     5
#define HTTP_STATE_REQUEST_QUERY_VALUE   6
#define HTTP_STATE_REQUEST_VERSION_WAIT  7
#define HTTP_STATE_REQUEST_VERSION       8
#define HTTP_STATE_RESPONSE_VERSION_WAIT 9
#define HTTP_STATE_RESPONSE_VERSION      10
#define HTTP_STATE_RESPONSE_STATUS_WAIT  11
#define HTTP_STATE_RESPONSE_STATUS       12
#define HTTP_STATE_RESPONSE_REASON_WAIT  13
#define HTTP_STATE_RESPONSE_REASON       14
#define HTTP_STATE_CR_WAIT               15
#define HTTP_STATE_LF_WAIT               16
#define HTTP_STATE_PARAM_WAIT            17
#define HTTP_STATE_PARAM                 18
#define HTTP_STATE_VALUE_WAIT            19
#define HTTP_STATE_VALUE                 20
#define HTTP_STATE_BODY                  21

static char http_empty = 0;
static char *http_content_len = "Content-Length";
static uint8_t http_hex2bin(char hex);

void http_init(http_t *hh, void *buf, int16_t count, uint8_t mode){
	hh->mode = mode;
	if(hh->mode == HTTP_MODE_REQUEST){
		hh->state = HTTP_STATE_REQUEST_METHOD_WAIT;
	}else{
		hh->state = HTTP_STATE_RESPONSE_VERSION_WAIT;
	}
	hh->cl = 0;
	hh->parse_loc = 0;
	hh->content_len = 0;
	hh->buffer_len = count;
	hh->buffer = (uint8_t *)buf;
}

void http_set_buffer(http_t *hh, void *buf, int16_t count){
	hh->buffer_len = count;
	hh->buffer = (uint8_t *)buf;
}

uint8_t http_parse(http_t *hh, const void *buf, int16_t count, void *user, uint8_t(*callback)(http_t *, int, int32_t, int16_t, const void *, int16_t, void *)){
	uint8_t c;
	int16_t i;
	int32_t l;
	i = 0;
	if(hh->state == HTTP_STATE_BODY){
		if(hh->cl == 255){
			l = (hh->content_len - hh->token_len);
			if(l > (count - i))l = (count - i);
			if(l){
				if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, l, &((uint8_t *)buf)[i], l, user))return(1);
				hh->parse_loc += l;
				hh->token_len += l;
				i += l;
			}
			if(hh->token_len == hh->content_len){
				if(callback(hh, HTTP_EVENT_BODY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(hh->mode == HTTP_MODE_REQUEST){
					hh->state = HTTP_STATE_REQUEST_METHOD_WAIT;
				}else{
					hh->state = HTTP_STATE_RESPONSE_VERSION_WAIT;
				}
				hh->cl = 0;
				hh->content_len = 0;
			}
		}else{
			if(count - i){
				if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, count - i, &((uint8_t *)buf)[0], count - i, user))return(1);
				hh->parse_loc += count;
			}
			return(0);
		}
	}
	for( ; i < count; i++, hh->parse_loc++){
		c = ((uint8_t *)buf)[i];
		if(hh->state == HTTP_STATE_REQUEST_METHOD_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_METHOD, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_URL, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_len = hh->buffer_loc = 0;
				hh->token_start = hh->parse_loc;
				hh->state = HTTP_STATE_REQUEST_METHOD;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_METHOD){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_METHOD, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_METHOD, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_URL, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_METHOD, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_METHOD, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->state = HTTP_STATE_REQUEST_URL_WAIT;
				continue;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_URL_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_URL, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->esc = 0;
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_REQUEST_URL;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_URL){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_REQUEST_VERSION_WAIT;
				continue;
			}else if(c == '?'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_URL, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->esc = 0;
				hh->token_start = hh->parse_loc + 1;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_REQUEST_QUERY_KEY;
				continue;
			}else if(c == '+'){
				hh->esc = 0;
				c = ' ';
			}else if(c == '%'){
				hh->esc = 1;
				hh->token_len++;
				continue;
			}else if(hh->esc == 1){
				hh->c = http_hex2bin(c);
				hh->esc = 2;
				hh->token_len++;
				continue;
			}else if(hh->esc == 2){
				c = (hh->c << 4) | http_hex2bin(c);
				hh->esc = 0;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_QUERY_KEY){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_REQUEST_VERSION_WAIT;
				continue;
			}else if(c == '&'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->esc = 0;
				hh->token_start = hh->parse_loc + 1;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_REQUEST_QUERY_KEY;
				continue;
			}else if(c == '='){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_KEY, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->esc = 0;
				hh->token_start = hh->parse_loc + 1;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_REQUEST_QUERY_VALUE;
				continue;
			}else if(c == '+'){
				hh->esc = 0;
				c = ' ';
			}else if(c == '%'){
				hh->esc = 1;
				hh->token_len++;
				continue;
			}else if(hh->esc == 1){
				hh->c = http_hex2bin(c);
				hh->esc = 2;
				hh->token_len++;
				continue;
			}else if(hh->esc == 2){
				c = (hh->c << 4) | http_hex2bin(c);
				hh->esc = 0;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_QUERY_VALUE){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_QUERY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_REQUEST_VERSION_WAIT;
				continue;
			}else if(c == '&'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_QUERY_VALUE, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->esc = 0;
				hh->token_start = hh->parse_loc + 1;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_REQUEST_QUERY_KEY;
				continue;
			}else if(c == '+'){
				hh->esc = 0;
				c = ' ';
			}else if(c == '%'){
				hh->esc = 1;
				hh->token_len++;
				continue;
			}else if(hh->esc == 1){
				hh->c = http_hex2bin(c);
				hh->esc = 2;
				hh->token_len++;
				continue;
			}else if(hh->esc == 2){
				c = (hh->c << 4) | http_hex2bin(c);
				hh->esc = 0;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_VERSION_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				hh->state = HTTP_STATE_REQUEST_VERSION;
			}
		}else if(hh->state == HTTP_STATE_REQUEST_VERSION){
			if(c == '\n'){
				hh->token_len -= hh->space_len;
				if(hh->token_len < hh->buffer_loc)hh->buffer_loc = hh->token_len;
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_REQUEST_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				hh->space_len++;
			}else{
				hh->space_len = 0;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_VERSION_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_VERSION, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REASON, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_RESPONSE_VERSION;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_VERSION){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_STATUS, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REASON, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_VERSION, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->state = HTTP_STATE_RESPONSE_STATUS_WAIT;
				continue;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_STATUS_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_STATUS, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_REASON, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->state = HTTP_STATE_RESPONSE_STATUS;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_STATUS){
			if(c == '\n'){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_STATUS, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_STATUS, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_REASON, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_STATUS, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_STATUS, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->state = HTTP_STATE_RESPONSE_REASON_WAIT;
				continue;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_REASON_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_REASON, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				hh->state = HTTP_STATE_RESPONSE_REASON;
			}
		}else if(hh->state == HTTP_STATE_RESPONSE_REASON){
			if(c == '\n'){
				hh->token_len -= hh->space_len;
				if(hh->token_len < hh->buffer_loc)hh->buffer_loc = hh->token_len;
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_REASON, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_REASON, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_STATUS_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				hh->space_len++;
			}else{
				hh->space_len = 0;
			}
		}else if(hh->state == HTTP_STATE_CR_WAIT){
			if(c == '\r'){
				hh->state = HTTP_STATE_LF_WAIT;
				continue;
			}else if(c == '\n'){
				if(callback(hh, HTTP_EVENT_HEADER_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->token_len = 0;
				hh->state = HTTP_STATE_BODY;
				i++;
				if(hh->cl == 255){
					l = (hh->content_len - hh->token_len);
					if(l > (count - i))l = (count - i);
					if(l){
						if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, l, &((uint8_t *)buf)[i], l, user))return(1);
						hh->parse_loc += l;
						hh->token_len += l;
						i += l;
					}
					if(hh->token_len == hh->content_len){
						if(callback(hh, HTTP_EVENT_BODY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
						if(hh->mode == HTTP_MODE_REQUEST){
							hh->state = HTTP_STATE_REQUEST_METHOD_WAIT;
						}else{
							hh->state = HTTP_STATE_RESPONSE_VERSION_WAIT;
						}
						hh->cl = 0;
						hh->content_len = 0;
					}
				}else{
					if(count - i){
						if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, count - i, &((uint8_t *)buf)[0], count - i, user))return(1);
						hh->parse_loc += count;
					}
					return(0);
				}
			}else if(isspace(c)){
				hh->state = HTTP_STATE_PARAM_WAIT;
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				if(hh->cl != 255)hh->cl = 0;
				hh->state = HTTP_STATE_PARAM;
			}
		}else if(hh->state == HTTP_STATE_LF_WAIT){
			if(c == '\r'){
				continue;
			}else if(c == '\n'){
				if(callback(hh, HTTP_EVENT_HEADER_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->token_len = 0;
				hh->state = HTTP_STATE_BODY;
				i++;
				if(hh->cl == 255){
					l = (hh->content_len - hh->token_len);
					if(l > (count - i))l = (count - i);
					if(l){
						if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, l, &((uint8_t *)buf)[i], l, user))return(1);
						hh->parse_loc += l;
						hh->token_len += l;
						i += l;
					}
					if(hh->token_len == hh->content_len){
						if(callback(hh, HTTP_EVENT_BODY_DONE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
						if(hh->mode == HTTP_MODE_REQUEST){
							hh->state = HTTP_STATE_REQUEST_METHOD_WAIT;
						}else{
							hh->state = HTTP_STATE_RESPONSE_VERSION_WAIT;
						}
						hh->cl = 0;
						hh->content_len = 0;
					}
				}else{
					if(count - i){
						if(callback(hh, HTTP_EVENT_BODY, hh->parse_loc, count - i, &((uint8_t *)buf)[0], count - i, user))return(1);
						hh->parse_loc += count;
					}
					return(0);
				}
			}else if(isspace(c)){
				hh->state = HTTP_STATE_PARAM_WAIT;
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				if(hh->cl != 255)hh->cl = 0;
				hh->state = HTTP_STATE_PARAM;
			}
		}else if(hh->state == HTTP_STATE_PARAM_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_PARAM, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(callback(hh, HTTP_EVENT_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				if(hh->cl != 255)hh->cl = 0;
				hh->state = HTTP_STATE_PARAM;
			}
		}else if(hh->state == HTTP_STATE_PARAM){
			if(c == '\n'){
				hh->token_len -= hh->space_len;
				if(hh->token_len < hh->buffer_loc)hh->buffer_loc = hh->token_len;
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_PARAM, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_PARAM, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(callback(hh, HTTP_EVENT_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(c == ':'){
				hh->token_len -= hh->space_len;
				if(hh->token_len < hh->buffer_loc)hh->buffer_loc = hh->token_len;
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_PARAM, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_PARAM, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				hh->state = HTTP_STATE_VALUE_WAIT;
				continue;
			}else if(isspace(c)){
				hh->space_len++;
			}else{
				hh->space_len = 0;
			}
		}else if(hh->state == HTTP_STATE_VALUE_WAIT){
			if(c == '\n'){
				if(callback(hh, HTTP_EVENT_VALUE, hh->parse_loc, 0, &http_empty, 0, user))return(1);
				if(hh->cl == 14)hh->cl = 255;
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				continue;
			}else{
				hh->token_start = hh->parse_loc;
				hh->token_len = hh->buffer_loc = 0;
				hh->space_len = 0;
				hh->state = HTTP_STATE_VALUE;
			}
		}else if(hh->state == HTTP_STATE_VALUE){
			if(c == '\n'){
				hh->token_len -= hh->space_len;
				if(hh->token_len < hh->buffer_loc)hh->buffer_loc = hh->token_len;
				if(hh->buffer_len){
					hh->buffer[hh->buffer_loc] = 0;
					if(callback(hh, HTTP_EVENT_VALUE, hh->token_start, hh->token_len, hh->buffer, hh->buffer_loc, user))return(1);
				}else{
					if(callback(hh, HTTP_EVENT_VALUE, hh->token_start, hh->token_len, &http_empty, 0, user))return(1);
				}
				if(hh->cl == 14)hh->cl = 255;
				hh->state = HTTP_STATE_CR_WAIT;
				continue;
			}else if(isspace(c)){
				hh->space_len++;
			}else{
				hh->space_len = 0;
			}
		}
		if((hh->cl != 255) && (hh->state == HTTP_STATE_PARAM) && !hh->space_len){
			if(hh->token_len >= 14){
				hh->cl = 0;
			}else{
				if(toupper(c) == toupper(http_content_len[hh->token_len])){
					hh->cl++;
				}
			}
		}
		if((hh->cl == 14) && (hh->state == HTTP_STATE_VALUE)){
			if(isdigit(c)){
				hh->content_len = (hh->content_len * 10) + (c - '0');
			}else{
				hh->cl = 255;
			}
		}
		hh->token_len++;
		if(hh->buffer_len && (hh->buffer_loc < (hh->buffer_len - 1))){
			hh->buffer[hh->buffer_loc++] = c;
		}
	}
	return(0);
}

static uint8_t http_hex2bin(char hex){
	if(hex == '1')return(1);
	if(hex == '2')return(2);
	if(hex == '3')return(3);
	if(hex == '4')return(4);
	if(hex == '5')return(5);
	if(hex == '6')return(6);
	if(hex == '7')return(7);
	if(hex == '8')return(8);
	if(hex == '9')return(9);
	if((hex == 'a') || (hex == 'A'))return(10);
	if((hex == 'b') || (hex == 'B'))return(11);
	if((hex == 'c') || (hex == 'C'))return(12);
	if((hex == 'd') || (hex == 'D'))return(13);
	if((hex == 'e') || (hex == 'E'))return(14);
	if((hex == 'f') || (hex == 'F'))return(15);
	return(0);
}

