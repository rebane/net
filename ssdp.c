#include "ssdp.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "net.h"
#include "http.h"

struct ssdp_callback_struct{
	void *buf;
	char *param;
	int16_t param_len;
	char *method;
	int16_t method_len;
	char *url;
	int16_t url_len;
	char *host;
	int16_t host_len;
	char *man;
	int16_t man_len;
	char *mx;
	int16_t mx_len;
	char *st;
	int16_t st_len;
	char *location;
	int16_t location_len;
	char *server;
	int16_t server_len;
	char *nt;
	int16_t nt_len;
	char *usn;
	int16_t usn_len;
};

static uint8_t ssdp_callback_func(http_t *hh, int event, int32_t parse_loc, int16_t parse_len, const void *buf, int16_t count, void *user);
static uint8_t ssdp_strncaseequal(char *str1, uint16_t len1, char *str2);

int ssdp_recv_query_msearch(ssdp_query_msearch_t *ssdp_query_msearch){
	net_udp_t udp;
	http_t hh;
	struct ssdp_callback_struct ssdp_callback;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != SSDP_DEST_PORT)return(0);
	memset(&ssdp_callback, 0, sizeof(struct ssdp_callback_struct));
	ssdp_callback.buf = udp.data;
	http_init(&hh, (void *)0, 0, HTTP_MODE_REQUEST);
	http_parse(&hh, udp.data, udp.len, (void *)&ssdp_callback, ssdp_callback_func);
	if(!ssdp_callback.method_len)return(0);
	if(!ssdp_strncaseequal(ssdp_callback.method, ssdp_callback.method_len, "M-SEARCH"))return(0);
	if(!ssdp_strncaseequal(ssdp_callback.url, ssdp_callback.url_len, "*"))return(0);
	if(!ssdp_callback.host)return(0);
	if(!ssdp_strncaseequal(ssdp_callback.man, ssdp_callback.man_len, "\"ssdp:discover\"") && !ssdp_strncaseequal(ssdp_callback.man, ssdp_callback.man_len, "ssdp:discover"))return(0);
	if(!ssdp_callback.mx || !ssdp_callback.mx_len)return(0); // TODO: get mx value to uint
	if(!ssdp_callback.st || !ssdp_callback.st_len)return(0);
	ssdp_query_msearch->ifnum = udp.ifnum;
	ssdp_query_msearch->src_ip = udp.src_ip;
	ssdp_query_msearch->src_port = udp.src_port;
	ssdp_query_msearch->data = udp.data;
	ssdp_query_msearch->len = udp.len;
	ssdp_query_msearch->mx = 0;
	ssdp_query_msearch->host = ssdp_callback.host;
	ssdp_query_msearch->host_len = ssdp_callback.host_len;
	ssdp_query_msearch->st = ssdp_callback.st;
	ssdp_query_msearch->st_len = ssdp_callback.st_len;
	return(1);
}

int ssdp_recv_notify(ssdp_notify_t *ssdp_notify){
	net_udp_t udp;
	http_t hh;
	struct ssdp_callback_struct ssdp_callback;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != SSDP_DEST_PORT)return(0);
	memset(&ssdp_callback, 0, sizeof(struct ssdp_callback_struct));
	ssdp_callback.buf = udp.data;
	http_init(&hh, (void *)0, 0, HTTP_MODE_REQUEST);
	http_parse(&hh, udp.data, udp.len, (void *)&ssdp_callback, ssdp_callback_func);
	if(!ssdp_callback.method_len)return(0);
	if(!ssdp_strncaseequal(ssdp_callback.method, ssdp_callback.method_len, "NOTIFY"))return(0);
	if(!ssdp_strncaseequal(ssdp_callback.url, ssdp_callback.url_len, "*"))return(0);
	ssdp_notify->ifnum = udp.ifnum;
	ssdp_notify->src_ip = udp.src_ip;
	ssdp_notify->src_port = udp.src_port;
	ssdp_notify->data = udp.data;
	ssdp_notify->len = udp.len;
	ssdp_notify->location = ssdp_callback.location;
	ssdp_notify->location_len = ssdp_callback.location_len;
	ssdp_notify->server = ssdp_callback.server;
	ssdp_notify->server_len = ssdp_callback.server_len;
	ssdp_notify->nt = ssdp_callback.nt;
	ssdp_notify->nt_len = ssdp_callback.nt_len;
	ssdp_notify->usn = ssdp_callback.usn;
	ssdp_notify->usn_len = ssdp_callback.usn_len;
	return(1);
}

int16_t ssdp_send_answer_msearch(int ifnum, uint32_t dest_ip, uint16_t dest_port, const char *location, const char *server, const char *usn, const void *st, int16_t st_len){
	int16_t len;
	len = 0;
	len += snprintf(&((char *)net_send_buffer_udp)[len], net_send_buffer_udp_len - len - 4,
			"HTTP/1.1 200 OK\r\n"
			"CACHE-CONTROL: max-age=1800\r\n"
			"EXT:\r\n"
			"LOCATION: %s\r\n"
			"SERVER: %s\r\n"
			"USN: %s\r\n"
			"ST: ",
		location,
		server,
		usn
	);
	if(st_len > (net_send_buffer_udp_len - len - 4))st_len = (net_send_buffer_udp_len - len - 4);
	memcpy(&((uint8_t *)net_send_buffer_udp)[len], st, st_len);
	len += st_len;
	memcpy(&((uint8_t *)net_send_buffer_udp)[len], "\r\n\r\n", 4);
	len += 4;
	return(net_send_udp(ifnum, dest_ip, 0, dest_port, SSDP_DEST_PORT, net_send_buffer_udp, len));
}

int16_t ssdp_send_notify(int ifnum, const char *location, const char *server, const char *nt, const char *usn){
	int16_t len;
	len = 0;
	len += snprintf(&((char *)net_send_buffer_udp)[len], net_send_buffer_udp_len - len - 4,
			"NOTIFY * HTTP/1.1\r\n"
			"HOST: 239.255.255.250:1900\r\n"
			"CACHE-CONTROL: max-age=1800\r\n"
			"LOCATION: %s\r\n"
			"NTS: ssdp:alive\r\n"
			"SERVER: %s\r\n"
			"NT: %s\r\n"
			"USN: %s\r\n\r\n",
		location,
		server,
		nt,
		usn
	);
	return(net_send_udp(ifnum, 0xEFFFFFFA, 0, SSDP_DEST_PORT, SSDP_DEST_PORT, net_send_buffer_udp, len));
}

static uint8_t ssdp_callback_func(http_t *hh, int event, int32_t parse_loc, int16_t parse_len, const void *buf, int16_t count, void *user){
	struct ssdp_callback_struct *ssdp_callback;
	ssdp_callback = (struct ssdp_callback_struct *)user;
	if(event == HTTP_EVENT_METHOD){
		ssdp_callback->method = &((char *)ssdp_callback->buf)[parse_loc];
		ssdp_callback->method_len = parse_len;
	}else if(event == HTTP_EVENT_URL){
		ssdp_callback->url = &((char *)ssdp_callback->buf)[parse_loc];
		ssdp_callback->url_len = parse_len;
	}else if(event == HTTP_EVENT_VALUE){
		if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "HOST")){
			ssdp_callback->host = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->host_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "MAN")){
			ssdp_callback->man = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->man_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "MX")){
			ssdp_callback->mx = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->mx_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "ST")){
			ssdp_callback->st = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->st_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "LOCATION")){
			ssdp_callback->location = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->location_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "SERVER")){
			ssdp_callback->server = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->server_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "NT")){
			ssdp_callback->nt = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->nt_len = parse_len;
		}else if(ssdp_strncaseequal(ssdp_callback->param, ssdp_callback->param_len, "USN")){
			ssdp_callback->usn = &((char *)ssdp_callback->buf)[parse_loc];
			ssdp_callback->usn_len = parse_len;
		}
	}
	if(event == HTTP_EVENT_PARAM){
		ssdp_callback->param = &((char *)ssdp_callback->buf)[parse_loc];
		ssdp_callback->param_len = parse_len;
	}else{
		ssdp_callback->param = (void *)0;
		ssdp_callback->param_len = 0;
	}
	return(0);
}

static uint8_t ssdp_strncaseequal(char *str1, uint16_t len1, char *str2){
	uint16_t i;
	if(str1 == (void *)0)return(0);
	for(i = 0; i < len1; i++){
		if(!str2[i])return(0);
		if(toupper(str1[i]) != toupper(str2[i]))return(0);
	}
	if(str2[i])return(0);
	return(1);
}

