#ifndef _MDNS_H_
#define _MDNS_H_

#include <stdint.h>

#define MDNS_DEST_PORT 5353

int mdns_recv_match_query_a_cstr(const char *name, int *ifnum);
int mdns_recv_match_query_ptr(uint32_t ip, int *ifnum);
int mdns_recv_match_query_ptr_local(int *ifnum);
int16_t mdns_send_answer_a_cstr(int ifnum, const char *name, uint32_t ip);
int16_t mdns_send_answer_ptr_cstr(int ifnum, uint32_t ip, const char *name);

#endif

