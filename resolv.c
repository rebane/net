#include "resolv.h"
#include "net.h"
#include "udp.h"
#include "dns.h"

int resolv_recv_match_answer_a(dns_name_t *name, uint32_t *ip){
	net_udp_t udp;
	dns_section_t d;
	dns_name_t n;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != RESOLV_DEST_PORT)return(0);
	for(dns_section_first(&d, udp.data, udp.len); dns_section_valid(&d); dns_section_next(&d)){
		if((dns_flags(udp.data) & 0x8000) && (d.section_type == DNS_SECTION_ANSWER) && (d.type == DNS_TYPE_A) && (d.class == 0x0001) && (d.rlength == 4)){
			if(dns_name_isequal(dns_name_label(&n, udp.data, udp.len, d.name), name)){
				*ip = ((uint32_t)((uint8_t *)udp.data)[d.rdata + 0] << 24) | ((uint32_t)((uint8_t *)udp.data)[d.rdata + 1] << 16) | ((uint32_t)((uint8_t *)udp.data)[d.rdata + 2] << 8) | ((uint32_t)((uint8_t *)udp.data)[d.rdata + 3] << 0);
				return(1);
			}
		}
	}
	return(0);
}

int resolv_recv_match_answer_cname(dns_name_t *name, dns_name_t *alias){
	net_udp_t udp;
	dns_section_t d;
	dns_name_t n;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != RESOLV_DEST_PORT)return(0);
	for(dns_section_first(&d, udp.data, udp.len); dns_section_valid(&d); dns_section_next(&d)){
		if((dns_flags(udp.data) & 0x8000) && (d.section_type == DNS_SECTION_ANSWER) && (d.type == DNS_TYPE_CNAME) && (d.class == 0x0001) && d.rlength){
			if(dns_name_isequal(dns_name_label(&n, udp.data, udp.len, d.name), name)){
				dns_name_label(alias, udp.data, udp.len, d.rdata);
				return(1);
			}
		}
	}
	return(0);
}

int resolv_recv_match_answer_a_cstr(const char *name, uint32_t *ip){
	dns_name_t n;
	return(resolv_recv_match_answer_a(dns_name_cstr(&n, name), ip));
}

int resolv_recv_match_answer_cname_cstr(const char *name, dns_name_t *alias){
	dns_name_t n;
	return(resolv_recv_match_answer_cname(dns_name_cstr(&n, name), alias));
}

int resolv_recv_match_answer_cstr(const char *name, uint32_t *ip){
	int r;
	dns_name_t n;
	dns_name_cstr(&n, name);
	do{
		r = resolv_recv_match_answer_a(&n, ip);
		if(r)return(r);
	}while(resolv_recv_match_answer_cname(&n, &n));
	return(0);
}

int16_t resolv_send_query_a(uint32_t ns, const char *name, int16_t count){
	int16_t len;
	int ifnum;
	ifnum = interface_ns_get(&ns);
	if(ifnum < 0)return(ifnum);
	dns_id_set(net_send_buffer_udp, 0);
	dns_flags_set(net_send_buffer_udp, 0x0100);
	dns_qdcount_set(net_send_buffer_udp, 1);
	dns_ancount_set(net_send_buffer_udp, 0);
	dns_nscount_set(net_send_buffer_udp, 0);
	dns_arcount_set(net_send_buffer_udp, 0);
	len = DNS_HEADER_LEN;
	len += dns_name_encode(&net_send_buffer_udp[len], 512, name, count);
	dns_uint16_set(&net_send_buffer_udp[len], 0x0001); len += 2;
	dns_uint16_set(&net_send_buffer_udp[len], 0x0001); len += 2;
	return(net_send_udp(ifnum, ns, 0, RESOLV_DEST_PORT, RESOLV_DEST_PORT, net_send_buffer_udp, len));
}

int16_t resolv_send_query_a_cstr(uint32_t ns, const char *name){
	return(resolv_send_query_a(ns, name, strlen(name)));
}

