#include "pap.h"
#include "net.h"

int pap_parse_raw(pap_header_t *header, const void *buf, int16_t count){
	uint16_t l;
	if(count < PAP_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(pap_len(buf) < PAP_HEADER_LEN)return(NET_ERROR_FORMAT);
	header->code = pap_code(buf);
	header->identifier = pap_identifier(buf);
	l = pap_len(buf) - PAP_HEADER_LEN;
	if(header->code == PAP_CODE_AUTH_REQUEST){
		header->peerid = (void *)0;
		header->peerid_len = 0;
		header->passwd = (void *)0;
		header->passwd_len = 0;
	}else if((header->code == PAP_CODE_AUTH_ACK) || (header->code == PAP_CODE_AUTH_NAK)){
		if(l > 0){
			l--;
			header->message_len = ((uint8_t *)buf)[PAP_HEADER_LEN];
			if(header->message_len > l)return(NET_ERROR_FORMAT);
			header->message = &((uint8_t *)buf)[PAP_HEADER_LEN + 1];
		}else{
			header->message = (void *)0;
			header->message_len = 0;
		}
		header->passwd = (void *)0;
		header->passwd_len = 0;
	}else{
		return(NET_ERROR_FORMAT);
	}
	return(0);
}

int16_t pap_write(void *dest, int16_t max, uint8_t code, uint8_t identifier, const void *buf, uint8_t count, const void *buf2, uint8_t count2){
	if(buf != (void *)0){
		if(buf2 != (void *)0){
			if(((uint16_t )PAP_HEADER_LEN + 1 + count + 1 + count2) > max)return(NET_ERROR_MEM);
			memmove(&((uint8_t *)dest)[PAP_HEADER_LEN + 1], buf, count);
			memmove(&((uint8_t *)dest)[PAP_HEADER_LEN + 1 + count + 1], buf2, count2);
			pap_code_set(dest, code);
			pap_identifier_set(dest, identifier);
			pap_len_set(dest, (PAP_HEADER_LEN + 1 + count + 1 + count2));
			((uint8_t *)dest)[PAP_HEADER_LEN] = count;
			((uint8_t *)dest)[PAP_HEADER_LEN + 1 + count] = count2;
			return(PAP_HEADER_LEN + 1 + count + 1 + count2);
		}
		if(((uint16_t )PAP_HEADER_LEN + 1 + count) > max)return(NET_ERROR_MEM);
		memmove(&((uint8_t *)dest)[PAP_HEADER_LEN + 1], buf, count);
		pap_code_set(dest, code);
		pap_identifier_set(dest, identifier);
		pap_len_set(dest, (PAP_HEADER_LEN + 1 + count));
		((uint8_t *)dest)[PAP_HEADER_LEN] = count;
		return(PAP_HEADER_LEN + 1 + count);
	}
	if(((uint16_t )PAP_HEADER_LEN) > max)return(NET_ERROR_MEM);
	pap_code_set(dest, code);
	pap_identifier_set(dest, identifier);
	pap_len_set(dest, PAP_HEADER_LEN);
	return(PAP_HEADER_LEN);
}

