#include "icmp.h"
#include "net.h"
#include <string.h>

int icmp_parse_raw(icmp_header_t *header, const void *buf, int16_t count){
	if(count < ICMP_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(icmp_chk(buf) != icmp_chk_calc(buf, count))return(NET_ERROR_FORMAT);
	header->type = icmp_type(buf);
	header->code = icmp_code(buf);
	header->header = icmp_header(buf);
	header->data = &((uint8_t *)buf)[ICMP_HEADER_LEN];
	header->len = count - ICMP_HEADER_LEN;
	return(0);
}

int16_t icmp_write(void *dest, int16_t max, uint8_t type, uint8_t code, const void *header, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((ICMP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[ICMP_HEADER_LEN], buf, count);
	icmp_header_set(dest, header);
	icmp_type_set(dest, type);
	icmp_code_set(dest, code);
	icmp_chk_set(dest, icmp_chk_calc(dest, ICMP_HEADER_LEN + count));
	return(ICMP_HEADER_LEN + count);
}

uint16_t icmp_chk_calc(const void *buf, int16_t count){
	uint32_t sum;
	int16_t i;
	sum = 0;
	for(i = 0; i < count; i += 2){
		if(i == 2)continue;
		if((i + 1) >= count){
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8);
		}else{
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	return((~sum) & 0xFFFF);
}

