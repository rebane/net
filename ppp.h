#ifndef _PPP_H_
#define _PPP_H_

#include <stdint.h>

#define PPP_HEADER_LEN                    4

#define PPP_LCP_CODE_VENDOR               0
#define PPP_LCP_CODE_CONFIGURE_REQUEST    1
#define PPP_LCP_CODE_CONFIGURE_ACK        2
#define PPP_LCP_CODE_CONFIGURE_NAK        3
#define PPP_LCP_CODE_CONFIGURE_REJECT     4
#define PPP_LCP_CODE_TERMINATE_REQUEST    5
#define PPP_LCP_CODE_TERMINATE_ACK        6
#define PPP_LCP_CODE_CODE_REJECT          7
#define PPP_LCP_CODE_PROTOCOL_REJECT      8
#define PPP_LCP_CODE_ECHO_REQUEST         9
#define PPP_LCP_CODE_ECHO_REPLY           10
#define PPP_LCP_CODE_DISCARD_REQUEST      11

#define PPP_LCP_TYPE_MRU                  1
#define PPP_LCP_TYPE_ASYNC_MAP            2
#define PPP_LCP_TYPE_AUTH_PROTOCOL        3
#define PPP_LCP_TYPE_QUALITY              4
#define PPP_LCP_TYPE_MAGIC                5
#define PPP_LCP_TYPE_PROTOCOL_FIELD_COMP  7
#define PPP_LCP_TYPE_ADDR_CTRL_FIELD_COMP 8
#define PPP_LCP_TYPE_FCS_ALTERNATIVES     9

#define PPP_IPCP_CODE_VENDOR              0
#define PPP_IPCP_CODE_CONFIGURE_REQUEST   1
#define PPP_IPCP_CODE_CONFIGURE_ACK       2
#define PPP_IPCP_CODE_CONFIGURE_NAK       3
#define PPP_IPCP_CODE_CONFIGURE_REJECT    4
#define PPP_IPCP_CODE_TERMINATE_REQUEST   5
#define PPP_IPCP_CODE_TERMINATE_ACK       6
#define PPP_IPCP_CODE_CODE_REJECT         7

#define PPP_IPCP_TYPE_IP_ADDRESSES        1
#define PPP_IPCP_TYPE_IP_COMP             2
#define PPP_IPCP_TYPE_IP_ADDRESS          3
#define PPP_IPCP_TYPE_MOBILE_IPV4         4
#define PPP_IPCP_TYPE_DNS1                129
#define PPP_IPCP_TYPE_NBNS1               130
#define PPP_IPCP_TYPE_DNS2                131
#define PPP_IPCP_TYPE_NBNS2               132

#define ppp_code(buf)                     (((uint8_t *)(buf))[0])
#define ppp_identifier(buf)               (((uint8_t *)(buf))[1])
#define ppp_len(buf)                      (((uint16_t)((uint8_t *)(buf))[2] << 8) | ((uint8_t *)(buf))[3])

#define ppp_code_set(buf, code)           (((uint8_t *)(buf))[0] = (code))
#define ppp_identifier_set(buf, ident)    (((uint8_t *)(buf))[1] = (ident))
#define ppp_len_set(buf, len)             do{ ((uint8_t *)(buf))[2] = (((uint16_t)(len) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(len) >> 0) & 0xFF); }while(0)

struct ppp_header_struct{
	uint8_t code;
	uint8_t identifier;
	void *data;
	int16_t len;
};

struct ppp_parameter_struct{
	uint8_t type;
	uint8_t len;
	uint8_t *data;
	uint8_t valid;
	uint8_t *buf;
	int16_t size;
	int16_t next;
};

typedef struct ppp_header_struct ppp_header_t;
typedef struct ppp_parameter_struct ppp_parameter_t;

int ppp_parse_raw(ppp_header_t *header, const void *buf, int16_t count);
int16_t ppp_write(void *dest, int16_t max, uint8_t code, uint8_t identifier, const void *buf, int16_t count);
int16_t ppp_parameter_write(void *dest, int16_t max, int16_t offset, uint8_t type, const void *buf, uint8_t count);
int16_t ppp_parameter_write_addr(void *dest, int16_t max, int16_t offset, uint8_t type, uint32_t addr);

void ppp_parameter_first(ppp_parameter_t *parameter, const void *buf, int16_t size);
uint8_t ppp_parameter_valid(ppp_parameter_t *parameter);
void ppp_parameter_next(ppp_parameter_t *parameter);

#endif

