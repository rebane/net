#include "socket.h"
#include "platform.h"
#include "mempool.h"
#include "net.h"
#include "tcp.h"
#include "interface.h"
#include <stdio.h>

#define SOCKET_STATE_LISTEN    1
#define SOCKET_STATE_SYN       2
#define SOCKET_STATE_SYN_CONN  3
#define SOCKET_STATE_CONNECTED 4
#define SOCKET_STATE_FLUSH     5
#define SOCKET_STATE_FIN       6
#define SOCKET_STATE_CLOSED    7

static uint16_t socket_local_port_current = 0;

static void socket_tx_update(socket_t *socket);
static void socket_poll_recv(socket_t *socket);
static void socket_poll_send(socket_t *socket);
static uint16_t socket_get_local_port();
static uint32_t socket_get_local_seq();
static uint16_t socket_get_window(socket_t *socket);

void socket_init(){
	socket_local_port_current = 1024 + (net_rand() & 0x7FFF);
}

void socket_reset(socket_t *socket){
	socket->rx.fifo_len = socket->tx.fifo_len = 0;
	socket->tx.len = 0;
	socket->send = socket->poll_send = 0;
	socket->rx.state = socket->tx.state = SOCKET_STATE_CLOSED;
}

int socket_status(socket_t *socket){
	if(socket->rx.state == SOCKET_STATE_LISTEN)return(SOCKET_STATUS_LISTEN);
	if((socket->rx.state == SOCKET_STATE_SYN) || (socket->tx.state == SOCKET_STATE_SYN))return(SOCKET_STATUS_CONNECTING);
	if(((socket->rx.state == SOCKET_STATE_FIN) || (socket->rx.state == SOCKET_STATE_CLOSED)) && (socket->tx.state == SOCKET_STATE_CLOSED))return(SOCKET_STATUS_CLOSED);
	return(SOCKET_STATUS_CONNECTED);
}

int16_t socket_accept(socket_t *socket, uint16_t local_port){
	if(local_port == 0){
		socket_reset(socket);
		return(NET_ERROR_FORMAT);
	}
	socket->local_port = local_port;
	socket->send = socket->poll_send = 0;
	socket->rx.state = SOCKET_STATE_LISTEN;
	return(0);
}

int16_t socket_connect(socket_t *socket, uint32_t remote_ip, uint16_t remote_port){
	uint32_t local_ip, gw;
	local_ip = 0;
	if(interface_route_get(-1, remote_ip, &local_ip, &gw) < 0){
		socket_reset(socket);
		return(NET_ERROR_NOROUTE); // no route to host
	}
	socket->remote_ip = remote_ip;
	socket->local_ip = local_ip;
	socket->remote_port = remote_port;
	socket->local_port = socket_get_local_port();

	socket->rx.seq = 0;
	socket->rx.fifo_len = socket->rx.fifo_in = socket->rx.fifo_out = 0;

	socket->tx.seq = socket_get_local_seq();
	socket->tx.seq_req = socket->tx.seq + 1;
	socket->tx.fifo_len = socket->tx.fifo_in = socket->tx.fifo_out = 0;
	socket->tx.len = 0;

	socket->try = 3;
	socket->timer = 0;
	socket->send = socket->poll_send = 0;
	socket->rx.state = SOCKET_STATE_SYN;
	socket->tx.state = SOCKET_STATE_SYN;
	return(0);
}

int16_t socket_read(socket_t *socket, void *buf, int16_t count){
	int16_t len;
	if(socket->rx.fifo_len){
		for(len = 0; socket->rx.fifo_len && (len < count); len++){
			((uint8_t *)buf)[len] = socket->rx.fifo[socket->rx.fifo_out];
			socket->rx.fifo_out++;
			if(socket->rx.fifo_out == SOCKET_RX_FIFO_SIZE)socket->rx.fifo_out = 0;
			socket->rx.fifo_len--;
		}
		return(len);
	}
	if((socket->rx.state == SOCKET_STATE_FIN) || (socket->rx.state == SOCKET_STATE_CLOSED))return(NET_ERROR_FORMAT);
	return(0);
}

int16_t socket_write(socket_t *socket, const void *buf, int16_t count){
	int16_t len;
	if(socket->tx.state != SOCKET_STATE_CONNECTED)return(NET_ERROR_FORMAT);
	for(len = 0; (socket->tx.fifo_len < SOCKET_TX_FIFO_SIZE) && (len < count); len++){
		socket->tx.fifo[socket->tx.fifo_in] = ((uint8_t *)buf)[len];
		socket->tx.fifo_in++;
		if(socket->tx.fifo_in == SOCKET_TX_FIFO_SIZE)socket->tx.fifo_in = 0;
		socket->tx.fifo_len++;
	}
	if(socket->tx.seq == socket->tx.seq_req){
		socket_tx_update(socket);
	}
	return(len);
}

int16_t socket_shutdown(socket_t *socket){
	if(socket->tx.state != SOCKET_STATE_CONNECTED)return(NET_ERROR_FORMAT);
	if(socket->tx.fifo_len || (socket->tx.seq != socket->tx.seq_req)){
		socket->tx.state = SOCKET_STATE_FLUSH;
	}else{
		socket->tx.seq_req++;
		socket->try = 3;
		socket->timer = 0;
		socket->tx.state = SOCKET_STATE_FIN;
	}
	return(0);
}

static void socket_poll_recv(socket_t *socket){
	uint16_t i;
	net_tcp_t tcp;
	if(!net_recv_tcp(&tcp))return;
	if(socket->rx.state == SOCKET_STATE_LISTEN){
		if(socket->local_port != tcp.dest_port)return;
		if(!(tcp.flags & TCP_FLAG_SYN) || (tcp.flags & TCP_FLAG_ACK) || (tcp.flags & TCP_FLAG_RST) || (tcp.flags & TCP_FLAG_FIN) || tcp.len)return; // incoming connection
		net_used_tcp();
		socket->remote_ip = tcp.src_ip;
		socket->local_ip = tcp.dest_ip;
		socket->remote_port = tcp.src_port;

		socket->rx.seq = tcp.seq + 1;
		socket->rx.fifo_len = socket->rx.fifo_in = socket->rx.fifo_out = 0;

		socket->tx.seq = socket_get_local_seq();
		socket->tx.seq_req = socket->tx.seq + 1;
		socket->tx.win = tcp.win;
		socket->tx.fifo_len = socket->tx.fifo_in = socket->tx.fifo_out = 0;
		socket->tx.len = 0;

		socket->try = 3;
		socket->timer = 0;
		socket->rx.state = SOCKET_STATE_CONNECTED;
		socket->tx.state = SOCKET_STATE_SYN;
		printf("SOCKET: RX CONNECTED LISTEN\n");
		return;
	}
	if((socket->remote_ip != tcp.src_ip) || (socket->local_ip != tcp.dest_ip) || (socket->remote_port != tcp.src_port) || (socket->local_port != tcp.dest_port))return;
	net_used_tcp();
	if(tcp.flags & TCP_FLAG_RST)printf("SOCKET: WARN: RST\n");
	if(tcp.flags & TCP_FLAG_SYN){
		if(socket->rx.state == SOCKET_STATE_SYN){
			socket->rx.seq = tcp.seq + 1;
			socket->rx.state = SOCKET_STATE_SYN_CONN;
			socket->send = 1;
			printf("SOCKET: RX CONNECTED ACCEPT\n");
		}else if((socket->rx.state == SOCKET_STATE_SYN_CONN) && (socket->rx.seq == (tcp.seq + 1))){
			socket->send = 1;
			printf("SOCKET: RX CONNECTED ACCEPT 2\n");
		}else{
			// net_reset_tcp();
			printf("SOCKET: STRAGNE SYN\n");
		}
	}
	if(socket->rx.seq == tcp.seq){
		if(tcp.flags & TCP_FLAG_RST){
			socket_reset(socket);
			printf("SOCKET: RST\n");
			return;
		}else if((socket->rx.state == SOCKET_STATE_SYN_CONN) || (socket->rx.state == SOCKET_STATE_CONNECTED)){
			if((socket->rx.state == SOCKET_STATE_SYN_CONN) && !(tcp.flags & TCP_FLAG_SYN))socket->rx.state = SOCKET_STATE_CONNECTED;
			if(tcp.len){
				for(i = 0; (i < tcp.len) && (socket->rx.fifo_len < SOCKET_RX_FIFO_SIZE); i++){
					socket->rx.fifo[socket->rx.fifo_in] = ((uint8_t *)tcp.data)[i];
					socket->rx.fifo_in++;
					if(socket->rx.fifo_in == SOCKET_RX_FIFO_SIZE)socket->rx.fifo_in = 0;
					socket->rx.fifo_len++;
				}
				socket->rx.seq += tcp.len;
				socket->send = 1;
			}
			if(tcp.flags & TCP_FLAG_FIN){
				socket->rx.seq++;
				socket->rx.state = SOCKET_STATE_FIN;
				if(socket->tx.state == SOCKET_STATE_CLOSED){
					printf("SOCKET: TXRX CLOSED\n");
				}else{
					printf("SOCKET: RX CLOSED\n");
					socket_shutdown(socket);
				}
				socket->send = 1;
			}
		}
	}else if((socket->rx.state == SOCKET_STATE_FIN) && (tcp.flags & TCP_FLAG_FIN)){
		if(tcp.seq == (socket->rx.seq - 1 - tcp.len)){
			printf("SOCKET: RX CLOSED 2\n");
			socket->send = 1;
		}
	}
	if((tcp.ack == socket->tx.seq_req) && (tcp.flags & TCP_FLAG_ACK)){
		socket->tx.win = tcp.win;
		socket->tx.seq = tcp.ack;
		if(socket->tx.state == SOCKET_STATE_SYN){
			socket->tx.state = SOCKET_STATE_CONNECTED;
			printf("SOCKET: TX CONNECTED\n");
		}else if(socket->tx.state == SOCKET_STATE_FIN){
			socket->tx.state = SOCKET_STATE_CLOSED;
			if(socket->rx.state == SOCKET_STATE_CLOSED){
				printf("SOCKET: RXTX CLOSED\n");
			}else{
				printf("SOCKET: TX CLOSED\n");
			}
		}
		socket_tx_update(socket);
	}
}

void socket_poll_send(socket_t *socket){
	uint16_t flags;
	if((socket->tx.seq_req != socket->tx.seq) && (net_timer_get() >= socket->timer)){
		if(socket->try){
			if(socket->try == 3){
				printf("SOCKET: SEND\n");
			}else{
				printf("SOCKET: RESEND\n");
			}
			socket->try--;
			if(socket->try == 2){
				socket->timer = net_timer_get() + 1;
			}else if(socket->try == 1){
				socket->timer = net_timer_get() + 3;
			}else{
				socket->timer = net_timer_get() + 6;
			}
			socket->send = 1;
		}else{
			socket_reset(socket);
			printf("SOCKET: CONNECT TIMEOUT\n");
		}
	}
	if(socket->send){
		flags = 0;
		if(socket->tx.len)flags |= TCP_FLAG_PSH;
		if(socket->tx.state == SOCKET_STATE_SYN){
			flags |= TCP_FLAG_SYN;
		}else if(socket->tx.state == SOCKET_STATE_FIN){
			flags |= TCP_FLAG_FIN;
		}
		if(socket->rx.state != SOCKET_STATE_SYN)flags |= TCP_FLAG_ACK;
		if(net_send_tcp(socket->remote_ip, socket->local_ip, socket->remote_port, socket->local_port, socket->tx.seq, socket->rx.seq, flags, socket_get_window(socket), socket->tx.buffer, socket->tx.len) < 0){
			socket_reset(socket);
			printf("SOCKET: SEND ERROR\n");
		}else{
			socket->send = 0;
		}
	}
}

void socket_poll(socket_t *socket){
	if((socket->rx.state == SOCKET_STATE_CLOSED) && (socket->tx.state == SOCKET_STATE_CLOSED))return;
	if(socket->poll_send){
		socket->poll_send = 0;
		socket_poll_send(socket);
	}
	socket_poll_recv(socket);
	if(socket->rx.state == SOCKET_STATE_LISTEN)return;
	if(!socket->rx.fifo_len){
		socket->poll_send = 0;
		socket_poll_send(socket);
	}else{
		socket->poll_send = 1;
	}
}

static void socket_tx_update(socket_t *socket){
	for(socket->tx.len = 0; socket->tx.fifo_len && (socket->tx.len < SOCKET_TX_SIZE) && (socket->tx.len < socket->tx.win); socket->tx.len++){
		socket->tx.buffer[socket->tx.len] = socket->tx.fifo[socket->tx.fifo_out];
		socket->tx.fifo_out++;
		if(socket->tx.fifo_out == SOCKET_TX_FIFO_SIZE)socket->tx.fifo_out = 0;
		socket->tx.fifo_len--;
	}
	socket->tx.seq_req += socket->tx.len;
	if((socket->tx.fifo_len == 0) && (socket->tx.state == SOCKET_STATE_FLUSH)){
		socket->tx.seq_req++;
		socket->tx.state = SOCKET_STATE_FIN;
	}
	if(socket->tx.seq != socket->tx.seq_req){
		socket->try = 3;
		socket->timer = 0;
	}
}

static uint16_t socket_get_local_port(){
	socket_local_port_current++;
	if(socket_local_port_current < 1024)socket_local_port_current = 1024;
	return(socket_local_port_current);
}

static uint32_t socket_get_local_seq(){
	return(net_rand());
}

static uint16_t socket_get_window(socket_t *socket){
	return((SOCKET_RX_FIFO_SIZE - socket->rx.fifo_len) / 2);
}

