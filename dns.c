#include "dns.h"
#include <string.h>
#include <ctype.h>

// http://www.zytrax.com/books/dns/ch15/

#define dns_uint16(buf) (((uint16_t)((uint8_t *)(buf))[0] << 8) | (((uint8_t *)(buf))[1] << 0))

void dns_section_first(dns_section_t *section, const void *buf, int16_t count){
	section->buf = (uint8_t *)buf;
	section->size = count;
	section->next = 12;
	section->section_num = 0;
	dns_section_next(section);
}

uint8_t dns_section_valid(dns_section_t *section){
	return(section->section_type != DNS_SECTION_ERROR);
}

void dns_section_next(dns_section_t *section){
	uint8_t section_type, l;
	section->section_type = DNS_SECTION_ERROR;
	if(section->next >= section->size)return;
	section->section_num++;
	if(section->section_num <= dns_qdcount(section->buf)){
		section_type = DNS_SECTION_QUESTION;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf))){
		section_type = DNS_SECTION_ANSWER;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf) + dns_nscount(section->buf))){
		section_type = DNS_SECTION_AUTHORITY;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf) + dns_nscount(section->buf) + dns_arcount(section->buf))){
		section_type = DNS_SECTION_ADDITIONAL;
	}else{
		return;
	}
	section->name = section->next;
	do{
		l = section->buf[section->next];
		if(l & 0xC0){
			section->next += 1 + 1;
			l = 0;
		}else{
			section->next += 1 + l;
		}
		if(section->next >= section->size)return;
	}while(l != 0);
	if((section->next + 4) > section->size)return;
	section->type = dns_uint16(&section->buf[section->next]);
	section->class = dns_uint16(&section->buf[section->next + 2]);
	section->next += 4;
	if(section_type == DNS_SECTION_QUESTION){
		section->section_type = section_type;
		section->ttl = 0;
		section->rlength = 0;
		section->rdata = 0;
		return;
	}
	if((section->next + 6) > section->size)return;
	section->ttl = (((uint32_t)section->buf[section->next] << 24) | ((uint32_t)section->buf[section->next + 1] << 16) | ((uint32_t)section->buf[section->next + 2] << 8) | ((uint32_t)section->buf[section->next + 3] << 0));
	section->next += 4;
	section->rlength = dns_uint16(&section->buf[section->next]);
	section->next += 2;
	section->rdata = section->next;
	section->next += section->rlength;
	if(section->next > section->size)return;
	section->section_type = section_type;
}

void dns_label_first(dns_label_t *label, const void *buf, int16_t count, int16_t loc){
	label->buf = (uint8_t *)buf;
	label->size = count;
	label->next = loc;
	dns_label_next(label);
}

uint8_t dns_label_valid(dns_label_t *label){
	return(label->valid);
}

void dns_label_next(dns_label_t *label){
	uint8_t l, loops;
	label->valid = 0;
	loops = 0;
restart:
	loops++;
	if(loops > 16)return;
	if(label->next >= label->size)return;
	l = label->buf[label->next];
	if(l & 0xC0){
		if((label->next + 1 + 1) >= label->size)return;
		label->next = (((uint16_t)(l & 0x3F)) << 8) | (((uint16_t)label->buf[label->next + 1]) << 0);
		goto restart;
	}
	if(!l)return;
	label->length = l;
	label->next += 1;
	label->label = label->next;
	label->next += l;
	if(label->next > label->size)return;
	label->valid = 1;
}

dns_name_t *dns_name_cstr(dns_name_t *name, const char *cstr){
	name->type = DNS_NAME_TYPE_STRING;
	name->name = (char *)cstr;
	name->len = strlen(cstr);
	return(name);
}

dns_name_t *dns_name_label(dns_name_t *name, const void *buf, int16_t count, int16_t loc){
	name->type = DNS_NAME_TYPE_LABEL;
	name->buf = (uint8_t *)buf;
	name->count = count;
	name->loc = loc;
	return(name);
}

uint8_t dns_name_isequal(dns_name_t *name1, dns_name_t *name2){
	dns_label_t label1, label2;
	int16_t s, i;
	if((name1->type == DNS_NAME_TYPE_STRING) && (name2->type == DNS_NAME_TYPE_STRING)){
		return((name1->len == name2->len) && !memcmp(name1->name, name2->name, name1->len));
	}else if((name1->type == DNS_NAME_TYPE_LABEL) && (name2->type == DNS_NAME_TYPE_LABEL)){
		for(dns_label_first(&label1, name1->buf, name1->count, name1->loc), dns_label_first(&label2, name2->buf, name2->count, name2->loc); dns_label_valid(&label1); dns_label_next(&label1), dns_label_next(&label2)){
			if(!dns_label_valid(&label2))return(0);
			if(label1.length != label2.length)return(0);
			for(i = 0; i < label1.length; i++){
				if(toupper(((char *)name1->buf)[label1.label + i]) != toupper(((char *)name2->buf)[label2.label + i]))return(0);
			}
		}
		if(dns_label_valid(&label2))return(0);
		return(1);
	}else if(name1->type == DNS_NAME_TYPE_STRING){
		dns_name_isequal(name2, name1);
	}
	s = 0;
	for(dns_label_first(&label1, name1->buf, name1->count, name1->loc); dns_label_valid(&label1); dns_label_next(&label1)){
		if(s != 0){
			if((s >= name2->len) || (name2->name[s] != '.'))return(0);
			s++;
		}
		for(i = 0; i < label1.length; i++){
			if((s >= name2->len) || (toupper(((char *)name1->buf)[label1.label + i]) != toupper(name2->name[s])))return(0);
			s++;
		}
	}
	if(s != name2->len)return(0);
	return(1);
}

int16_t dns_name_encode(void *dest, int16_t max, const char *str, int16_t count){
	int16_t loc, lenloc, strloc;
	uint8_t l;
	if(max < 1)return(max);
	if((max < 3) || !count){
		((char *)dest)[0] = 0;
		return(1);
	}
	l = 0;
	lenloc = 0;
	loc = 1;
	for(strloc = 0; strloc < count; strloc++){
		if(str[strloc] == '.'){
			if(!l)break;
			((uint8_t *)dest)[lenloc] = l;
			l = 0;
			lenloc = loc;
			loc++;
		}else{
			if(l == 255)break;
			((char *)dest)[loc++] = str[strloc];
			l++;
		}
		if(loc >= (max - 1))break;
	}
	if(l){
		((uint8_t *)dest)[lenloc] = l;
	}else{
		loc--;
	}
	((char *)dest)[loc++] = 0;
	return(loc);
}

int16_t dns_name_encode_cstr(void *dest, int16_t max, const char *str){
	return(dns_name_encode(dest, max, str, strlen(str)));
}

