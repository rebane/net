#include "tcp.h"
#include "net.h"
#include <string.h>

int tcp_parse_raw(tcp_header_t *header, uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count){
	// TODO: urgent data handling
	uint8_t header_len;
	if(count < TCP_HEADER_LEN)return(NET_ERROR_FORMAT);
	header_len = tcp_data_offset(buf) * 4;
	if(header_len < TCP_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(count < header_len)return(NET_ERROR_FORMAT);
	if(tcp_chk(buf) != tcp_chk_calc(src_ip, dest_ip, buf, count))return(NET_ERROR_FORMAT);
	header->dest_port = tcp_dest_port(buf);
	header->src_port = tcp_src_port(buf);
	header->seq = tcp_seq_number(buf);
	header->ack = tcp_ack_number(buf);
	header->flags = tcp_flags(buf);
	header->win = tcp_window_size(buf);
	header->data = &((uint8_t *)buf)[header_len];
	header->len = count - header_len;
	return(0);
}

int16_t tcp_write(void *dest, int16_t max, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, uint32_t seq, uint32_t ack, uint16_t flags, uint16_t win, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((TCP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[TCP_HEADER_LEN], buf, count);
	tcp_src_port_set(dest, src_port);
	tcp_dest_port_set(dest, dest_port);
	tcp_seq_number_set(dest, seq);
	tcp_ack_number_set(dest, ack);
	tcp_data_offset_set(dest, (TCP_HEADER_LEN / 4));
	tcp_flags_set(dest, flags);
	tcp_window_size_set(dest, win);
	tcp_urgent_pointer_set(dest, 0);
	tcp_chk_set(dest, tcp_chk_calc(dest_ip, src_ip, dest, (TCP_HEADER_LEN + count)));
	return(TCP_HEADER_LEN + count);
}

uint16_t tcp_chk_calc(uint32_t dest_ip, uint32_t src_ip, const void *buf, int16_t count){
	uint32_t sum;
	int16_t i;
	sum = 0;
	sum += (src_ip >> 16) & 0xFFFF;
	sum += (src_ip >> 0) & 0xFFFF;
	sum += (dest_ip >> 16) & 0xFFFF;
	sum += (dest_ip >> 0) & 0xFFFF;
	sum += (uint16_t)TCP_IPV4_PROTOCOL_TCP;
	sum += count;
	for(i = 0; i < count; i += 2){
		if(i == 16)continue;
		if((i + 1) >= count){
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8);
		}else{
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	sum = (~sum) & 0xFFFF;
	return(sum);
}

