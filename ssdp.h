#ifndef _SSDP_H_
#define _SSDP_H_

#include <stdint.h>

#define SSDP_DEST_PORT 1900

struct ssdp_query_msearch_struct{
	int ifnum;
	uint32_t src_ip;
	uint16_t src_port;
	void *data;
	int16_t len;
	uint16_t mx;
	void *host;
	int16_t host_len;
	void *st;
	int16_t st_len;
};

struct ssdp_notify_struct{
	int ifnum;
	uint32_t src_ip;
	uint16_t src_port;
	void *data;
	int16_t len;
	void *location;
	int16_t location_len;
	void *server;
	int16_t server_len;
	void *nt;
	int16_t nt_len;
	void *usn;
	int16_t usn_len;
};

typedef struct ssdp_query_msearch_struct ssdp_query_msearch_t;
typedef struct ssdp_notify_struct ssdp_notify_t;

int ssdp_recv_query_msearch(ssdp_query_msearch_t *ssdp);
int ssdp_recv_notify(ssdp_notify_t *ssdp);
int16_t ssdp_send_answer_msearch(int ifnum, uint32_t dest_ip, uint16_t dest_port, const char *location, const char *server, const char *usn, const void *st, int16_t st_len);
int16_t ssdp_send_notify(int ifnum, const char *location, const char *server, const char *nt, const char *usn);

#endif

