#ifndef _NET_H_
#define _NET_H_

#include <stdint.h>
#include "interface.h"
#include "ethernet.h"
#include "ahdlc.h"
#include "ppp.h"
#include "pap.h"
#include "arp.h"
#include "dhcp.h"
#include "ipv4.h"
#include "icmp.h"
#include "igmp.h"
#include "udp.h"
#include "tcp.h"
#include "socket.h"
#include "dns.h"
#include "mdns.h"
#include "http.h"
#include "ssdp.h"
#include "resolv.h"
#include "url.h"

#define NET_SEND_BUFFER_LEN               1500

#define net_send_buffer_ethernet          (&net_send_buffer[0])
#define net_send_buffer_ethernet_len      (NET_SEND_BUFFER_LEN - 0)
#define net_send_buffer_arp               (&net_send_buffer[ETHERNET_HEADER_LEN])
#define net_send_buffer_arp_len           (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN)

#define net_send_buffer_ahdlc             (&net_send_buffer[ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN])
#define net_send_buffer_ahdlc_len         (NET_SEND_BUFFER_LEN - (ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN))
#define net_send_buffer_ppp               (&net_send_buffer[(ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) + AHDLC_HEADER_LEN])
#define net_send_buffer_ppp_len           (NET_SEND_BUFFER_LEN - (ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) - AHDLC_HEADER_LEN)
#define net_send_buffer_ppp_parameter     (&net_send_buffer[(ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) + AHDLC_HEADER_LEN + PPP_HEADER_LEN])
#define net_send_buffer_ppp_parameter_len (NET_SEND_BUFFER_LEN - (ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) - AHDLC_HEADER_LEN - PPP_HEADER_LEN)
#define net_send_buffer_pap               (&net_send_buffer[(ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) + AHDLC_HEADER_LEN])
#define net_send_buffer_pap_len           (NET_SEND_BUFFER_LEN - (ETHERNET_HEADER_LEN - AHDLC_HEADER_LEN) - AHDLC_HEADER_LEN)

#define net_send_buffer_ipv4              (&net_send_buffer[ETHERNET_HEADER_LEN])
#define net_send_buffer_ipv4_len          (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN)
#define net_send_buffer_icmp              (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN])
#define net_send_buffer_icmp_len          (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN)
#define net_send_buffer_igmp              (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN])
#define net_send_buffer_igmp_len          (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN)
#define net_send_buffer_udp               (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN])
#define net_send_buffer_udp_len           (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN)
#define net_send_buffer_tcp               (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN])
#define net_send_buffer_tcp_len           (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN)

#define net_send_buffer_dhcp              (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN + UDP_HEADER_LEN])
#define net_send_buffer_dhcp_len          (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN - UDP_HEADER_LEN)
#define net_send_buffer_dhcp_option       (&net_send_buffer[ETHERNET_HEADER_LEN + IPV4_HEADER_LEN + UDP_HEADER_LEN + DHCP_HEADER_LEN])
#define net_send_buffer_dhcp_option_len   (NET_SEND_BUFFER_LEN - ETHERNET_HEADER_LEN - IPV4_HEADER_LEN - UDP_HEADER_LEN - DHCP_HEADER_LEN)

extern uint8_t net_send_buffer[];

#define NET_ERROR_OK                      (0)
#define NET_ERROR_MEM                     (-1)
#define NET_ERROR_NOROUTE                 (-2)
#define NET_ERROR_TIMEOUT                 (-3)
#define NET_ERROR_FORMAT                  (-4)
#define NET_ERROR_HW                      (-5)

#define net_ipv4_addr(a, b, c, d)         (((uint32_t)(a) << 24) | ((uint32_t)(b) << 16) | ((uint32_t)(c) << 8) | ((uint32_t)(d) << 0))
#define net_ipv4_1(ip)                    (((uint32_t)(ip) >> 24) & 0xFF)
#define net_ipv4_2(ip)                    (((uint32_t)(ip) >> 16) & 0xFF)
#define net_ipv4_3(ip)                    (((uint32_t)(ip) >> 8) & 0xFF)
#define net_ipv4_4(ip)                    (((uint32_t)(ip) >> 0) & 0xFF)

struct net_recv_struct{
	int ifnum;
	void *buf;
	int16_t count;
	union{
		struct{
			uint8_t ok;
			uint8_t next;
			ipv4_header_t header;
			union{
				struct{
					uint8_t ok;
					icmp_header_t header;
				}icmp;
				struct{
					uint8_t ok;
					igmp_header_t header;
				}igmp;
				struct{
					uint8_t ok;
					udp_header_t header;
				}udp;
				struct{
					uint8_t ok;
					uint8_t used;
					tcp_header_t header;
				}tcp;
			};
		}ipv4;
	};
};

typedef struct net_recv_struct net_recv_t;

struct net_ipv4_struct{
	int ifnum;
	uint32_t dest_ip;
	uint32_t src_ip;
	uint8_t protocol;
	void *data;
	int16_t len;
};

typedef struct net_ipv4_struct net_ipv4_t;

struct net_icmp_struct{
	int ifnum;
	uint32_t dest_ip;
	uint32_t src_ip;
	uint8_t type;
	uint8_t code;
	void *header;
	void *data;
	int16_t len;
};

typedef struct net_icmp_struct net_icmp_t;

struct net_igmp_struct{
	int ifnum;
	uint32_t dest_ip;
	uint32_t src_ip;
	uint8_t type;
	uint8_t max_resp;
	uint32_t group_address;
	void *data;
	int16_t len;
};

typedef struct net_igmp_struct net_igmp_t;

struct net_udp_struct{
	int ifnum;
	uint32_t dest_ip;
	uint32_t src_ip;
	uint16_t dest_port;
	uint16_t src_port;
	void *data;
	int16_t len;
};

typedef struct net_udp_struct net_udp_t;

struct net_tcp_struct{
	uint32_t dest_ip;
	uint32_t src_ip;
	uint16_t dest_port;
	uint16_t src_port;
	uint32_t seq;
	uint32_t ack;
	uint16_t flags;
	uint16_t win;
	void *data;
	int16_t len;
};

typedef struct net_tcp_struct net_tcp_t;

void net_init();
void net_poll();

// IPV4
void net_recv_ipv4_raw(int ifnum, const void *buf, int16_t count);
int net_recv_ipv4(net_ipv4_t *ipv4);
int16_t net_send_ipv4(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint8_t protocol, const void *buf, int16_t count);

// ICMP
void net_recv_icmp_raw(net_recv_t *recv);
int net_recv_icmp(net_icmp_t *icmp);
int16_t net_send_icmp(uint32_t dest_ip, uint32_t src_ip, uint8_t type, uint8_t code, const void *header, const void *buf, int16_t count);

// IGMP
void net_recv_igmp_raw(net_recv_t *recv);
int net_recv_igmp(net_igmp_t *igmp);
int16_t net_send_igmp(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint8_t type, uint8_t max_resp, uint32_t group_address, const void *buf, int16_t count);

// UDP
void net_recv_udp_raw(net_recv_t *recv);
int net_recv_udp(net_udp_t *udp);
int16_t net_send_udp(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, int16_t count);

// TCP
void net_recv_tcp_raw(net_recv_t *recv);
int net_recv_tcp(net_tcp_t *tcp);
void net_used_tcp();
int16_t net_send_tcp(uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, uint32_t seq, uint32_t ack, uint16_t flags, uint16_t win, const void *buf, int16_t count);

// SOCKET
void net_connected_socket(int id);
void net_accept_socket(uint16_t port);
void net_recv_socket(int id);
void net_disconnected_socket(int id);
int net_socket_listen(uint16_t port);
void net_socket_nolisten(uint16_t port);
int net_socket_accept(uint16_t port);
int net_socket_connect(uint32_t dest_ip, uint16_t dest_port);
int net_socket_connected(int id);
int net_socket_read(int id, void *buf, int count);
int net_socket_write(int id, const void *buf, int count);
int net_socket_write_pending(int id);
void net_socket_shutdown(int id);
void net_socket_close(int id);

void net_print_mac(char *prefix, const void *mac);
void net_print_ip(char *prefix, uint32_t ip);
uint32_t net_timer_get();
uint32_t net_rand();

#endif

