#include "arp.h"
#include "net.h"
#include <string.h>

int arp_parse_raw(arp_header_t *header, const void *buf, int16_t count){
	if(count < ARP_HEADER_LEN)return(NET_ERROR_FORMAT);
	header->hlen = arp_hlen(buf);
	header->plen = arp_plen(buf);
	if(count < (ARP_HEADER_LEN + header->hlen + header->plen + header->hlen + header->plen))return(NET_ERROR_FORMAT);
	header->htype = arp_htype(buf);
	header->ptype = arp_ptype(buf);
	header->oper = arp_oper(buf);
	header->sha = arp_sha(buf);
	header->spa = arp_spa(buf);
	header->tha = arp_tha(buf);
	header->tpa = arp_tpa(buf);
	return(0);
}

int arp_parse_ethernet_ipv4_raw(arp_ethernet_ipv4_header_t *header, const void *buf, int16_t count){
	arp_header_t arp_header;
	if(arp_parse_raw(&arp_header, buf, count) < 0)return(NET_ERROR_FORMAT);
	if((arp_header.htype != ARP_HTYPE_ETHERNET) || (arp_header.ptype != ARP_PTYPE_IPV4) || (arp_header.hlen != ARP_HLEN_ETHERNET) || (arp_header.plen != ARP_PLEN_IPV4))return(NET_ERROR_FORMAT);
	header->oper = arp_header.oper;
	header->sender_mac = arp_mac(arp_header.sha);
	header->sender_ip = arp_ipv4(arp_header.spa);
	header->target_mac = arp_mac(arp_header.tha);
	header->target_ip = arp_ipv4(arp_header.tpa);
	return(0);
}

int16_t arp_write_ethernet_ipv4(void *dest, int16_t max, uint16_t oper, void *target_mac, void *sender_mac, uint32_t target_ip, uint32_t sender_ip){
	uint8_t tpa[4];
	uint8_t spa[4];
	tpa[0] = (target_ip >> 24) & 0xFF;
	tpa[1] = (target_ip >> 16) & 0xFF;
	tpa[2] = (target_ip >> 8) & 0xFF;
	tpa[3] = (target_ip >> 0) & 0xFF;
	spa[0] = (sender_ip >> 24) & 0xFF;
	spa[1] = (sender_ip >> 16) & 0xFF;
	spa[2] = (sender_ip >> 8) & 0xFF;
	spa[3] = (sender_ip >> 0) & 0xFF;
	return(arp_write(dest, max, ARP_HTYPE_ETHERNET, ARP_PTYPE_IPV4, ARP_HLEN_ETHERNET, ARP_PLEN_IPV4, oper, target_mac, sender_mac, tpa, spa));
}

int16_t arp_write(void *dest, int16_t max, uint16_t htype, uint16_t ptype, uint8_t hlen, uint8_t plen, uint16_t oper, void *tha, void *sha, void *tpa, void *spa){
	if((ARP_HEADER_LEN + hlen + plen + hlen + plen) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[ARP_HEADER_LEN], sha, hlen);
	memmove(&((uint8_t *)dest)[ARP_HEADER_LEN + hlen], spa, plen);
	memmove(&((uint8_t *)dest)[ARP_HEADER_LEN + hlen + plen], tha, hlen);
	memmove(&((uint8_t *)dest)[ARP_HEADER_LEN + hlen + plen + hlen], tpa, plen);
	arp_htype_set(dest, htype);
	arp_ptype_set(dest, ARP_PTYPE_IPV4);
	arp_hlen_set(dest, hlen);
	arp_plen_set(dest, ARP_PLEN_IPV4);
	arp_oper_set(dest, oper);
	return(ARP_HEADER_LEN + hlen + plen + hlen + plen);
}

