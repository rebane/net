#include "ahdlc.h"
#include "net.h"

int ahdlc_parse_raw(ahdlc_header_t *header, const void *buf, int16_t count){
	int16_t i;
	uint16_t crc;
	if(count < 3)return(NET_ERROR_FORMAT);
	if((((uint8_t *)buf)[0] == 0xFF) && (((uint8_t *)buf)[1] == 0x03)){
		header->data = (void *)&((uint8_t *)buf)[2];
		header->len = count - 4;
	}else{
		header->data = (void *)buf;
		header->len = count - 2;
	}
	if(header->len < 1)return(NET_ERROR_FORMAT);
	if(((uint8_t *)header->data)[0] & 0x01){
		header->protocol = ((uint8_t *)header->data)[0];
		header->data = &((uint8_t *)header->data)[1];
		header->len -= 1;
	}else{
		if(header->len < 2)return(NET_ERROR_FORMAT);
		header->protocol = ((uint16_t)((uint8_t *)header->data)[0] << 8) | ((uint8_t *)header->data)[1];
		header->data = &((uint8_t *)header->data)[2];
		header->len -= 2;
	}
	crc = 0xFFFF;
	for(i = 0; i < count; i++){
		crc = ahdlc_crc(crc, ((uint8_t *)buf)[i]);
	}
	if(crc != 0xF0B8)return(NET_ERROR_FORMAT);
	return(0);
}

int16_t ahdlc_write(void *dest, int16_t max, uint16_t protocol, const void *buf, int16_t count){
	int16_t i;
	uint16_t crc;
	if(count < 0)return(count);
	if((AHDLC_HEADER_LEN + count + 2) > max)return(NET_ERROR_MEM);
	ahdlc_address_set(dest, 0xFF);
	ahdlc_control_set(dest, 0x03);
	ahdlc_protocol_set(dest, protocol);
	crc = 0xFFFF;
	crc = ahdlc_crc(crc, ((uint8_t *)dest)[0]);
	crc = ahdlc_crc(crc, ((uint8_t *)dest)[1]);
	crc = ahdlc_crc(crc, ((uint8_t *)dest)[2]);
	crc = ahdlc_crc(crc, ((uint8_t *)dest)[3]);
	for(i = 0; i < count; i++){
		((uint8_t *)dest)[AHDLC_HEADER_LEN + i] = ((uint8_t *)buf)[i];
		crc = ahdlc_crc(crc, ((uint8_t *)buf)[i]);
	}
	crc ^= 0xFFFF;
	((uint8_t *)dest)[AHDLC_HEADER_LEN + count + 0] = (crc >> 0) & 0xFF;
	((uint8_t *)dest)[AHDLC_HEADER_LEN + count + 1] = (crc >> 8) & 0xFF;
	return(AHDLC_HEADER_LEN + count + 2);
}

uint16_t ahdlc_crc(uint16_t crc, uint8_t c){
	uint16_t b;
	b = (crc ^ c) & 0xFF;
	b = (b ^ (b << 4)) & 0xFF;
	b = (b << 8) ^ (b << 3) ^ (b >> 4);
	return((crc >> 8) ^ b);
}

