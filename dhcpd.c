#include "dhcpd.h"
#include "net.h"
#include "interface.h"
#include "dhcp.h"

static uint32_t dhcpd_ip_get(dhcpd_t *dhcpd, const void *mac);

void dhcpd_init(dhcpd_t *dhcpd, int ifnum){
	int i;
	interface_t *interface;
	interface = interface_get(ifnum);
	dhcpd->ifnum = ifnum;
	dhcpd->ip = interface->ip;
	dhcpd->nm = interface->nm;
	dhcpd->gw = interface->gw;
	dhcpd->bc = interface->bc;
	dhcpd->net = interface->net;
	dhcpd->ns = interface->ns;
	for(i = 0; i < DHCPD_TABLE_SIZE; i++){
		dhcpd->table[i].mac[0] = dhcpd->table[i].mac[1] = dhcpd->table[i].mac[2] = dhcpd->table[i].mac[3] = dhcpd->table[i].mac[4] = dhcpd->table[i].mac[5] = 0;
		dhcpd->table[i].timestamp = 0;
	}
}

void dhcpd_poll(dhcpd_t *dhcpd){
	net_udp_t udp;
	dhcp_header_t dhcp_header;
	dhcp_option_t dhcp_option;
	uint8_t type;
	uint32_t ip;
	int16_t len;
	if(!net_recv_udp(&udp))return;
	if(udp.dest_port != DHCP_SERVER_PORT)return;
	if(udp.src_port != DHCP_CLIENT_PORT)return;
	if(dhcp_parse_raw(&dhcp_header, udp.data, udp.len) < 0)return;
	if(dhcp_header.op != DHCP_OP_REQUEST)return;
	if(dhcp_header.htype != DHCP_HTYPE_ETHERNET)return;
	if(dhcp_header.hlen != 6)return;
	type = 0;
	for(dhcp_option_first(&dhcp_option, dhcp_header.data, dhcp_header.len); dhcp_option_valid(&dhcp_option); dhcp_option_next(&dhcp_option)){
		if(dhcp_option.option == 53){
			if(dhcp_option.len){
				type = dhcp_option.param[0];
			}
		}
	}
	if((type != 1) && (type != 3))return;
	printf("DHCPD PACKET\n");
	ip = dhcpd_ip_get(dhcpd, dhcp_header.chaddr);

	len = 0;
	if(type == 1){
		len = dhcp_option_write(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 53, "\x02", 1);
	}else{
		len = dhcp_option_write(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 53, "\x05", 1);
	}
	len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 1, dhcpd->nm);
	len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 3, dhcpd->gw);
	len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 6, dhcpd->ns);
	len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 54, dhcpd->ip);
	len = dhcp_write(net_send_buffer_dhcp, net_send_buffer_dhcp_len, DHCP_OP_REPLY, dhcp_header.xid, ip, dhcpd->ip, dhcp_header.chaddr, net_send_buffer_dhcp_option, len);
	len = udp_write(net_send_buffer_udp, net_send_buffer_udp_len, 0xFFFFFFFF, 0x00000000, DHCP_CLIENT_PORT, DHCP_SERVER_PORT, net_send_buffer_dhcp, len);
	len = ipv4_write(net_send_buffer_ipv4, net_send_buffer_ipv4_len, 0xFFFFFFFF, 0x00000000, IPV4_PROTOCOL_UDP, net_send_buffer_udp, len);
	interface_ethernet_send_ethernet(dhcpd->ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ETHERNET_TYPE_IPV4, net_send_buffer_ipv4, len);
}

static uint32_t dhcpd_ip_get(dhcpd_t *dhcpd, const void *mac){
	int i, j;
	uint32_t lowest_timestamp;
	j = 0;
	lowest_timestamp = 0xFFFFFFFF;
	for(i = 0; i < DHCPD_TABLE_SIZE; i++){
		if(!memcmp(dhcpd->table[i].mac, mac, 6)){
			dhcpd->table[i].timestamp = net_timer_get();
			return(dhcpd->table[i].ip);
		}
		if(dhcpd->table[i].timestamp < lowest_timestamp){
			lowest_timestamp = dhcpd->table[i].timestamp;
			j = i;
		}
	}
	memcpy(dhcpd->table[j].mac, mac, 6);
	dhcpd->table[j].ip = 0xC0A8020A + j;
	dhcpd->table[j].timestamp = net_timer_get();
	net_print_ip("DHCPD INSERT IP: ", dhcpd->table[j].ip);
	net_print_mac("DHCPD INSERT MAC: ", dhcpd->table[j].mac);
	return(dhcpd->table[j].ip);
}

