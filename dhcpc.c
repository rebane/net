#include "dhcpc.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "net.h"
#include "ethernet.h"
#include "ipv4.h"
#include "udp.h"
#include "dhcp.h"

#define DHCPC_STATE_DISCOVER 1
#define DHCPC_STATE_REQUEST  2
#define DHCPC_STATE_DONE     3
#define DHCPC_STATE_ERR      4

static void dhcpc_packet_poll(dhcpc_t *dhcpc);

void dhcpc_init(dhcpc_t *dhcpc, int ifnum){
	dhcpc->state = DHCPC_STATE_DISCOVER;
	dhcpc->ifnum = ifnum;
	dhcpc->xid = net_rand();
	dhcpc->timer = net_timer_get() + 1;
}

void dhcpc_poll(dhcpc_t *dhcpc){
	int16_t len;
	uint32_t timer;
	if((dhcpc->state == DHCPC_STATE_DISCOVER) || (dhcpc->state == DHCPC_STATE_REQUEST))dhcpc_packet_poll(dhcpc);
	if(dhcpc->state == DHCPC_STATE_DISCOVER){
		timer = net_timer_get();
		if(timer > dhcpc->timer){
			len = 0;
			len = dhcp_option_write(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 53, "\x01", 1);
			len = dhcp_option_write(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 55, "\x01\x03\x06", 3);
			len = dhcp_option_write_end(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len);
			len = dhcp_write(net_send_buffer_dhcp, net_send_buffer_dhcp_len, DHCP_OP_REQUEST, dhcpc->xid, 0x00000000, 0x00000000, interface_ethernet_mac_get(dhcpc->ifnum), net_send_buffer_dhcp_option, len);
			len = udp_write(net_send_buffer_udp, net_send_buffer_udp_len, 0xFFFFFFFF, 0x00000000, DHCP_SERVER_PORT, DHCP_CLIENT_PORT, net_send_buffer_dhcp, len);
			len = ipv4_write(net_send_buffer_ipv4, net_send_buffer_ipv4_len, 0xFFFFFFFF, 0x00000000, IPV4_PROTOCOL_UDP, net_send_buffer_udp, len);
			if(interface_ethernet_send_ethernet(dhcpc->ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ETHERNET_TYPE_IPV4, net_send_buffer_ipv4, len) < 0){
				dhcpc->state = DHCPC_STATE_ERR;
			}else{
				printf("Sending DHCP discover...\n");
				dhcpc->timer = timer + 5;
			}
		}
	}else if(dhcpc->state == DHCPC_STATE_REQUEST){
		timer = net_timer_get();
		if(timer > dhcpc->timer){
			dhcpc->try++;
			if(dhcpc->try > 5){
				dhcpc->state = DHCPC_STATE_DISCOVER;
				return;
			}
			len = 0;
			len = dhcp_option_write(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 53, "\x03", 1);
			len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 50, dhcpc->ip);
			len = dhcp_option_write_addr(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len, 54, dhcpc->server);
			len = dhcp_option_write_end(net_send_buffer_dhcp_option, net_send_buffer_dhcp_option_len, len);
			len = dhcp_write(net_send_buffer_dhcp, net_send_buffer_dhcp_len, DHCP_OP_REQUEST, dhcpc->xid, 0x00000000, dhcpc->server, interface_ethernet_mac_get(dhcpc->ifnum), net_send_buffer_dhcp_option, len);
			len = udp_write(net_send_buffer_udp, net_send_buffer_udp_len, 0xFFFFFFFF, 0x00000000, DHCP_SERVER_PORT, DHCP_CLIENT_PORT, net_send_buffer_dhcp, len);
			len = ipv4_write(net_send_buffer_ipv4, net_send_buffer_ipv4_len, 0xFFFFFFFF, 0x00000000, IPV4_PROTOCOL_UDP, net_send_buffer_udp, len);
			if(interface_ethernet_send_ethernet(dhcpc->ifnum, "\xFF\xFF\xFF\xFF\xFF\xFF", ETHERNET_TYPE_IPV4, net_send_buffer_ipv4, len) < 0){
				dhcpc->state = DHCPC_STATE_ERR;
			}else{
				printf("Sending DHCP request...\n");
				dhcpc->timer = timer + 5;
			}
		}
	}
}

static void dhcpc_packet_poll(dhcpc_t *dhcpc){
	int16_t len;
	void *data;
	int ok;
	ethernet_header_t ethernet_header;
	ipv4_header_t ipv4_header;
	udp_header_t udp_header;
	dhcp_header_t dhcp_header;
	dhcp_option_t option;
	data = interface_ethernet_recv_ethernet(dhcpc->ifnum, &len);
	if(ethernet_parse_raw(&ethernet_header, data, len) < 0)return;
	if(ethernet_header.type != ETHERNET_TYPE_IPV4)return;
	if(ipv4_parse_raw(&ipv4_header, ethernet_header.data, ethernet_header.len) < 0)return;
	if(ipv4_header.protocol != IPV4_PROTOCOL_UDP)return;
	if(udp_parse_raw(&udp_header, ipv4_header.dest_ip, ipv4_header.src_ip, ipv4_header.data, ipv4_header.len) < 0)return;
	if(udp_header.dest_port != DHCP_CLIENT_PORT)return;
	if(udp_header.src_port != DHCP_SERVER_PORT)return;
	if(dhcp_parse_raw(&dhcp_header, udp_header.data, udp_header.len) < 0)return;
	if(dhcp_header.op != DHCP_OP_REPLY)return;
	if(dhcp_header.htype != DHCP_HTYPE_ETHERNET)return;
	if(dhcp_header.hlen != 6)return;
	if(dhcp_header.xid != dhcpc->xid)return;
	if(memcmp(dhcp_header.chaddr, interface_ethernet_mac_get(dhcpc->ifnum), 6))return;
	printf("DHCPC PACKET!\n");
	ok = 0;
	if(dhcpc->state == DHCPC_STATE_DISCOVER){
		dhcpc->ip = dhcp_header.yiaddr;
		dhcpc->nm = 0;
		dhcpc->gw = 0;
		dhcpc->ns = 0;
		dhcpc->server = dhcp_header.siaddr;
		for(dhcp_option_first(&option, dhcp_header.data, dhcp_header.len); dhcp_option_valid(&option); dhcp_option_next(&option)){
			if(option.option == 53){ // DHCP message type
				if(option.len == 1){
					if(((uint8_t *)option.param)[0] == 2)ok = 1;
				}
			}else if(option.option == 1){ // subnet mask
				if(option.len == 4){
					dhcpc->nm = dhcp_option_iaddr(option.param);
				}
			}else if(option.option == 3){ // router
				if(option.len >= 4){
					dhcpc->gw = dhcp_option_iaddr(option.param);
				}
			}else if(option.option == 6){ // nameserver
				if(option.len >= 4){
					dhcpc->ns = dhcp_option_iaddr(option.param);
				}
			}else if(option.option == 54){ // DHCP server
				if(option.len >= 4){
					dhcpc->server = dhcp_option_iaddr(option.param);
				}
			}
		}
		if(!ok)return;
		net_print_ip("DHCP offer from ", dhcpc->server);
		dhcpc->try = 0;
		dhcpc->timer = 0;
		dhcpc->state = DHCPC_STATE_REQUEST;
	}else if(dhcpc->state == DHCPC_STATE_REQUEST){
		for(dhcp_option_first(&option, dhcp_header.data, dhcp_header.len); dhcp_option_valid(&option); dhcp_option_next(&option)){
			if(option.option == 53){ // DHCP message type
				if(option.len == 1){
					if(((uint8_t *)option.param)[0] == 5)ok = 1;
				}
			}
		}
		if(!ok)return;
		net_print_ip("DHCP ack from ", dhcpc->server);
		dhcpc->state = DHCPC_STATE_DONE;
		interface_ipv4_set(dhcpc->ifnum, dhcpc->ip, dhcpc->nm, dhcpc->gw, dhcpc->ns);
		if(dhcpc->server == dhcpc->gw)interface_ethernet_arp_add(dhcpc->ifnum, dhcpc->gw, ethernet_header.src_mac);
	}
}

