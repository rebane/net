#ifndef _AHDLC_H_
#define _AHDLC_H_

#include <stdint.h>

#define AHDLC_HEADER_LEN                     4

#define AHDLC_PROTOCOL_LCP                   0xC021
#define AHDLC_PROTOCOL_PAP                   0xC023
#define AHDLC_PROTOCOL_IPCP                  0x8021
#define AHDLC_PROTOCOL_IPV4                  0x0021

#define ahdlc_address_set(buf, address)      (((uint8_t *)(buf))[0] = (address))
#define ahdlc_control_set(buf, control)      (((uint8_t *)(buf))[1] = (control))
#define ahdlc_protocol_set(buf, protocol)    do{ ((uint8_t *)(buf))[2] = (((uint16_t)(protocol) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(protocol) >> 0) & 0xFF); }while(0)

struct ahdlc_header_struct{
	uint16_t protocol;
	void *data;
	int16_t len;
};

typedef struct ahdlc_header_struct ahdlc_header_t;

int ahdlc_parse_raw(ahdlc_header_t *header, const void *buf, int16_t count);
int16_t ahdlc_write(void *dest, int16_t max, uint16_t protocol, const void *buf, int16_t count);
uint16_t ahdlc_crc(uint16_t crc, uint8_t c);

#endif

