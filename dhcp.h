#ifndef _DHCP_H_
#define _DHCP_H_

#include <stdint.h>
#include <string.h>

#define DHCP_HEADER_LEN                    240

#define DHCP_SERVER_PORT                   67
#define DHCP_CLIENT_PORT                   68

#define DHCP_OP_REQUEST                    1
#define DHCP_OP_REPLY                      2

#define DHCP_HTYPE_ETHERNET                1

#define DHCP_MAGIC_COOKIE                  0x63825363

#define dhcp_op(buf)                       (((uint8_t *)(buf))[0])
#define dhcp_htype(buf)                    (((uint8_t *)(buf))[1])
#define dhcp_hlen(buf)                     (((uint8_t *)(buf))[2])
#define dhcp_hops(buf)                     (((uint8_t *)(buf))[3])
#define dhcp_xid(buf)                      (((uint32_t)((uint8_t *)(buf))[4] << 24) | ((uint32_t)((uint8_t *)(buf))[5] << 16) | ((uint32_t)((uint8_t *)(buf))[6] << 8) | ((uint32_t)((uint8_t *)(buf))[7] << 0))
#define dhcp_secs(buf)                     (((uint16_t)((uint8_t *)(buf))[8] << 8) | (((uint8_t *)(buf))[9] << 0))
#define dhcp_flags(buf)                    (((uint16_t)((uint8_t *)(buf))[10] << 8) | (((uint8_t *)(buf))[11] << 0))
#define dhcp_yiaddr(buf)                   (((uint32_t)((uint8_t *)(buf))[16] << 24) | ((uint32_t)((uint8_t *)(buf))[17] << 16) | ((uint32_t)((uint8_t *)(buf))[18] << 8) | ((uint32_t)((uint8_t *)(buf))[19] << 0))
#define dhcp_siaddr(buf)                   (((uint32_t)((uint8_t *)(buf))[20] << 24) | ((uint32_t)((uint8_t *)(buf))[21] << 16) | ((uint32_t)((uint8_t *)(buf))[22] << 8) | ((uint32_t)((uint8_t *)(buf))[23] << 0))
#define dhcp_chaddr(buf)                   (&((uint8_t *)(buf))[28])
#define dhcp_magic_cookie(buf)             (((uint32_t)((uint8_t *)(buf))[236] << 24) | ((uint32_t)((uint8_t *)(buf))[237] << 16) | ((uint32_t)((uint8_t *)(buf))[238] << 8) | ((uint32_t)((uint8_t *)(buf))[239] << 0))

#define dhcp_op_set(buf, op)               (((uint8_t *)(buf))[0] = ((uint8_t)(op)))
#define dhcp_htype_set(buf, htype)         (((uint8_t *)(buf))[1] = ((uint8_t)(htype)))
#define dhcp_hlen_set(buf, hlen)           (((uint8_t *)(buf))[2] = ((uint8_t)(hlen)))
#define dhcp_hops_set(buf, hops)           (((uint8_t *)(buf))[3] = ((uint8_t)(hops)))
#define dhcp_xid_set(buf, xid)             do{ ((uint8_t *)(buf))[4] = (((uint32_t)(xid) >> 24) & 0xFF); ((uint8_t *)(buf))[5] = (((uint32_t)(xid) >> 16) & 0xFF); ((uint8_t *)(buf))[6] = (((uint32_t)(xid) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint32_t)(xid) >> 0) & 0xFF); }while(0)
#define dhcp_secs_set(buf, secs)           do{ ((uint8_t *)(buf))[8] = (((uint16_t)(secs) >> 8) & 0xFF); ((uint8_t *)(buf))[9] = (((uint16_t)(secs) >> 0) & 0xFF); }while(0)
#define dhcp_flags_set(buf, flags)         do{ ((uint8_t *)(buf))[10] = (((uint16_t)(flags) >> 8) & 0xFF); ((uint8_t *)(buf))[11] = (((uint16_t)(flags) >> 0) & 0xFF); }while(0)
#define dhcp_ciaddr_set(buf, ciaddr)       do{ ((uint8_t *)(buf))[12] = (((uint32_t)(ciaddr) >> 24) & 0xFF); ((uint8_t *)(buf))[13] = (((uint32_t)(ciaddr) >> 16) & 0xFF); ((uint8_t *)(buf))[14] = (((uint32_t)(ciaddr) >> 8) & 0xFF); ((uint8_t *)(buf))[15] = (((uint32_t)(ciaddr) >> 0) & 0xFF); }while(0)
#define dhcp_yiaddr_set(buf, yiaddr)       do{ ((uint8_t *)(buf))[16] = (((uint32_t)(yiaddr) >> 24) & 0xFF); ((uint8_t *)(buf))[17] = (((uint32_t)(yiaddr) >> 16) & 0xFF); ((uint8_t *)(buf))[18] = (((uint32_t)(yiaddr) >> 8) & 0xFF); ((uint8_t *)(buf))[19] = (((uint32_t)(yiaddr) >> 0) & 0xFF); }while(0)
#define dhcp_siaddr_set(buf, siaddr)       do{ ((uint8_t *)(buf))[20] = (((uint32_t)(siaddr) >> 24) & 0xFF); ((uint8_t *)(buf))[21] = (((uint32_t)(siaddr) >> 16) & 0xFF); ((uint8_t *)(buf))[22] = (((uint32_t)(siaddr) >> 8) & 0xFF); ((uint8_t *)(buf))[23] = (((uint32_t)(siaddr) >> 0) & 0xFF); }while(0)
#define dhcp_giaddr_set(buf, giaddr)       do{ ((uint8_t *)(buf))[24] = (((uint32_t)(giaddr) >> 24) & 0xFF); ((uint8_t *)(buf))[25] = (((uint32_t)(giaddr) >> 16) & 0xFF); ((uint8_t *)(buf))[26] = (((uint32_t)(giaddr) >> 8) & 0xFF); ((uint8_t *)(buf))[27] = (((uint32_t)(giaddr) >> 0) & 0xFF); }while(0)
#define dhcp_chaddr_set(buf, chaddr, hlen) do { memmove(&((uint8_t *)(buf))[28], (chaddr), (hlen)); memset(&((uint8_t *)(buf))[28 + (hlen)], 0, 16 - (hlen)); }while(0)
#define dhcp_magic_cookie_set(buf, mc)     do{ ((uint8_t *)(buf))[236] = (((uint32_t)(mc) >> 24) & 0xFF); ((uint8_t *)(buf))[237] = (((uint32_t)(mc) >> 16) & 0xFF); ((uint8_t *)(buf))[238] = (((uint32_t)(mc) >> 8) & 0xFF); ((uint8_t *)(buf))[239] = (((uint32_t)(mc) >> 0) & 0xFF); }while(0)

#define dhcp_option_iaddr(buf)             (((uint32_t)((uint8_t *)(buf))[0] << 24) | ((uint32_t)((uint8_t *)(buf))[1] << 16) | ((uint32_t)((uint8_t *)(buf))[2] << 8) | ((uint32_t)((uint8_t *)(buf))[3] << 0))

struct dhcp_header_struct{
	uint8_t op;
	uint8_t htype;
	uint8_t hlen;
	uint32_t xid;
//	uint32_t ciaddr;
	uint32_t yiaddr;
	uint32_t siaddr;
//	uint32_t giaddr;
	uint8_t *chaddr;
	void *data;
	uint16_t len;
};

struct dhcp_option_struct{
	uint8_t option;
	uint8_t len;
	uint8_t *param;
	uint8_t *buf;
	int16_t size;
	int16_t next;
};

typedef struct dhcp_header_struct dhcp_header_t;
typedef struct dhcp_option_struct dhcp_option_t;

int dhcp_parse_raw(dhcp_header_t *header, const void *buf, int16_t count);
int16_t dhcp_write(void *dest, int16_t max, uint8_t op, uint32_t xid, uint32_t yiaddr, uint32_t siaddr, const void *chaddr, const void *buf, int16_t count);

int16_t dhcp_option_write(void *dest, int16_t max, int16_t offset, uint8_t type, const void *buf, uint8_t count);
int16_t dhcp_option_write_addr(void *dest, int16_t max, int16_t offset, uint8_t type, uint32_t addr);
int16_t dhcp_option_write_end(void *dest, int16_t max, int16_t offset);

void dhcp_option_first(dhcp_option_t *option, const void *buf, int16_t count);
uint8_t dhcp_option_valid(dhcp_option_t *option);
void dhcp_option_next(dhcp_option_t *option);

#endif

