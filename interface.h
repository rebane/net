#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include <stdint.h>

#define INTERFACE_TYPE_NONE            0
#define INTERFACE_TYPE_ETHERNET        1
#define INTERFACE_TYPE_PPP             2

#define INTERFACE_ETHERNET_ARP_MAX     6

#define INTERFACE_PPP_READ_BUFFER_LEN  1500
#define INTERFACE_PPP_WRITE_FIFO_LEN   1500

struct interface_struct{
	uint8_t enabled;
	uint8_t link;
	uint32_t ip;
	uint32_t nm;
	uint32_t bc;
	uint32_t net;
	uint32_t gw;
	uint32_t ns;
};

struct interface_ethernet_struct{
	struct interface_struct interface;
	void*(*recv)(int16_t *len);
	int16_t(*send)(const void *buf, int16_t count);
	uint8_t mac[6];
	struct{
		uint32_t ip;
		uint8_t mac[6];
		uint8_t count;
		uint32_t used_timeout;
		uint32_t refresh_timeout;
	}arp_table[INTERFACE_ETHERNET_ARP_MAX];
	uint32_t arp_temp_ip;
	uint8_t arp_temp_mac[6];
	void *data;
	int16_t len;
};

struct interface_ppp_struct{
	struct interface_struct interface;
	int16_t(*read)(void *buf, int16_t count);
	int16_t(*write)(const void *buf, int16_t count);
	int16_t ipv4_len;
	uint8_t read_buffer[INTERFACE_PPP_READ_BUFFER_LEN];
	uint8_t read_esc;
	uint16_t read_loc;
	uint8_t write_fifo[INTERFACE_PPP_WRITE_FIFO_LEN];
	uint16_t write_fifo_in;
	uint16_t write_fifo_out;
	uint16_t write_fifo_len;
	uint32_t timer;
	uint8_t state;

	uint8_t identifier;
	uint8_t ok_local;
	uint8_t ok_remote;

	uint8_t auth;
	uint8_t ask_ns1;
	uint8_t ask_ns2;

	uint32_t ip_local;
	uint32_t ip_remote;
	uint32_t ns1;
	uint32_t ns2;
};

typedef struct interface_struct interface_t;
typedef struct interface_ethernet_struct interface_ethernet_t;
typedef struct interface_ppp_struct interface_ppp_t;

void interface_init();
int interface_add(int type, interface_t *iface, void *custom);
interface_t *interface_get(int ifnum);
void *interface_get_custom(int ifnum);
int interface_type(int ifnum);
void interface_poll();
void interface_link_set(int ifnum, uint8_t link);
void interface_enable(int ifnum);
void interface_disable(int ifnum);
void interface_ipv4_set(int ifnum, uint32_t ip, uint32_t nm, uint32_t gw, uint32_t ns);
int interface_route_get(int ifnum, uint32_t dest_ip, uint32_t *src_ip, uint32_t *gw);
int interface_route_is_local(int ifnum, uint32_t dest_ip);
int16_t interface_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count);
uint32_t interface_ip_get(int ifnum);
int interface_ns_get(uint32_t *ns);

// ETHERNET
int interface_ethernet_add(interface_ethernet_t *interface_ethernet, void*(*recv)(int16_t *len), int16_t(*send)(const void *buf, int16_t count));
interface_ethernet_t *interface_ethernet_get(int ifnum);
int16_t interface_ethernet_poll(int ifnum, interface_ethernet_t *interface_ethernet);
void interface_ethernet_mac_set(int ifnum, void *mac);
void *interface_ethernet_mac_get(int ifnum);
void interface_ethernet_arp_add(int ifnum, uint32_t ip, void *mac);
void *interface_ethernet_recv_ethernet(int ifnum, int16_t *len);
int16_t interface_ethernet_send_ethernet(int ifnum, const void *dest_mac, uint16_t type, const void *buf, int16_t count);
int16_t interface_ethernet_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count);

// PPP
int interface_ppp_add(interface_ppp_t *interface_ppp, int16_t(*read)(void *buf, int16_t count), int16_t(*write)(const void *buf, int16_t count));
interface_ppp_t *interface_ppp_get(int ifnum);
int16_t interface_ppp_poll(int ifnum, interface_ppp_t *interface_ppp);
int16_t interface_ppp_send_ahdlc(int ifnum, uint16_t protocol, const void *buf, int16_t count);
int16_t interface_ppp_send_lcp(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count);
int16_t interface_ppp_send_pap(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count, const void *buf2, int16_t count2);
int16_t interface_ppp_send_ipcp(int ifnum, uint8_t code, uint8_t identifier, const void *buf, int16_t count);
int16_t interface_ppp_send_ipv4_raw(int ifnum, uint32_t gw, const void *buf, int16_t count);

#endif

