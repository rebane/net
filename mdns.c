#include "mdns.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "net.h"
#include "interface.h"
#include "dns.h"

static uint8_t mdns_name2num(uint8_t *name, uint8_t len);

int mdns_recv_match_query_a_cstr(const char *name, int *ifnum){
	net_udp_t udp;
	dns_section_t d;
	dns_name_t name1, name2;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != MDNS_DEST_PORT)return(0);
	for(dns_section_first(&d, udp.data, udp.len); dns_section_valid(&d); dns_section_next(&d)){
		if(!(dns_flags(udp.data) & 0x8000) && (d.section_type == DNS_SECTION_QUESTION) && (d.type == DNS_TYPE_A)){
			// if(dns_name_isequal_cstr(udp.data, udp.len, d.name, name)){
			if(dns_name_isequal(dns_name_label(&name1, udp.data, udp.len, d.name), dns_name_cstr(&name2, name))){
				*ifnum = udp.ifnum;
				return(1);
			}
		}
	}
	return(0);
}

int mdns_recv_match_query_ptr(uint32_t ip, int *ifnum){
	net_udp_t udp;
	dns_section_t d;
	dns_label_t label;
	uint32_t ip2;
	uint8_t i;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != MDNS_DEST_PORT)return(0);
	for(dns_section_first(&d, udp.data, udp.len); dns_section_valid(&d); dns_section_next(&d)){
		if(!(dns_flags(udp.data) & 0x8000) && (d.section_type == DNS_SECTION_QUESTION) && (d.type == DNS_TYPE_PTR)){
			ip2 = 0;
			i = 0;
			for(dns_label_first(&label, udp.data, udp.len, d.name); dns_label_valid(&label); dns_label_next(&label)){
				if(i == 0){
					ip2 |= ((uint32_t)mdns_name2num(&((uint8_t *)udp.data)[label.label], label.length) << 0);
				}else if(i == 1){
					ip2 |= ((uint32_t)mdns_name2num(&((uint8_t *)udp.data)[label.label], label.length) << 8);
				}else if(i == 2){
					ip2 |= ((uint32_t)mdns_name2num(&((uint8_t *)udp.data)[label.label], label.length) << 16);
				}else if(i == 3){
					ip2 |= ((uint32_t)mdns_name2num(&((uint8_t *)udp.data)[label.label], label.length) << 24);
				}
				i++;
			}
			if((i == 6) && (ip == ip2)){
				*ifnum = udp.ifnum;
				return(1);
			}
		}
	}
	return(0);
}

int mdns_recv_match_query_ptr_local(int *ifnum){
	net_udp_t udp;
	uint32_t ip;
	if(!net_recv_udp(&udp))return(0);
	if(udp.dest_port != MDNS_DEST_PORT)return(0);
	ip = interface_ip_get(udp.ifnum);
	return(mdns_recv_match_query_ptr(ip, ifnum));
}

int16_t mdns_send_answer_a_cstr(int ifnum, const char *name, uint32_t ip){
	int16_t len;
	dns_id_set(net_send_buffer_udp, 0);
	dns_flags_set(net_send_buffer_udp, 0x8400);
	dns_qdcount_set(net_send_buffer_udp, 0);
	dns_ancount_set(net_send_buffer_udp, 1);
	dns_nscount_set(net_send_buffer_udp, 0);
	dns_arcount_set(net_send_buffer_udp, 0);
	len = DNS_HEADER_LEN;
	len += dns_name_encode_cstr(&net_send_buffer_udp[len], 512, name);
	dns_uint16_set(&net_send_buffer_udp[len], 0x0001); len += 2;
	dns_uint16_set(&net_send_buffer_udp[len], 0x8001); len += 2;
	dns_uint32_set(&net_send_buffer_udp[len], 300); len += 4;
	dns_uint16_set(&net_send_buffer_udp[len], 4); len += 2;
	net_send_buffer_udp[len++] = ((ip >> 24) & 0xFF);
	net_send_buffer_udp[len++] = ((ip >> 16) & 0xFF);
	net_send_buffer_udp[len++] = ((ip >> 8) & 0xFF);
	net_send_buffer_udp[len++] = ((ip >> 0) & 0xFF);
	return(net_send_udp(ifnum, 0xE00000FB, 0, MDNS_DEST_PORT, MDNS_DEST_PORT, net_send_buffer_udp, len));
}

int16_t mdns_send_answer_ptr_cstr(int ifnum, uint32_t ip, const char *name){
	int16_t len, name_len_loc, name_len;
	char buffer[32];
	dns_id_set(net_send_buffer_udp, 0);
	dns_flags_set(net_send_buffer_udp, 0x8400);
	dns_qdcount_set(net_send_buffer_udp, 0);
	dns_ancount_set(net_send_buffer_udp, 1);
	dns_nscount_set(net_send_buffer_udp, 0);
	dns_arcount_set(net_send_buffer_udp, 0);
	len = DNS_HEADER_LEN;
	snprintf(buffer, 32, "%u.%u.%u.%u.in-addr.arpa", (unsigned int)((ip >> 0) & 0xFF), (unsigned int)((ip >> 8) & 0xFF), (unsigned int)((ip >> 16) & 0xFF), (unsigned int)((ip >> 24) & 0xFF));
	len += dns_name_encode_cstr(&net_send_buffer_udp[len], 512, buffer);
	dns_uint16_set(&net_send_buffer_udp[len], 0x000C); len += 2;
	dns_uint16_set(&net_send_buffer_udp[len], 0x8001); len += 2;
	dns_uint32_set(&net_send_buffer_udp[len], 300); len += 4;
	name_len_loc = len; len += 2;
	name_len = dns_name_encode_cstr(&net_send_buffer_udp[len], 512, name);
	dns_uint16_set(&net_send_buffer_udp[name_len_loc], name_len);
	len += name_len;
	return(net_send_udp(ifnum, 0xE00000FB, 0, MDNS_DEST_PORT, MDNS_DEST_PORT, net_send_buffer_udp, len));
}

static uint8_t mdns_name2num(uint8_t *name, uint8_t len){
	if(len == 1){
		return(name[0] - '0');
	}else if(len == 2){
		return(((name[0] - '0') * 10) + (name[1] - '0'));
	}else if(len == 3){
		return(((name[0] - '0') * 100) + ((name[1] - '0') * 10) + (name[2] - '0'));
	}
	return(0);
}

