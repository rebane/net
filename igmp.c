#include "igmp.h"
#include "net.h"
#include "ipv4.h"
#include <stdio.h>

int igmp_parse_raw(igmp_header_t *header, const void *buf, int16_t count){
	if(count < IGMP_HEADER_LEN)return(NET_ERROR_FORMAT);
	if(igmp_chk(buf) != igmp_chk_calc(buf, count))return(NET_ERROR_FORMAT);
	header->type = igmp_type(buf);
	header->max_resp = igmp_max_resp(buf);
	header->group_address = igmp_group_address(buf);
	return(0);
}

int16_t igmp_write(void *dest, int16_t max, uint8_t type, uint8_t max_resp, uint32_t group_address, const void *buf, int16_t count){
	if(count < 0)return(count);
	if((IGMP_HEADER_LEN + count) > max)return(NET_ERROR_MEM);
	memmove(&((uint8_t *)dest)[IGMP_HEADER_LEN], buf, count);
	igmp_type_set(dest, type);
	igmp_max_resp_set(dest, max_resp);
	igmp_group_address_set(dest, group_address);
	igmp_chk_set(dest, igmp_chk_calc(dest, IGMP_HEADER_LEN + count));
	return(IGMP_HEADER_LEN + count);
}

uint16_t igmp_chk_calc(const void *buf, int16_t count){
	uint32_t sum;
	int16_t i;
	sum = 0;
	for(i = 0; i < count; i += 2){
		if(i == 2)continue;
		if((i + 1) >= count){
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8);
		}else{
			sum += ((uint16_t)((uint8_t *)buf)[i] << 8) | (uint16_t)((uint8_t *)buf)[i + 1];
		}
	}
	while(sum >> 16)sum = (sum & 0xFFFF) + (sum >> 16);
	return((~sum) & 0xFFFF);
}

