#include "net.h"
#include "platform.h"
#include "mtimer.h"
#include "interface.h"
#include "dhcpd.h"
#include "server.h"
#include <stdio.h>

char *net_tcp_flags_cstr(uint16_t flags);

uint8_t net_send_buffer[NET_SEND_BUFFER_LEN];

static net_recv_t net_recv;

void net_init(){
	interface_init();
	socket_init();
}

void net_poll(){
	net_recv.buf = (void *)0;
	net_recv.count = 0;
	net_recv.ipv4.ok = 0;
	interface_poll();
}

// IPV4
void net_recv_ipv4_raw(int ifnum, const void *buf, int16_t count){
	net_recv.ifnum = ifnum;
	net_recv.buf = (void *)buf;
	net_recv.count = count;
	if(ipv4_parse_raw(&net_recv.ipv4.header, buf, count) < 0)return;
	if(!interface_route_is_local(ifnum, net_recv.ipv4.header.dest_ip)){
		// net_send_ipv4(-1, net_recv.ipv4.header.dest_ip, net_recv.ipv4.header.src_ip, net_recv.ipv4.header.protocol, buf, count);
		return;
	}
	net_recv.ipv4.ok = 1;
	net_recv.ipv4.next = 0;
}

int net_recv_ipv4(net_ipv4_t *ipv4){
	if(!net_recv.count || !net_recv.ipv4.ok)return(0);
	ipv4->ifnum = net_recv.ifnum;
	ipv4->dest_ip = net_recv.ipv4.header.dest_ip;
	ipv4->src_ip = net_recv.ipv4.header.src_ip;
	ipv4->protocol = net_recv.ipv4.header.protocol;
	ipv4->data = net_recv.ipv4.header.data;
	ipv4->len = net_recv.ipv4.header.len;
	return(1);
}

int16_t net_send_ipv4(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint8_t protocol, const void *buf, int16_t count){
	uint32_t gw;
	if(count < 0)return(count);
	ifnum = interface_route_get(ifnum, dest_ip, &src_ip, &gw);
	if(ifnum < 0)return(ifnum); // no route to host
/*	printf("ROUTE IF: %d\n", interface);
	net_print_ip("DEST IP: ", dest_ip);
	net_print_ip("GW: ", gw);
	net_print_ip("SRC: ", src_ip);*/
	count = ipv4_write(net_send_buffer_ipv4, net_send_buffer_ipv4_len, dest_ip, src_ip, protocol, buf, count);
	if(count < 0)return(count);
	return(interface_send_ipv4_raw(ifnum, gw, net_send_buffer_ipv4, count));
}

// ICMP
int net_recv_icmp(net_icmp_t *icmp){
	if(!net_recv.count || !net_recv.ipv4.ok || (net_recv.ipv4.header.protocol != IPV4_PROTOCOL_ICMP))return(0);
	if(!net_recv.ipv4.next){
		net_recv.ipv4.next = 1;
		if(icmp_parse_raw(&net_recv.ipv4.icmp.header, net_recv.ipv4.header.data, net_recv.ipv4.header.len) < 0){
			net_recv.ipv4.icmp.ok = 0;
			return(0);
		}
		net_recv.ipv4.icmp.ok = 1;
	}
	if(!net_recv.ipv4.icmp.ok)return(0);
	icmp->ifnum = net_recv.ifnum;
	icmp->dest_ip = net_recv.ipv4.header.dest_ip;
	icmp->src_ip = net_recv.ipv4.header.src_ip;
	icmp->type = net_recv.ipv4.icmp.header.type;
	icmp->code = net_recv.ipv4.icmp.header.code;
	icmp->header = net_recv.ipv4.icmp.header.header;
	icmp->data = net_recv.ipv4.icmp.header.data;
	icmp->len = net_recv.ipv4.icmp.header.len;
	return(1);
}

int16_t net_send_icmp(uint32_t dest_ip, uint32_t src_ip, uint8_t type, uint8_t code, const void *header, const void *buf, int16_t count){
	count = icmp_write(net_send_buffer_icmp, net_send_buffer_icmp_len, type, code, header, buf, count);
	if(count < 0)return(count);
	return(net_send_ipv4(-1, dest_ip, src_ip, IPV4_PROTOCOL_ICMP, net_send_buffer_icmp, count));
}

// IGMP
int net_recv_igmp(net_igmp_t *igmp){
	if(!net_recv.count || !net_recv.ipv4.ok || (net_recv.ipv4.header.protocol != IPV4_PROTOCOL_IGMP))return(0);
	if(!net_recv.ipv4.next){
		net_recv.ipv4.next = 1;
		if(igmp_parse_raw(&net_recv.ipv4.igmp.header, net_recv.ipv4.header.data, net_recv.ipv4.header.len) < 0){
			net_recv.ipv4.igmp.ok = 0;
			return(0);
		}
		net_recv.ipv4.igmp.ok = 1;
	}
	if(!net_recv.ipv4.igmp.ok)return(0);
	igmp->ifnum = net_recv.ifnum;
	igmp->dest_ip = net_recv.ipv4.header.dest_ip;
	igmp->src_ip = net_recv.ipv4.header.src_ip;
	igmp->type = net_recv.ipv4.igmp.header.type;
	igmp->max_resp = net_recv.ipv4.igmp.header.max_resp;
	igmp->group_address = net_recv.ipv4.igmp.header.group_address;
	igmp->data = net_recv.ipv4.igmp.header.data;
	igmp->len = net_recv.ipv4.igmp.header.len;
	return(1);
}

int16_t net_send_igmp(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint8_t type, uint8_t max_resp, uint32_t group_address, const void *buf, int16_t count){
	count = igmp_write(net_send_buffer_igmp, net_send_buffer_igmp_len, type, max_resp, group_address, buf, count);
	if(count < 0)return(count);
	return(net_send_ipv4(ifnum, dest_ip, src_ip, IPV4_PROTOCOL_IGMP, net_send_buffer_igmp, count));
}

// UDP
int net_recv_udp(net_udp_t *udp){
	if(!net_recv.count || !net_recv.ipv4.ok || (net_recv.ipv4.header.protocol != IPV4_PROTOCOL_UDP))return(0);
	if(!net_recv.ipv4.next){
		net_recv.ipv4.next = 1;
		if(udp_parse_raw(&net_recv.ipv4.udp.header, net_recv.ipv4.header.dest_ip, net_recv.ipv4.header.src_ip, net_recv.ipv4.header.data, net_recv.ipv4.header.len) < 0){
			net_recv.ipv4.udp.ok = 0;
			return(0);
		}
		net_recv.ipv4.udp.ok = 1;
	}
	if(!net_recv.ipv4.udp.ok)return(0);
	udp->ifnum = net_recv.ifnum;
	udp->dest_ip = net_recv.ipv4.header.dest_ip;
	udp->src_ip = net_recv.ipv4.header.src_ip;
	udp->dest_port = net_recv.ipv4.udp.header.dest_port;
	udp->src_port = net_recv.ipv4.udp.header.src_port;
	udp->data = net_recv.ipv4.udp.header.data;
	udp->len = net_recv.ipv4.udp.header.len;
	return(1);
}

int16_t net_send_udp(int ifnum, uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, const void *buf, int16_t count){
	uint32_t gw;
	ifnum = interface_route_get(ifnum, dest_ip, &src_ip, &gw);
	if(ifnum < 0)return(ifnum);
	count = udp_write(net_send_buffer_udp, net_send_buffer_udp_len, dest_ip, src_ip, dest_port, src_port, buf, count);
	if(count < 0)return(count);
	return(net_send_ipv4(ifnum, dest_ip, src_ip, IPV4_PROTOCOL_UDP, net_send_buffer_udp, count));
}

// TCP
int net_recv_tcp(net_tcp_t *tcp){
	if(!net_recv.count || !net_recv.ipv4.ok || (net_recv.ipv4.header.protocol != IPV4_PROTOCOL_TCP))return(0);
	if(!net_recv.ipv4.next){
		net_recv.ipv4.next = 1;
		net_recv.ipv4.tcp.used = 0;
		if(tcp_parse_raw(&net_recv.ipv4.tcp.header, net_recv.ipv4.header.dest_ip, net_recv.ipv4.header.src_ip, net_recv.ipv4.header.data, net_recv.ipv4.header.len) < 0){
			net_recv.ipv4.tcp.ok = 0;
			return(0);
		}
		net_recv.ipv4.tcp.ok = 1;
		printf("    NET: RECV TCP: SEQ: %u, ACK: %u, FLAGS: (%s), WIN: %u, COUNT: %u\n",
			(unsigned int)net_recv.ipv4.tcp.header.seq,
			(unsigned int)net_recv.ipv4.tcp.header.ack,
			net_tcp_flags_cstr(net_recv.ipv4.tcp.header.flags),
			(unsigned int)net_recv.ipv4.tcp.header.win,
			(unsigned int)net_recv.ipv4.tcp.header.len
		);
	}
	if(!net_recv.ipv4.tcp.ok || net_recv.ipv4.tcp.used)return(0);
	tcp->dest_ip = net_recv.ipv4.header.dest_ip;
	tcp->src_ip = net_recv.ipv4.header.src_ip;
	tcp->dest_port = net_recv.ipv4.tcp.header.dest_port;
	tcp->src_port = net_recv.ipv4.tcp.header.src_port;
	tcp->seq = net_recv.ipv4.tcp.header.seq;
	tcp->ack = net_recv.ipv4.tcp.header.ack;
	tcp->flags = net_recv.ipv4.tcp.header.flags;
	tcp->win = net_recv.ipv4.tcp.header.win;
	tcp->data = net_recv.ipv4.tcp.header.data;
	tcp->len = net_recv.ipv4.tcp.header.len;
	return(1);
}

void net_used_tcp(){
	if(!net_recv.count || !net_recv.ipv4.ok || (net_recv.ipv4.header.protocol != IPV4_PROTOCOL_TCP) || !net_recv.ipv4.next || !net_recv.ipv4.tcp.ok)return;
	net_recv.ipv4.tcp.used = 1;
}

int16_t net_send_tcp(uint32_t dest_ip, uint32_t src_ip, uint16_t dest_port, uint16_t src_port, uint32_t seq, uint32_t ack, uint16_t flags, uint16_t win, const void *buf, int16_t count){
	printf("NET: SEND TCP: SEQ: %u, ACK: %u, FLAGS: (%s), WIN: %u, COUNT: %u, SP: %u, DP: %u\n",
		(unsigned int)seq,
		(unsigned int)ack,
		net_tcp_flags_cstr(flags),
		(unsigned int)win,
		(unsigned int)count,
		(unsigned int)src_port,
		(unsigned int)dest_port
	);
	count = tcp_write(net_send_buffer_tcp, net_send_buffer_tcp_len, dest_ip, src_ip, dest_port, src_port, seq, ack, flags, win, buf, count);
	if(count < 0)return(count);
	return(net_send_ipv4(-1, dest_ip, src_ip, IPV4_PROTOCOL_TCP, net_send_buffer_tcp, count));
}

// *****
void net_print_mac(char *prefix, const void *mac){
	printf("%s%02X:%02X:%02X:%02X:%02X:%02X\n", prefix, (unsigned int)((uint8_t *)mac)[0], (unsigned int)((uint8_t *)mac)[1], (unsigned int)((uint8_t *)mac)[2], (unsigned int)((uint8_t *)mac)[3], (unsigned int)((uint8_t *)mac)[4], (unsigned int)((uint8_t *)mac)[5]);
}

void net_print_ip(char *prefix, uint32_t ip){
	printf("%s%u.%u.%u.%u\n", prefix, (unsigned int)((ip >> 24) & 0xFF), (unsigned int)((ip >> 16) & 0xFF), (unsigned int)((ip >> 8) & 0xFF), (unsigned int)((ip >> 0) & 0xFF));
}

uint32_t net_timer_get(){
	return(mtimer_get(NULL));
}

uint32_t net_rand(){
	return(platform_rand());
}

static char tcp_flags[64];
char *net_tcp_flags_cstr(uint16_t flags){
	int i;
	i = 0;
	if(flags & TCP_FLAG_FIN){
		i += snprintf(&tcp_flags[i], 64 - i, "fin");
	}
	if(flags & TCP_FLAG_SYN){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " syn");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "syn");
		}
	}
	if(flags & TCP_FLAG_RST){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " rst");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "rst");
		}
	}
	if(flags & TCP_FLAG_PSH){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " psh");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "psh");
		}
	}
	if(flags & TCP_FLAG_ACK){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " ack");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "ack");
		}
	}
	if(flags & TCP_FLAG_URG){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " urg");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "urg");
		}
	}
	if(flags & TCP_FLAG_ECE){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " ece");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "ece");
		}
	}
	if(flags & TCP_FLAG_CWR){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " cwr");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "cwr");
		}
	}
	if(flags & TCP_FLAG_NS){
		if(i){
			i += snprintf(&tcp_flags[i], 64 - i, " ns");
		}else{
			i += snprintf(&tcp_flags[i], 64 - i, "ns");
		}
	}
	tcp_flags[i] = 0;
	return(tcp_flags);
}

