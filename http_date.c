#include "http_date.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>

// man getdate_r
// https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html
// http://www.csgnetwork.com/timerfc1123calc.html

static int str2wday(void *str);
static int str2mon(void *str, int mday);

int getdate_http_r(const void *buf, int size, struct tm *res){
	if(!getdate_rfc1123_r(buf, size, res))return(0);
	if(!getdate_rfc850_r(buf, size, res))return(0);
	return(getdate_asctime_r(buf, size, res));
}

int getdate_rfc1123_r(const void *buf, int size, struct tm *res){
	if(size != 29)return(8);
	if(((char *)buf)[3] != ',')return(8);
	if(((char *)buf)[4] != ' ')return(8);
	if(((char *)buf)[7] != ' ')return(8);
	if(((char *)buf)[11] != ' ')return(8);
	if(((char *)buf)[16] != ' ')return(8);
	if(((char *)buf)[19] != ':')return(8);
	if(((char *)buf)[22] != ':')return(8);
	if(((char *)buf)[25] != ' ')return(8);
	if(((char *)buf)[26] != 'G')return(8);
	if(((char *)buf)[27] != 'M')return(8);
	if(((char *)buf)[28] != 'T')return(8);

	if(!isdigit(((char *)buf)[5]))return(8);
	if(!isdigit(((char *)buf)[6]))return(8);

	if(!isdigit(((char *)buf)[12]))return(8);
	if(!isdigit(((char *)buf)[13]))return(8);
	if(!isdigit(((char *)buf)[14]))return(8);
	if(!isdigit(((char *)buf)[15]))return(8);

	if(!isdigit(((char *)buf)[17]))return(8);
	if(!isdigit(((char *)buf)[18]))return(8);
	if(!isdigit(((char *)buf)[20]))return(8);
	if(!isdigit(((char *)buf)[21]))return(8);
	if(!isdigit(((char *)buf)[23]))return(8);
	if(!isdigit(((char *)buf)[24]))return(8);

	res->tm_wday = str2wday(&((char *)buf)[0]);
	if(res->tm_wday < 0)return(8);

	res->tm_mday = ((((uint8_t *)buf)[5] - '0') * 10) + (((uint8_t *)buf)[6] - '0');
	if(res->tm_mday < 1)return(8);

	res->tm_mon = str2mon(&((char *)buf)[8], res->tm_mday);
	if(res->tm_mon < 0)return(8);

	res->tm_year = ((int)(((uint8_t *)buf)[12] - '0') * 1000) + ((int)(((uint8_t *)buf)[13] - '0') * 100) + ((((uint8_t *)buf)[14] - '0') * 10) + ((((uint8_t *)buf)[15] - '0') * 1);
	if(res->tm_year < 1900)return(8);

	res->tm_hour = ((((uint8_t *)buf)[17] - '0') * 10) + ((((uint8_t *)buf)[18] - '0') * 1);
	if(res->tm_hour > 23)return(8);

	res->tm_min = ((((uint8_t *)buf)[20] - '0') * 10) + ((((uint8_t *)buf)[21] - '0') * 1);
	if(res->tm_min > 59)return(8);

	res->tm_sec = ((((uint8_t *)buf)[23] - '0') * 10) + ((((uint8_t *)buf)[24] - '0') * 1);
	if(res->tm_sec > 60)return(8);

	res->tm_year -= 1900;
	return(0);
}

int getdate_rfc850_r(const void *buf, int size, struct tm *res){
	if((size < 30) || (size > 33))return(8);
	if(((char *)buf)[size - 27] != 'd')return(8);
	if(((char *)buf)[size - 26] != 'a')return(8);
	if(((char *)buf)[size - 25] != 'y')return(8);
	if(((char *)buf)[size - 24] != ',')return(8);
	if(((char *)buf)[size - 23] != ' ')return(8);
	if(((char *)buf)[size - 20] != '-')return(8);
	if(((char *)buf)[size - 16] != '-')return(8);
	if(((char *)buf)[size - 13] != ' ')return(8);
	if(((char *)buf)[size - 10] != ':')return(8);
	if(((char *)buf)[size - 7] != ':')return(8);
	if(((char *)buf)[size - 4] != ' ')return(8);
	if(((char *)buf)[size - 3] != 'G')return(8);
	if(((char *)buf)[size - 2] != 'M')return(8);
	if(((char *)buf)[size - 1] != 'T')return(8);

	if(!isdigit(((char *)buf)[size - 22]))return(8);
	if(!isdigit(((char *)buf)[size - 21]))return(8);

	if(!isdigit(((char *)buf)[size - 15]))return(8);
	if(!isdigit(((char *)buf)[size - 14]))return(8);

	if(!isdigit(((char *)buf)[size - 12]))return(8);
	if(!isdigit(((char *)buf)[size - 11]))return(8);
	if(!isdigit(((char *)buf)[size - 9]))return(8);
	if(!isdigit(((char *)buf)[size - 8]))return(8);
	if(!isdigit(((char *)buf)[size - 6]))return(8);
	if(!isdigit(((char *)buf)[size - 5]))return(8);

	res->tm_wday = str2wday(&((char *)buf)[0]);
	if(res->tm_wday < 0)return(8);

	res->tm_mday = ((((uint8_t *)buf)[size - 22] - '0') * 10) + (((uint8_t *)buf)[size - 21] - '0');
	if(res->tm_mday < 1)return(8);

	res->tm_mon = str2mon(&((char *)buf)[size - 19], res->tm_mday);
	if(res->tm_mon < 0)return(8);

	res->tm_year = ((((uint8_t *)buf)[size - 15] - '0') * 10) + ((((uint8_t *)buf)[size - 14] - '0') * 1);

	res->tm_hour = ((((uint8_t *)buf)[size - 12] - '0') * 10) + ((((uint8_t *)buf)[size - 11] - '0') * 1);
	if(res->tm_hour > 23)return(8);

	res->tm_min = ((((uint8_t *)buf)[size - 9] - '0') * 10) + ((((uint8_t *)buf)[size - 8] - '0') * 1);
	if(res->tm_min > 59)return(8);

	res->tm_sec = ((((uint8_t *)buf)[size - 6] - '0') * 10) + ((((uint8_t *)buf)[size - 5] - '0') * 1);
	if(res->tm_sec > 60)return(8);

	if(res->tm_year < 70)res->tm_year += 100;
	return(0);
}

int getdate_asctime_r(const void *buf, int size, struct tm *res){
	if(size != 24)return(8);
	if(((char *)buf)[3] != ' ')return(8);
	if(((char *)buf)[7] != ' ')return(8);
	if(((char *)buf)[10] != ' ')return(8);
	if(((char *)buf)[13] != ':')return(8);
	if(((char *)buf)[16] != ':')return(8);
	if(((char *)buf)[19] != ' ')return(8);

	if(!isdigit(((char *)buf)[9]))return(8);

	if(!isdigit(((char *)buf)[11]))return(8);
	if(!isdigit(((char *)buf)[12]))return(8);
	if(!isdigit(((char *)buf)[14]))return(8);
	if(!isdigit(((char *)buf)[15]))return(8);
	if(!isdigit(((char *)buf)[17]))return(8);
	if(!isdigit(((char *)buf)[18]))return(8);

	if(!isdigit(((char *)buf)[20]))return(8);
	if(!isdigit(((char *)buf)[21]))return(8);
	if(!isdigit(((char *)buf)[22]))return(8);
	if(!isdigit(((char *)buf)[23]))return(8);

	res->tm_wday = str2wday(&((char *)buf)[0]);
	if(res->tm_wday < 0)return(8);

	if(isdigit(((char *)buf)[8])){
		res->tm_mday = ((((uint8_t *)buf)[8] - '0') * 10) + (((uint8_t *)buf)[9] - '0');
	}else if(((char *)buf)[8] == ' '){
		res->tm_mday = (((uint8_t *)buf)[9] - '0');
	}else{
		return(8);
	}
	if(res->tm_mday < 1)return(8);

	res->tm_mon = str2mon(&((char *)buf)[4], res->tm_mday);
	if(res->tm_mon < 0)return(8);

	res->tm_hour = ((((uint8_t *)buf)[11] - '0') * 10) + ((((uint8_t *)buf)[12] - '0') * 1);
	if(res->tm_hour > 23)return(8);

	res->tm_min = ((((uint8_t *)buf)[14] - '0') * 10) + ((((uint8_t *)buf)[15] - '0') * 1);
	if(res->tm_min > 59)return(8);

	res->tm_sec = ((((uint8_t *)buf)[17] - '0') * 10) + ((((uint8_t *)buf)[18] - '0') * 1);
	if(res->tm_sec > 60)return(8);

	res->tm_year = ((int)(((uint8_t *)buf)[20] - '0') * 1000) + ((int)(((uint8_t *)buf)[21] - '0') * 100) + ((((uint8_t *)buf)[22] - '0') * 10) + ((((uint8_t *)buf)[23] - '0') * 1);
	if(res->tm_year < 1900)return(8);

	res->tm_year -= 1900;
	return(0);
}

/*int main(){
	struct tm tm;
	time_t t;
	char *date = "Thu, 29 Dec 2016 10:51:43 GMT";
//	char *date = "Sun, 06 Nov 1994 08:49:37 GMT";
//	char *date = "Sunday, 06-Nov-94 08:49:37 GMT";
//	char *date = "Sun Nov  6 08:49:37 1994";
	memset(&tm, 0, sizeof(struct tm));
	printf("DATE: %s\n", date);
	printf("%d\n", getdate_http_r(date, strlen(date), &tm));
	printf("WDAY: %d\n", tm.tm_wday);
	printf("MDAY: %d\n", tm.tm_mday);
	printf("MONTH: %d\n", tm.tm_mon);
	printf("YEAR: %d\n", tm.tm_year);
	printf("HOUR: %d\n", tm.tm_hour);
	printf("MIN: %d\n", tm.tm_min);
	printf("SEC: %d\n", tm.tm_sec);
	t = mktime(&tm);
	printf("%s\n", ctime(&t));
}*/

static int str2wday(void *str){
	if(!memcmp(str, "Sun", 3))return(0);
	if(!memcmp(str, "Mon", 3))return(1);
	if(!memcmp(str, "Tue", 3))return(2);
	if(!memcmp(str, "Wed", 3))return(3);
	if(!memcmp(str, "Thu", 3))return(4);
	if(!memcmp(str, "Fri", 3))return(5);
	if(!memcmp(str, "Sat", 3))return(6);
	return(-1);
}

static int str2mon(void *str, int mday){
	if(!memcmp(str, "Jan", 3)){
		if(mday > 31)return(-1);
		return(0);
	}
	if(!memcmp(str, "Feb", 3)){
		if(mday > 29)return(-1);
		return(1);
	}
	if(!memcmp(str, "Mar", 3)){
		if(mday > 31)return(-1);
		return(2);
	}
	if(!memcmp(str, "Apr", 3)){
		if(mday > 30)return(-1);
		return(3);
	}
	if(!memcmp(str, "May", 3)){
		if(mday > 31)return(-1);
		return(4);
	}
	if(!memcmp(str, "Jun", 3)){
		if(mday > 30)return(-1);
		return(5);
	}
	if(!memcmp(str, "Jul", 3)){
		if(mday > 31)return(-1);
		return(6);
	}
	if(!memcmp(str, "Aug", 3)){
		if(mday > 31)return(-1);
		return(7);
	}
	if(!memcmp(str, "Sep", 3)){
		if(mday > 30)return(-1);
		return(8);
	}
	if(!memcmp(str, "Oct", 3)){
		if(mday > 31)return(-1);
		return(9);
	}
	if(!memcmp(str, "Nov", 3)){
		if(mday > 30)return(-1);
		return(10);
	}
	if(!memcmp(str, "Dec", 3)){
		if(mday > 31)return(-1);
		return(11);
	}
	return(-1);
}

