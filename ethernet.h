#ifndef _ETHERNET_H_
#define _ETHERNET_H_

#include <stdint.h>
#include <string.h>

#define ETHERNET_HEADER_LEN                  14

#define ETHERNET_TYPE_IPV4                   0x0800
#define ETHERNET_TYPE_ARP                    0x0806

#define ethernet_dest_mac(buf)               (&((uint8_t *)(buf))[0])
#define ethernet_src_mac(buf)                (&((uint8_t *)(buf))[6])
#define ethernet_type(buf)                   (((uint16_t)((uint8_t *)(buf))[12] << 8) | ((uint8_t *)(buf))[13])

#define ethernet_dest_mac_set(buf, dest_mac) memmove(&((uint8_t *)(buf))[0], (dest_mac), 6)
#define ethernet_src_mac_set(buf, dest_mac)  memmove(&((uint8_t *)(buf))[6], (dest_mac), 6)
#define ethernet_type_set(buf, type)         do{ ((uint8_t *)(buf))[12] = (((uint16_t)(type) >> 8) & 0xFF); ((uint8_t *)(buf))[13] = (((uint16_t)(type) >> 0) & 0xFF); }while(0)

struct ethernet_header_struct{
	uint8_t *dest_mac;
	uint8_t *src_mac;
	uint16_t type;
	void *data;
	int16_t len;
};

typedef struct ethernet_header_struct ethernet_header_t;

int ethernet_parse_raw(ethernet_header_t *header, const void *buf, int16_t count);
int16_t ethernet_write(void *dest, int16_t max, const void *dest_mac, const void *src_mac, uint16_t type, const void *buf, int16_t count);

#endif

